---
title: Tu-144D for FlightGear
subtitle: Credits
css: ../Tu-144D-docs.css
---

Maintainers
-----------

- `@femboywings` ([on gitlab.com](https://gitlab.com/femboywings))
- `@ShFsn` ([on gitlab.com](https://gitlab.com/tigr.tigrov.1977))

Authors
-------

- (c) `@femboywings`, 2019-
    - FDM, systems, etc.
- (c) `@ShFsn`, 2021-
    - Mach cone, graphical instruments
- (c) `@konsni` ([on avsim.su](https://www.avsim.su/forum/profile/6380-konsni)), 2013, 2019
    - External view 3D

Borrowed work
-------------

- `@Vitos`
    - MPClash from [Su-15](http://autopsi.info/Su-15/Su-15.zip)
- Carnegie Mellon University
    - `us_slt_arctic_clunits`, `us_bdl_arctic_clunits`
- `@Lenya69` on avsim.su
    - [cockpit font + GOST 26.008\_85 and GOST 50140\_92](https://www.avsim.su/f/dokumentaciya-obshego-znacheniya-16/shrift-aviacionniy-gosti-29276.html)
- Stolpyanski Victor on avsim.su
    - [Aircraft registration fonts](https://www.avsim.su/f/fs2004-dlya-dizaynerov-i-konstruktorov-37/shrifti-dlya-bortovikh-nomerov-10678.html)

Special Thanks
--------------------

- `@merspieler`
- Many people on FlightGear Discord, Matrix and IRC
- `@NikolaiVChr`
- `@Octal450`
- `@Richard`
- `@mickybadia`
- `@Vitos`
