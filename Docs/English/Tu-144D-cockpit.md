---
title: Tu-144D for FlightGear
subtitle: Cockpit reference
css: ../Tu-144D-docs.css
---

**NOTE.** Self-contained instruments are shown in blue.

Pilots
------

### Left Pilot

![Left Pilot panel photo](../gfx/front_l.svg)

- A1: Rudder command (°)
- A2: Elevator command (°)
- A3: AoA (°), G meter
- A4: IAS (km/h), AT speed bug
- A5: Attitude direction indicator, annunciators (amber): "Left roll warn", "Right roll warn", lamp: "3rd MRG inop" (amber)
- A6: Vertical speed (m/s)
- A7: Vertical regime indicator "IVR": program IAS (km/h) wrt alt (km), current air-data IAS (km/h)
- A8: Engines' RPM
- B2: Aileron command (°)
- B4: Mach (thick needle), TAS (km/h, thin needle)
- B5: Horizontal situation indicator, heading mode lamps: "True gyro" (green), "Mag gyro" (amber), "Mag" (amber)
- B6: Air-data altimeter (m, mmHg)
- B7: Reserve AI
- B8: Thrust lever indicator "UPRT" (°)
- C2: Clock (UTC)
- C4: Radio-magnetic indicator
- C6: Radar alt, warning bug (m), warning light (amber)
- C7: Turn and slip indicator, vertical speed (m/s)
- D1: Flight control system hydraulic power switches, h.s. pressures (kgf/cm²), h.s. lamps: "Low pressure" (red)
- D2: Outside air temperature (°C)
- D3: Leg altitude input and error lamps (red)
- D4: Total air temperature (°C)
- D5: Left and right brake pressure (kgf/cm²)
- D6: Emergency brake accumulator pressure (kgf/cm²), lamp: "Low pressure" (red)
- D7: Altimeter (m)
- E1: Speed warning (red): "IAS", "Mach", "Total air temp"
- E2: "Unable to autotrim" (amber)
- E3: FD approach annunciators (red): "Roll manually", "Pitch manually"
- E4: "Go around manually" (amber)
- E5: FD approach integrated warning, push to test
- E6: "Control thrust manually" (red)
- E7: Director needle deflections out of range (amber)
- F1: FBW pitch, roll, yaw channels: "No reserve" (amber), "Fail" (red)
- F2: AoA and vertical G warning (red)
- F3: Marker beacon lamp: outer (blue), middle (amber), inner (white)
- F4: Tail strike (red)
- F5: Radar alt warning (amber)
- F6: Main brake failure (red)
- F7: Dangerous height (red)
- F8: Autopilot engaged (green): AT, Pitch, Roll
- F9: Avionics annunciators: "Enable FD approach mode" (amber), "Waypoint change" (green), "Correction beacon in range" (green)
- F10: Antiskid active left, right (green)
- F11: Propulsion and config annunciators:
    - amber: "Check engine 1..4", "Check CG", "Eng. 1, 4 Reverser"
    - red: "Fire", "Not ready to taxi", "Not ready for take-off", "Fuel leak warning", "Extend gear", "Reverser fail Eng. 1, 4"

### Right Pilot

![Right Pilot panel photo](../gfx/front_r.svg)

- C8: Set course of left HSI.
- C9: Distance to DME1/RSBN and DME2/Katet.
- D8: Heading panel "PK": heading display, HI caging button, magnetic variation input
- D9: NAV1, NAV2 radial selectors "SK"
- D10: Ground speed radar wind drift and bug (°)
- F12: Avionics annunciators (amber): "Orbita inop", "Ground speed radar inop", "Rhumb inop"
- F13: Gear, tail strike shield, canard: "Out" (green) "Not in" (red), nose cone: "Unlocked" (red).

### Radio Panel

![Radio Panel photo](../gfx/radio.svg)

- A1: FD approach mode control
    - buttons: "Enable", "Test", "Disable"
    - annunciators (green): 3x "Pitch", "Roll"
- A2: HSI source switches: HSI1 and AP, HSI2
- A4: SP-50 balance knobs, push to test
- A5: Runway heading for FD approach mode
- B1: COMM1, COMM2 VHF
- B2: Beacon types for FD approach and INS correction
- B3: RSBN and Katet
- B4: NAV1, NAV2 VOR-DME, ILS and SP-50
- C1: ADF

### Orbita-10 computer panel

![Orbita-10 panel photo](../gfx/Orbita-10.svg)

- A1: Flight to:
    - 01 - 30: waypoint (PPM) № 1 - 30;
    - 31 - 40: alternate waypoint (OPM) № 1 - 10;
    - 41 - 49: landing airport (AP) № 1 - 9.
- A2: Fuel range, x10 km (without aeronav fuel (ANZ), for current flight regime and fuel consumption).
- A3: Fuel time, H MM (without aeronav fuel (ANZ), for current flight regime and fuel consumption).
- A4: Light-button: "Enter aeronav fuel (ANZ)" (not implemented, used for VOR1 magvar prompt).
- A5: Remaining distance, km:
    - normal mode: route to landing airport or last waypoint (KPM), depending on the "PPM-AP" switch;
    - "Direct" report mode: direct to the point selected by "Report" switch;
    - "Route" report mode: route to the point selected by "Report" switch;
- A6: ETA, HH MM (for current flight regime):
    - normal mode: route to landing airport or last waypoint (KPM), depending on the "PPM-AP" switch;
    - "Direct" report mode: direct to the point selected by "Report" switch;
    - "Route" report mode: route to the point selected by "Report" switch;
- A7: Switch: "Time: Moscow - UTC".
- A8: Report mode switching:
    - Switch: "Report №", allowed values:
        - 01 - 30: PPM № 1 - 30,
        - 31 - 40: OPM № 1 - 10,
        - 41 - 49: AP № 1 - 9.
    - light-button "Report: direct"
    - light-button "Report: route"
- A9: Switches:
    - "PPM-AP": distance and ETA to last PPM or to AP;
    - "Units: metric, imperial";
    - "Leg Altitude: program, override (from PZVE)";
    - "Runway: 1, 2".
- A10: Switch: "INS coordinate integration".
- A11:
    - Switches:
        - "Leg select": Program (automatic waypoint rotation), PPM, PPM+10, PPM+20, OPM, AP, Holding pattern ("Holding" not implemented);
        - "№ PPM (waypoint)";
        - "AP (landing airport)".
    - Light-buttons: "Test", "Execute".
- B1: Lines I, II, units, as appropriate for different modes:
    - S KKK MMM: sign, km, m;
    - S DDD MM S: sign, degrees, angular minutes, x10 angular seconds;
    - S HHH MM S: sign (unused), hours, minutes, x10 sec.
- B2: indication mode light-buttons:
    - Z/X/, S/Y/: aircraft's orthodromic coordinates:
        - flying to waypoint — relative to leg ending with WP: Z: crosstrack distance, S: track distance,
        - flying to airport — relative to airport's centre and rwy direction: X: distance along base leg, Y: distance along final leg;
    - H эш, ΔH: leg altitude, altitude error;
    - A ВПП, A РД: runway and taxiway heading;
    - ΔZ, ΔS: correction to Z and S from beacon;
    - φ, λ: latitude, longitude of aircraft;
    - φ ПМ, λ ПМ: latitude, longitude of waypoint;
    - φ АП, λ АП: latitude, longitude of airport;
    - φ РМ, λ РМ: latitude, longitude of beacon;
    - U/Ux, δ/Uz: wind:
        - flying to waypoint: speed U, from heading δ;
        - flying to airport: track Ux and cross-track Uz components (not implemented);
    - Код: data input (not implemented);
    - T, T приб: time, ETA (for current flight regime):
    - № РМ, № кан.: beacon number, RSBN channel or VOR-DME frequency.
- B3: switch: "Continuous correction - correction once".
- B4: light-buttons "Hint" (not implemented), "Enter" (works only in "ΔZ, ΔS" mode to apply correction).
- B5: data entry keypad (not implemented).

Engineer
--------

![Engineer's Main Panel photo](../gfx/engineer_main.svg)

### 1: Power and APU

- A1: H.s. I valve switch and annunciators: "Open" (green), "Closed" (red)
- A2: H.s. I+II tank nitrogen pressure (kgf/cm²) and annunciator: "Nitrogen on" (amber)
- A3: H.s. I pressure (kgf/cm²), annunciator: "Low pressure" (red)
- A4: Liquid in the tank H.s. I+II (Litre)
- B1: Nitrogen gauge (kgf/cm²)
- B2: Emergency brake accumulator pressure (kgf/cm²) and annunciator: "Low pressure" (red)
- B3: Brake annunciators (red): "Parking", "Emergency"
- B4: Lamp test button
- C1: Battery 1, 3 switches
- C2: Annunciator: "Left network on battery power" (red)
- C3: Left 27V sources voltmeter
- C4: Rectifier 1 switch and annunciator: "Off" (red)
- C5: 27V left+right network join switch, annunciator "Networks joined" (amber)
- C6: 27V buses voltmeter
- D1: Eng. 1 200V generator switch, annunciator: "Off" (red), ampermeter
- D2: Gen. 1+2 bus join switch and annunciator: "Buses separated" (amber)
- D3: APU generator switch and annunciator: "APU Generator" (green)
- D4: 200V Ground power switch
- D5: APU and ground power ampermeter
- D6: 200V ampermeters' phase
- D7: 200V network join switches: "left+APU", "APU+right", annunciator "Networks joined" (amber)
- D8: 200V frequency and voltmeter
- D9: 200V essential bus inverter switch with annunciator: "PO-500" (amber)
- E1: 36V essential bus inverter switch with annunciator: "PT-1000" (amber)
- E2: Left 36V transformer switch with annunciator: "Left network on reserve transformer" (amber)
- E3: 36V voltmeter
- E4: "PTS-250" (amber) 36V essential bus inverter annunciator
- F1: APU RPM
- F2: APU emergency stop button
- F3: APU annunciators:
    - red: "Low oil", "Oil pressure low", "EGT warn", "RPM warn", "APU control fault"
    - amber: "Inlet & exhaust open"
    - green: "Main fuel pump", "Reserve fuel pump", "Bleed open", "Starter ready", "Ready", "Starter"
- F6: APU control buttons: "Stop", "Start", switches: "Master", "Ignition", "Bleed"

### 2: Enginess and inlets
- A1: Nozzle position indicator
- A2: Annunciators: "Oil filter debris" (amber), "IDG oil pressure low" (red)
- A3: Fuel flow indicator (tonnes / hour)
- A4: Thrust lever indicator "UPRT" eng. 1+2
- A5: Annunciators (red): "Eng. 1 stopped", "Vibration warn", "Compressor surge"
- A6: Eng. 1 RPM (needle №1) and IDG 1 RPM (needle №2)
- A7: IDG 1 CVD disconnect switch
- A8: "Inlet guide vane closed" annunciator (amber)
- A9: Eng. 1 EGT (°C)
- A10: Annunciators: "Turbine cooling off" (amber), "EGT warn" (red)
- B1: Inlet 1 ramp and spill door position and manual command (%)
- B2: Inlet 1 ramp switches: "Auto control h.s.", "Manual control h.s.", annunciators: "Main" (green), "Reserve" (amber), "Manual" (amber), "Fail" lamp (red)
- B3: Inlet 1 spill door auto switch, annunciators: "Auto" (green), "Manual" (amber), "Fail" lamp (red)
- C1: Eng. 1 annunciators (red): "Fuel pressure low", "Oil pressure low", "Low oil", "Too much oil"
- C2: Eng. 1 & 2 oil level (Litre)
- C3: Combined engine indicator: fuel pressure (kgf/cm²), oil pressure (kgf/cm²), oil temp (°C)
- C4: Switch: "Combined engine indicator and vibration indicator engine" (not implemented, see Engineer dialog instead), buttons: "EGT indicators test", "Lamp test"

### 3: Fuel
- A1: Tank 8 jettison
- A2: Feed tanks jettison
- B1: Clock (UTC)
- C1: CG and CG bug (%MAC)
- C2: Tanks 1, 2 (tonnes)
- C3: Tanks 4L, 4R (tonnes)
- C4: Tanks 5L, 5R (tonnes)
- C5: Tanks 3, 7 (tonnes)
- C6: Tanks 6, 8 (tonnes)
- C7: Total fuel (x10 tonnes) and feed tank / feed tank sum (tonnes)
- C8: Feed tank display select
- C9: Lamp test
- C10: Fuel flow meter: fuel left (tonnes)
- D1: Fuel trim control:
    - transfer switches, lamps (green): "O": pump, "I": valve
    - lamps (red): "Auto CG inop" and "Tank 8 valve №3 closed"
- D2: Tank selection:
    - lamps: "main pump" (green), "finalising pump" (amber); tank select switches
    - annunciator: "&lt; 2500 kg in tank 6" (red)
- D3: Feed tank 1:
    - lamps (green): "Valve", "Boost pump"
    - annunciators (red): "&lt; 1000 kg", "Turn on boost pump"
    - switch: boost pumps
- D4: Crossfeed valve switches and annunciators (amber)

**NOTE.** Tank naming and layout (see Aircraft help):

![Fuel tanks diagram](../gfx/tanks.svg)
