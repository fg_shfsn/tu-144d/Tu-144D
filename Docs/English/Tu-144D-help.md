---
title: Tu-144D for FlightGear
subtitle: Aircraft help
css: ../Tu-144D-docs.css
---

Hotkeys
-------

### Control, mechanisation

    q                      Elevator Trim in the direction of column force
    Control-←,→            Aileron Trim
    Control-,,.            Rudder Trim
    G, Shift-G             Gear: Retract, Neutral, Neutral (latched), Neutral, Extend
    F, Shift-F             Cone: Retract (latched), Retract, 11°, 17°
    [, ]                   Canard: Retract (latched), Retract, Neutral, Extend
    Shift-B                Start Brake: On, Off
    Shift-N                Parking Brake: On, Off
    N                      Emergency Brake
    Delete, Insert         Reverse Thrust Eng. 1,4 (thrust lever must be at Idle)
    Control-Delete,Insert  Parachute: Deploy, Drop
    Shift-~,1,2,3,4,5,6    Eng./sys. select: all, Eng. 1, 2, 3, 4, APU, Avionics
    S                      Start selected engine or system

### Autopilot

    =                        AP+AT Quick Disengage / Confirm
    Control-[Shift-]Y        AP Roll / Pitch Disengage and Confirm
    [Shift-]F4,F5            AP Pitch Knob: Dive, Climb
    Control-F4,F5            AP Pitch Knob: Neutral
    F6,[Shift-]F7,F8         AP Turn Knob: Neutral, Left, Right
    Control-T                AP Take-off / Go-around
    Control-Q                Heading Hold
    Control-D                Flight Director: activate
    Control-PageUp,PageDown  AT speed bug (engages AT IAS hold)
    Control-Shift-S          AT: toggle Mach or TAT hold

**NOTE.** Heading hold key was moved from to Control-Q because of a bug in FG.

### Fuel trim

    Control-K           Auto: Climb, Cruise, Descent, Off
    Control-Home        Climb, Cruise: toggle forward (Tank 8 to Tanks 1 or 2)
    Control-End         Climb, Cruise: toggle aft (Tanks 1 or 2 to Tanks 8 or 4)
    Control-Shift-Home  Descent: toggle forward (Tank 8 to Tanks 6 or 4)
    Control-Shift-End   Descent: toggle aft (Tanks 1 or 2 to Tank 6)

### Common

    W                  Master Caution/Warning: Ack. (Except AP Disengage)
    [Shift-]L          Cockpit Light Brightness
    Control-[Shift-]L  Front Light: Taxi, Off, Landing
    [Shift-]C          Change Seat
    Y                  Yoke, Armrests: Hide, Show
    F1                 Checklists, Procedures

### Comm

Modifier keys of the PTT button (Space) select corresponding radio:

    Space                VHF1
    Shift-Space          VHF2
    Control-Space        HF1
    Control-Shift-Space  HF2

This function can be turned off in Tu-144D settings.

Characteristics
---------------

### Speed

    Decision (V_1)          240--255 km/h
    Rotate (V_R)            320--340 km/h
    Lift-off                380--400 km/h
    Safety (V_2)            380--400 km/h (205--216 kt)

    Gear                  < 400 km/h
    Canard Retract        < 440 km/h
    Canard Extend         < 400 km/h
    Cone 17 deg.          < 440 km/h
    Cone 11 deg.          < M0.9

    Cruise                  M1.9--2.1
    Max. at 19 km           M2.15
    Skin heating limit      M2.35 ≤ 5 min

    Pattern (canard out)    340 km/h (184 kt)
    Final                   300 km/h (162 kt)
    Glideslope              290 km/h (157 kt)
    Threshold (V_ref)       290 km/h (157 kt)
    Touchdown             < 290 km/h
    Chute                 < 350 km/h
    Brakes                < 260 km/h (above 100 km/h limited automatically)

### Weight

    Empty (OEW)                        99200 kg
    Max zero fuel (MZFW)              114200 kg
    Taxi                  typ.        182500 kg
                          max         209000 kg
    Take-off              typ.        180000 kg
                          max (MTOW)  207000 kg
    Max landing (MLW)                 132000 kg
    Max Fuel Capacity                 106000 kg
    Max payload                        15000 kg
    Max passengers                       150

### Altitude

    Cruise             16000--18500 m (52493--60695 ft)
    Practical ceiling  19700 m        (64632 ft)

### Distance
    Climb and acceleration    ~ 500 km  (270 nmi)
    Braking and descent         600 km  (324 nmi)
    Practical range            5700 km (3078 nmi)
    (load 7500 kg, max fuel)

### ICAO
    Performance code                 D
    Weight category                  H
    Radio/Nav Equipment              SDFHI
    Transponder Equipment            C

Fuel system
-----------

![Fuel tanks layout](../gfx/tanks.svg)

"Л" is "Left", "П" is "Right", "ЦТ" is aircraft's empty CG (41% MAC).

Feed tanks (yellow): FT1, FT2, FT3, FT4.

- Used by the engines and fuel transfer pumps. APU uses FT4.
- Must always stay full, are depleted only in emergency.

Main tanks (red): 3, 4, 5, 6, 7.

- Used to replenish feed tanks.
- Automatically selected so as not to affect CG too much: 4, 3&4, 3&5, 3&7, 6&7, 6.

Trim tanks (blue): 1, 2, 8.

- Fuel is pumped between 1, 2 (front) and 8 (tail) to move CG.
- Fuel no longer needed for trim can be moved to main tanks.

Fuel trim CG limits versus Mach number:

![CG limits vs. Mach number](../gfx/cg.svg)

Bold line: safety limits, dash-dotted line: limits in normal flight, thin line: approximate CG in fully fueled climb.

Hydraulic systems
-----------------

    System    Sources        Consumers
    I         Eng. 1&2, APU  FCS, brakes, inlets 1&2 (reserve)
    II        Eng. 1&2, APU  FCS, gear, trim pumps, system "em.brake", inlets 1&2
    III       Eng. 3&4, APU  FCS, nose wheel steering, trim pumps, inlets 3&4
    IV        Eng. 3&4, APU  FCS, inlets 3&4 (reserve)
    em.brake  system "II"    emergency brakes

Weather minimums
----------------

1. Ceiling and visibility:
    - 100 x 1200 m for destination airport;
    - 150 x 2000 m for diversion airports.
2. Icing in airport area:
    - no heavy icing;
    - no icing for temperatures < 10 degC.
3. Friction coefficient μ ≥ 0.3.
4. Max. crosswind:
    - 14 m/s for μ ≥ 0.5
    - 7 m/s for 0.5 < μ ≤ 0.3;
5. Max. tailwind 5 m/s.
6. Air temperature: -30..+30 °C.

Fuel planning
-------------

**CAUTION.** Only valid for distance (L) ≤ 5700 km, payload ≤ 7500 kg, cruise M = 2.0.

    1. Landing:   13700 kg
    2. Take-off:  13700 + 15.67 * L kg        (landing + trip)
    3. Fueling:   13700 + 2700 + 15.67 * L kg (take-off + taxi)

Max. fuel: 106000 kg.
