---
title: Ту-144Д для FlightGear
subtitle: Список авторов и благодарностей
css: ../Tu-144D-docs.css
---

Разработчики
------------

- `@femboywings` ([на gitlab.com](https://gitlab.com/femboywings))
- `@ShFsn` ([на gitlab.com](https://gitlab.com/tigr.tigrov.1977))

Авторы
------

- (c) `@femboywings`, 2019-
    - Модель динамики, логики и приборов
- (c) `@ShFsn`, 2021-
    - Конус Маха, графические приборы
- (c) `@konsni` ([на avsim.su](https://www.avsim.su/forum/profile/6380-konsni)), 2013, 2019
    - Визуальная модель

Заимствованные материалы
------------------------

- `@Vitos`
    - поддержка MPClash из пакета [Су-15](http://autopsi.info/Su-15/Su-15.zip)
- Carnegie Mellon University
    - `us_slt_arctic_clunits`, `us_bdl_arctic_clunits`
- `@Lenya69` на avsim.su
    - [шрифт авиационный для кабинных надписей + ГОСТ 26.008\_85 и ГОСТ 50140\_92](https://www.avsim.su/f/dokumentaciya-obshego-znacheniya-16/shrift-aviacionniy-gosti-29276.html)
- Столпянский Виктор на avsim.su
    - [Шрифты для бортовых номеров](https://www.avsim.su/f/fs2004-dlya-dizaynerov-i-konstruktorov-37/shrifti-dlya-bortovikh-nomerov-10678.html)

Особые благодарности
--------------------

- `@merspieler`
- Многие на Discord, Matrix и IRC FlightGear
- `@NikolaiVChr`
- `@Octal450`
- `@Richard`
- `@mickybadia`
- `@Vitos`
