# FIXME The whole "crew" thing is a trainwreck with epicycles on top of epicycles -- rewrite!
# FIXME Split up the main loop into one function per task and make an on/off option for each of them.

# Settings.

# var control = 1;
var _callouts = 1;


# Orders.

var _wet_runway = 1;


# State.

var _speaking = 0;

var _ready = 0;

var _running = [1, 1, 1, 1];
var _takeoff = 0;
var _apu = 0;

var _cg = 41.0;
var _cg_diff = 100.0;



var say = func(sound, length, allowskip = 0) {
 if((sound == "") or !sound or !_callouts){
  return;
 }
 if(_speaking){
  if(allowskip){
   print("engineer: sound ", sound, " skipped.");
   return;
  }
  settimer(func{say(sound, length)}, 0.5, 1);
  print("engineer: sound ", sound, " in queue.");
  return;
 }
 _speaking = 1;
 setprop("/sim/crew/engineer/sound/" ~ sound, 1);
 settimer(func{
  setprop("/sim/crew/engineer/sound/" ~ sound, 0);
  _speaking = 0;
 }, length, 1);
}



var chat = func(text, emphasis = 0) {
 if(!_callouts){
  return;
 }
 Tu144D.chat("(engineer): " ~ text, emphasis);
}



var do = func(action) {
 if(!_control){
  return;
 }
 action();
}



var approx = func(val1, val2, tol) {
 return (abs(val1 - val2) < tol);
}



var TakeoffPrompt = func() {
 var takeoff_tonnes = math.round(getprop("/fdm/jsbsim/inertia/weight-lbs") * 0.001 * LB2KG, 1.0);
 if(!_ready){
  chat("Not ready for takeoff!", 3);
  return;
 }
 var emphasis = 0;
 var text = "V1: " ~ Tu144D.V1() ~ ", Vr: " ~ Tu144D.Vr() ~ ", V2: " ~ Tu144D.V2() ~ " km/h. PLA: " ~ Tu144D.TakeoffPLA() ~ " deg. TOW: " ~ takeoff_tonnes ~ " tonnes.";
 if(takeoff_tonnes > 207){
  text = text ~ " MTOW exceeded!";
  emphasis = 3;
 }
 chat(text, emphasis);
}



var _loop = func {

 if(
  getprop("/sim/freeze/replay-state") or
  getprop("/fdm/jsbsim/systems/airframe/damage/break")
 ){
  return;
 }

 var running = [0, 0, 0, 0];
 for(var ii = 0; ii < 4; ii = ii + 1){
  running[ii] = getprop("/fdm/jsbsim/propulsion/engine[" ~ ii ~ "]/set-running");
 }

 var WOW = getprop("/gear/gear[1]/wow") or getprop("/gear/gear[2]/wow");
 var Vind = getprop("/fdm/jsbsim/fcs/NPK/instr/US-I[1]/gauge/V-km_h");
 var ready = (!getprop("/fdm/jsbsim/fcs/NPK/status/takeoff-notready") or (Vind > 100.0));
 if(WOW){
  for(var ii = 0; ii < 4; ii += 1){
   if(running[ii] == 0){
    ready = 0;
    break;
   }
  }
 }

 if(ready > _ready){
  settimer(func {
   say("ready", 2.0);
   TakeoffPrompt();
  }, 5.0);
 }

 var cg = getprop("/fdm/jsbsim/propulsion/fuel/panel/gauge/cg-pMAC");
 # FIXME CG bug should be set by engineer, instead of just reading it.
 var emphasis = 0;
 var cg_auto = getprop("/fdm/jsbsim/propulsion/fuel/panel/gauge/cg-set-pMAC");
 var cg_diff = math.round(cg_auto - cg, 0.1);
 var cg = math.round(cg, 0.1);
 if(((_cg != cg) or (abs(cg_diff) > abs(_cg_diff))) and abs(cg_diff) > 0.75){
  if(abs(cg_diff) > 1.5){
   emphasis = 2;
  }
  chat(sprintf("CG: %.1f%% MAC. Required: ~%.1f%% MAC.", cg, cg_auto), emphasis);
 }
 _cg = cg;
 _cg_diff = cg_diff;

 for(var ii = 0; ii < 4; ii = ii + 1){
  if(running[ii] > _running[ii]){
   if(getprop("/fdm/jsbsim/propulsion/engine[" ~ ii ~ "]/panel/lever/throttle/pos-deg") < 10.0){
    say("eng" ~ (ii + 1) ~ "idle", 2.0);
   }
   Tu144D.Stopwatch(2, 1);
  }else if(running[ii] < _running[ii]){
   if(!getprop("/fdm/jsbsim/propulsion/engine[" ~ ii ~ "]/panel/lever/cutoff")){
    say("eng" ~ (ii + 1) ~ "fault", 1.0);
   }
   var nrun = 0;
   for(var jj = 0; jj < 4; jj = jj + 1){
    nrun += running[jj];
   }
   if(!nrun){
    Tu144D.Stopwatch(2, 2);
   }
  }
 }

 var apu = (getprop("/fdm/jsbsim/propulsion/engine[4]/panel/gauge/n") > 70.0);
 if(apu < _apu){
  say("apu_off", 2.0);
 }
_apu = apu;

 if(WOW){
  var takeoff = 1;
  for(var i = 0; i < 4; i = i + 1){
   if(
    ((getprop("/fdm/jsbsim/propulsion/engine[" ~ i ~ "]/panel/lever/throttle/pos-deg") - Tu144D.TakeoffPLA()) < -0.5) or
    (abs(getprop("/fdm/jsbsim/propulsion/engine[" ~ i ~ "]/panel/gauge/n") - Tu144D.TakeoffN()) > 10.0) or
    getprop("/fdm/jsbsim/propulsion/engine[" ~ i ~ "]/panel/light/check-engine")
   ){
    takeoff = 0;
    break;
   }
  }
  if(takeoff > _takeoff){
   say("takeoff", 5.0);
  }
  _takeoff = takeoff;
 }

 _ready = ready;
 _running = running;

}


var _timer = maketimer(1.0, func{_loop()});
_timer.simulatedTime = 1;



setlistener("/sim/signals/fdm-initialized", func(p) {
 if(p.getValue() == 0){
  return;
 }
 var overlay = getprop("/nasal/overlay/name") or "";
 if((overlay == "subsonic") or (overlay == "cruise") or (overlay == "approach")){
  _ready = 1;
 }
 settimer(func {
  _cg = math.round(getprop("/fdm/jsbsim/propulsion/fuel/panel/gauge/cg-pMAC"), 0.1);
  _timer.start();
 }, 1.5);
});

setlistener("/sim/crew/engineer/control", func(p) {
 _control = p.getValue();
}, 1, 0);

setlistener("/sim/crew/engineer/callouts", func(p) {
 _callouts = p.getValue();
}, 1, 0);
