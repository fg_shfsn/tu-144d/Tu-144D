var pilot_type   = "Aircraft/Tu-144D/Models/Tu-144D.xml";
var copilot_type = "Aircraft/Tu-144D/Models/Tu-144D-copilot.xml";

props.globals.initNode("/sim/remote/pilot-callsign", "", "STRING");

var pilot_connect_copilot = func (copilot) {
 return [];
}

var pilot_disconnect_copilot = func {
 return [];
}

var copilot_connect_pilot = func (pilot) {
 return [];
}

var copilot_disconnect_pilot = func {
 return [];
}
