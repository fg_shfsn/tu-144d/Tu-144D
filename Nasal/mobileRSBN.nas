# Mobile RSBN unit
#
# This is a kludge for FG's inability to add/remove navaids during
# runtime.


var latitude = nil;
var longitude = nil;
var elevation = 0.0;


# @brief Return maximum range in km.
# @param dH height of the aircraft wrt navaid in m.
var range = func(dH) {
 # 35 km 500..550 km
 # 20 km 450 km
 # 5 km  250 km
 # 250 m >=50 km
 if(dH > 250.0){
  if(dH > 5000.0){
   if(dH > 20000.0){
    if(dH > 35000.0){
     return 550.0;
    }
    # 450.0 + (dH - 20000.0) * (550.0 - 450.0)/(35000.0 - 20000.0)
    return dH * 0.00667 + 316.667;
   }
   # 250.0 + (dH - 5000.0) * (450.0 - 250.0)/(20000.0 - 5000.0)
   return dH * 0.0133 + 183.333;
  }
  # 50.0 + (dH - 250.0) * (250.0 - 50.0)/(5000.0 - 250.0)
  return dH * 0.0421 + 39.474;
 }
 return 50.0;
}

# @brief Return current radial.
var radial = func() {
 var (A, mS) = courseAndDistance(
  {
   lat: latitude,
   lon: longitude
  },
  geo.aircraft_position()
 );
 return geo.normdeg(A+180);
}

# @brief Return slant distance.
var slantdist = func() {
 var aircraft = geo.Coord.new(geo.aircraft_position());
 var navaid = geo.Coord.new();
 navaid.set_latlon(latitude, longitude, elevation);
 return navaid.direct_distance_to(aircraft) * 0.001;
}


var slowloop = func() {
 var H = geo.elevation(latitude, longitude);
 if(H != nil){
  elevation = H;
 }else{
  elevation = 0.0;
 }
}

var fastloop = func() {
 var mS = slantdist();
 var in_range = (mS < range(geo.aircraft_position().alt() - elevation));
 setprop("/sim/mobileRSBN/in-range", in_range);
 if(in_range){
  setprop("/sim/mobileRSBN/indicated-distance-km", mS);
  setprop("/sim/mobileRSBN/indicated-radial-deg", radial());
 }
}


var slowtimer = maketimer(7.9, func{slowloop()});
var fasttimer = maketimer(0.05, func{fastloop()});

var start = func() {
 latitude = getprop("/sim/mobileRSBN/latitude-deg");
 longitude = getprop("/sim/mobileRSBN/longitude-deg");
 slowtimer.start();
 fasttimer.start();
}

var stop = func() {
 slowtimer.stop();
 fasttimer.stop();
 setprop("/sim/mobileRSBN/in-range", 0);
}


setlistener("/sim/mobileRSBN/enabled", func(p) {
 var enable = p.getValue();
 if(enable){
  start();
 }else{
  stop();
 }
}, 1, 0);

setlistener("/sim/mobileRSBN/latitude-deg", func(p) {
 latitude = p.getValue();
}, 1, 0);

setlistener("/sim/mobileRSBN/longitude-deg", func(p) {
 longitude = p.getValue();
}, 1, 0);
