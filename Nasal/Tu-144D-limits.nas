var _airframe_damage = 0;


var Check = func {

 var airframe_damage = 0;
 if(getprop("/fdm/jsbsim/systems/airframe/damage/break") or 0){
  airframe_damage = 3;
 }else if(getprop("/fdm/jsbsim/systems/airframe/damage/deform") or 0){
  airframe_damage = 2;
 }else if(getprop("/fdm/jsbsim/systems/airframe/damage/systems") or 0){
  airframe_damage = 1;
 }

 if(airframe_damage > _airframe_damage){
  if(airframe_damage == 3){
   gui.popupTip("W  A  S  T  E  D");
  }else if(airframe_damage == 2){
   gui.popupTip("Airframe: deformation G limit exceeded!");
  }else if(airframe_damage == 1){
   gui.popupTip("Airframe: systems/passengers G limit exceeded!");
  }
 }

 if(airframe_damage == 3){
  var ncont = 28;
  for(var icont = 0; icont < ncont; icont += 1){
   var wow = getprop("/gear/gear[" ~ icont ~ "]/wow");
   if(wow){
    setprop("/sim/crashed", 1);
    break;
   }else if(wow == nil){
    break;
   }
  }
 }

 if((_airframe_damage == 3) and (airframe_damage < _airframe_damage)){
  setprop("/sim/crashed", 0);
 }

 _airframe_damage = airframe_damage;

}


var _timer = maketimer(1.0, func{Check()});


_timer.start();
