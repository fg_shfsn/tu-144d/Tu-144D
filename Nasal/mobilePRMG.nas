# Mobile PRMG unit
#
# This is a kludge for FG's inability to add/remove navaids during
# runtime.


#var LOC_range = 45.0;
var LOC_maxangle = 15.0;
#var GS_range = 18.0;
var GS_maxanglemult = 0.75;
#var DM_range = 45.0;

# FIXME In this code there is a sharp cutoff without a zone of unreliable signal. The workaround for now is to just increase the ranges.
var LOC_range = 90.0;
var GS_range = 36.0;
var DM_range = 90.0;


var LOC_latitude = nil;
var LOC_longitude = nil;
var LOC_elevation = 0.0;
var LOC_heading = 0.0;
var LOC_width_half = 2.25;
var GS_latitude = nil;
var GS_longitude = nil;
var GS_elevation = 0.0;
var GS_slope = 3.0;
var valid = 0;
var GS_elev_valid = 0;
var LOC_elev_valid = 0;

var code = nil;
var channel = 0;


var init = func() {
 invalidate();
 var ap = airportinfo(code);
 if(ap == nil){
  return;
 }
 var rwy = findrwy(ap, channel * 10.0, 5.0);
 if(rwy != nil){
  var LOC_ant=geo.Coord.new();
  LOC_ant.set_latlon(ap.runways[rwy].lat, ap.runways[rwy].lon);
  LOC_ant.apply_course_distance(
   ap.runways[rwy].heading,
   ap.runways[rwy].length + ap.runways[rwy].reciprocal.stopway + 590.0
  );
  LOC_latitude = LOC_ant.lat();
  LOC_longitude = LOC_ant.lon();
  LOC_heading = ap.runways[rwy].heading;
  var GS_ant = geo.Coord.new();
  GS_ant.set_latlon(ap.runways[rwy].lat, ap.runways[rwy].lon);
  GS_ant.apply_course_distance(
   ap.runways[rwy].heading,
   ap.runways[rwy].threshold + 300.0
  );
#  GS_ant.apply_course_distance(
#   ap.runways[rwy].heading - 90.0,
#   ap.runways[rwy].width / 2 + 50.0
#  );
  GS_latitude = GS_ant.lat();
  GS_longitude = GS_ant.lon();
  valid = 1;
  gui.popupTip("Katet landing beacons: " ~ code ~ " " ~ rwy ~ ".");
 }
}

# @brief Find the longest runway in the airport with the heading.
# @param ap Airport object.
# @param psi Heading of the runway, deg.
# @param tol Magnetic heading tolerance, deg. Default: 0.1.
var findrwy = func(ap, psi, tol = 0.1) {
 var rwy = nil;
 var score = 0.0;
 foreach(var i; keys(ap.runways)){
  if(abs(geo.normdeg180(ap.runways[i].heading - psi)) < tol){
   # Select the longest runway if there are multiple.
   if(ap.runways[i].length > score){
    rwy = i;
    score = ap.runways[i].length;
   }
  }
 }
 return rwy;
}

# @brief Return GS deflection in degrees.
var GSdefl = func() {
 var H = geo.aircraft_position().alt() - GS_elevation;
 var (A, S) = courseAndDistance(
  {
   lat: GS_latitude,
   lon: GS_longitude
  },
  geo.aircraft_position()
 );
 return GS_slope - math.atan2(H, S * NM2M) * R2D;
}

# @brief Return slant distance to the GS beacon.
var GSdist = func() {
 var aircraft = geo.Coord.new(geo.aircraft_position());
 var navaid = geo.Coord.new();
 navaid.set_latlon(GS_latitude, GS_longitude, GS_elevation);
 return navaid.direct_distance_to(aircraft) * 0.001;
}

# @brief Return LOC side deflection in degrees.
var LOCdefl = func() {
 var (A, S) = courseAndDistance(
  {
   lat: LOC_latitude,
   lon: LOC_longitude
  },
  geo.aircraft_position()
 );
 return geo.normdeg180(A + 180.0 - LOC_heading);
}

# @brief Return slant distance to the LOC beacon.
var LOCdist = func() {
 var aircraft = geo.Coord.new(geo.aircraft_position());
 var navaid = geo.Coord.new();
 navaid.set_latlon(LOC_latitude, LOC_longitude, LOC_elevation);
 return navaid.direct_distance_to(aircraft) * 0.001;
}


var slowloop = func() {
 var code_new = getprop("/sim/mobilePRMG/airport-id");
 var channel_new = getprop("/sim/mobilePRMG/channel");
 if((code_new != code) + (channel_new != channel)){
  channel = channel_new;
  code = code_new;
  init();
 }
 if(valid){
  GS_slope = getprop("/sim/mobilePRMG/glide-slope-deg");
  LOC_width_half = getprop("/sim/mobilePRMG/width-deg") * 0.5;
  if(!GS_elev_valid){
   var H = geo.elevation(GS_latitude, GS_longitude);
   if(H != nil){
    GS_elevation = H;
    GS_elev_valid = 1;
   }else{
    GS_elevation = 0.0;
   }
  }
  if(!LOC_elev_valid){
   H = geo.elevation(LOC_latitude, LOC_longitude);
   if(H != nil){
    LOC_elevation = H;
    LOC_elev_valid = 1;
   }else{
    LOC_elevation = 0.0;
   }
  }
 }
}

var fastloop = func() {
 if(valid){
  if(abs(LOCdefl()) < LOC_maxangle){
   var LOC_in_range = (LOCdist() < LOC_range);
   setprop("/sim/mobilePRMG/in-range", LOC_in_range);
   if(LOC_in_range){
    setprop("/sim/mobilePRMG/heading-needle-deflection-norm", LOCdefl() / LOC_width_half);
   }else{
    setprop("/sim/mobilePRMG/heading-needle-deflection-norm", 0.0);
   }
   var mS = GSdist();
   var GS_in_range = (mS < GS_range);
   setprop("/sim/mobilePRMG/gs-in-range", GS_in_range);
   var DM_in_range = (mS < DM_range);
   setprop("/sim/mobilePRMG/dm-in-range", DM_in_range);
   if(GS_in_range){
    # GS beam width: 1.4 deg. 2 / 1.4 = 1.43
    setprop("/sim/mobilePRMG/gs-needle-deflection-norm", 1.43 * GSdefl());
   }else{
    setprop("/sim/mobilePRMG/gs-needle-deflection-norm", 0.0);
   }
   if(DM_in_range){
    setprop("/sim/mobilePRMG/indicated-distance-km", mS);
   }else{
    setprop("/sim/mobilePRMG/indicated-distance-km", 0.0);
   }
  }else{
   setprop("/sim/mobilePRMG/in-range", 0);
   setprop("/sim/mobilePRMG/gs-in-range", 0);
   setprop("/sim/mobilePRMG/dm-in-range", 0);
   setprop("/sim/mobilePRMG/heading-needle-deflection-norm", 0.0);
   setprop("/sim/mobilePRMG/indicated-relative-bearing-deg", 0.0);
   setprop("/sim/mobilePRMG/gs-needle-deflection-norm", 0.0);
   setprop("/sim/mobilePRMG/indicated-distance-km", 0.0);
  }
 }
}


var slowtimer = maketimer(5.3, func{slowloop()});
var fasttimer = maketimer(0.023, func{fastloop()});

var invalidate = func() {
 valid = 0;
 LOC_elev_valid = 0;
 GS_elev_valid = 0;
 setprop("/sim/mobilePRMG/in-range", 0);
 setprop("/sim/mobilePRMG/gs-in-range", 0);
 setprop("/sim/mobilePRMG/dm-in-range", 0);
 setprop("/sim/mobilePRMG/heading-needle-deflection-norm", 0.0);
 setprop("/sim/mobilePRMG/indicated-relative-bearing-deg", 0.0);
 setprop("/sim/mobilePRMG/gs-needle-deflection-norm", 0.0);
 setprop("/sim/mobilePRMG/indicated-distance-km", 0.0);
}

var start = func() {
 slowtimer.start();
 fasttimer.start();
}

var stop = func() {
 slowtimer.stop();
 fasttimer.stop();
 invalidate();
}


setlistener("/sim/mobilePRMG/enabled", func(p) {
 var enable = p.getValue();
 if(enable){
  start();
 }else{
  stop();
 }
}, 1, 0);
