var _run = 0;
var _t = 0.0;
var _starttime = nil;
var _startburn = nil;
var _wow = 1;


var FuelBurn = func() {
 var sum = 0.0;
 for(var ii = 0; ii < 5; ii = ii + 1){
  sum = sum + getprop("/fdm/jsbsim/propulsion/engine[" ~ ii ~ "]/fuel-used-lbs") * LB2KG;
 }
 return sum;
}


var _loop = func {

 if(getprop("/sim/freeze/replay-state")){
  return;
 }

 var t = getprop("/sim/time/elapsed-sec");
 var dt = t - _t;
 _t = t;

 var wow = 0;
 for(var ii = 0; ii < 28; ii = ii + 1){
  if(getprop("/gear/gear[" ~ ii ~ "]/wow")){
   wow = 1;
   break;
  }
 }
 if(!wow){
  setprop(("/sim/statistics/flight-sec"), getprop("/sim/statistics/flight-sec") + dt);
 }

 var run = 0;
 for(var ii = 0; ii < 4; ii = ii + 1){
  if(getprop("/fdm/jsbsim/propulsion/engine[" ~ ii ~ "]/set-running")){
   run = 1;
   break;
  }
 }
 if(run){
  setprop(("/sim/statistics/run-sec"), getprop("/sim/statistics/run-sec") + dt);
 }
 if(wow){
  if(run > _run){
   _starttime = getprop("/sim/statistics/run-sec");
   _startburn = FuelBurn();
  }else if(wow * (run < _run)){
   gui.popupTip("Engine runtime: " ~ sprintf("%.0f", (getprop("/sim/statistics/run-sec") - _starttime) / 60.0) ~ " minutes\nFuel used: " ~ sprintf("%.0f", FuelBurn() - _startburn) ~ " kg");
  }
  _run = run;
 }

 if(wow > _wow){
  setprop(("/sim/statistics/cycles"), getprop("/sim/statistics/cycles") + 1);
 }

 _wow = wow;
}


var _timer = maketimer(7.1, func{_loop()});


setlistener("/sim/signals/fdm-initialized", func(p) {
 if(p.getValue() == 0){
  return;
 }
 settimer(func {
  if(getprop("/sim/statistics/flight-sec") == nil){
   setprop("/sim/statistics/flight-sec", 0.0);
  }
  if(getprop("/sim/statistics/run-sec") == nil){
   setprop("/sim/statistics/run-sec", 0.0);
  }
  if(getprop("/sim/statistics/cycles") == nil){
   setprop("/sim/statistics/cycles", 0);
  }
  # SIC Trigger the listener the first time.
  setprop("/sim/statistics/run-sec", getprop("/sim/statistics/run-sec"));
  setprop("/sim/statistics/flight-sec", getprop("/sim/statistics/flight-sec"));
  _timer.start();
 }, 1.1);
});


setlistener("/sim/statistics/run-sec", func(p){
 var seconds = p.getValue();
 if(seconds == nil){
  seconds = 0.0;
 }
 var hours = int(seconds/3600);
 var minutes = int(math.mod(seconds/60, 60));
 seconds = int(math.mod(seconds, 60));
 setprop("/sim/statistics/run-string", sprintf("%0u:%02u:%02u", hours, minutes, seconds));
}, 1, 0);

setlistener("/sim/statistics/flight-sec", func(p){
 var seconds = p.getValue();
 if(seconds == nil){
  seconds = 0.0;
 }
 var hours = int(seconds/3600);
 var minutes = int(math.mod(seconds/60, 60));
 seconds = int(math.mod(seconds, 60));
 setprop("/sim/statistics/flight-string", sprintf("%0u:%02u:%02u", hours, minutes, seconds));
}, 1, 0);
