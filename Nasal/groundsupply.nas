var _standing_delayed = 1;
var _standing_tries = 0;

var _loop = func() {
 var standing = (
  getprop("/gear/gear[0]/wow") and
  getprop("/gear/gear[1]/wow") and
  getprop("/gear/gear[2]/wow") and
  (getprop("/gear/gear[0]/position-norm") > 0.999) and
  (getprop("/gear/gear[1]/position-norm") > 0.999) and
  (getprop("/gear/gear[2]/position-norm") > 0.999)
 );
 # This should stop pushback from disconnecting on every bump.
 if(standing){
  _standing_delayed = 1;
  _standing_tries = 0;
 }else{
  if(_standing_tries < 3){
   _standing_tries += 1;
  }else{
   _standing_delayed = 0;
  }
 }
 setprop("/sim/model/autopush/available", _standing_delayed);
 var avail = (
  (getprop("/velocities/groundspeed-kt") < 5.40)
 );
 setprop("/systems/ground-supply/chocks-available", avail);
 var secured = avail and (getprop("/systems/ground-supply/dialog/wheel-chocks") or 0);
 setprop("/fdm/jsbsim/gear/wheel-chocks", secured);
 setprop("/systems/ground-supply/available", secured);
 setprop(
  "/fdm/jsbsim/simulation/refill",
  (secured and (getprop("systems/ground-supply/dialog/refill") or 0)) or
  (getprop("systems/ground-supply/dialog/refill-cheat") or 0)
 );
 setprop(
  "/fdm/jsbsim/simulation/repair",
  (secured and (getprop("systems/ground-supply/dialog/repair") or 0)) or
  (getprop("systems/ground-supply/dialog/repair-cheat") or 0)
 );
 setprop(
  "/systems/ground-supply/stairs",
  secured and (getprop("/systems/ground-supply/dialog/stairs") or 0)
 );
 setprop(
  "/systems/ground-supply/air",
  secured and (getprop("/systems/ground-supply/dialog/air") or 0)
 );
 setprop(
  "/fdm/jsbsim/propulsion/air/source/APU_ASU/ASU-attached",
  secured and (getprop("/systems/ground-supply/dialog/air") or 0)
 );
 setprop(
  "/fdm/jsbsim/propulsion/air/source/APU_ASU/ASU-attached",
  secured and (getprop("/systems/ground-supply/dialog/air") or 0)
 );
 setprop(
  "/fdm/jsbsim/systems/elec/AC115/GPU/U-V",
  secured * (getprop("/systems/ground-supply/dialog/electrical") or 0) * 115.0
 );
 setprop(
  "/fdm/jsbsim/propulsion/hot-air-unit",
  secured and (getprop("/systems/ground-supply/dialog/hot-air-unit") or 0)
 );
 setprop(
  "/fdm/jsbsim/propulsion/hot-air-unit-temp-degC",
  math.max(
   (getprop("/systems/ground-supply/dialog/hot-air-unit-temp-degC") or 0.0),
   getprop("/fdm/jsbsim/atmosphere/T-degC")
  )
 );
 setprop(
  "/fdm/jsbsim/simulation/external-actuation",
  secured and (getprop("systems/ground-supply/dialog/external-actuation") or 0)
 );
 setprop("/payload/aircraft-weight-kg", getprop("/fdm/jsbsim/inertia/weight-lbs") * LB2KG);
 calcWeight();
 calcWeightLanding();
}

var _timer = maketimer(1.02, func{_loop()});


setlistener("/sim/signals/fdm-initialized", func(p) {
 if(p.getValue() == 0){
  return;
 }
 _timer.start();
});


var numweights = 8;

var calcWeight = func() {
 var sum = 0.0;
 for(var iweight = 0; iweight < numweights; iweight += 1){
  sum += getprop("/payload/weight[" ~ iweight ~ "]/weight-lb");
 }
 sum *= LB2KG;
 setprop("/payload/payload-weight-kg", sum);
}

var calcWeightLanding = func() {
 # Empty CG x = 2.610 m, see "mass.xml".
 var weight = 0.0;
 weight += (getprop("fdm/jsbsim/inertia/empty-weight-lbs") + getprop("fdm/jsbsim/inertia/pointmass-weight-lbs[8]"));
 var X =
  (getprop("fdm/jsbsim/inertia/empty-weight-lbs") + getprop("fdm/jsbsim/inertia/pointmass-weight-lbs[8]")) *
  2.610 * M2IN;
 # Ca. 2142 kg in feed tanks
 for(var itank = 0; itank < 4; itank += 1){
  weight += 2142.0 * KG2LB;
  X +=
   2142.0 * KG2LB *
   getprop("fdm/jsbsim/propulsion/tank[" ~ itank ~ "]/x-position");
 }
 # Oil tanks
 for(var ioil = 14; ioil < 19; ioil += 1){
  weight += getprop("fdm/jsbsim/propulsion/tank[" ~ ioil ~ "]/contents-lbs");
  X +=
   getprop("fdm/jsbsim/propulsion/tank[" ~ ioil ~ "]/contents-lbs") *
   getprop("fdm/jsbsim/propulsion/tank[" ~ ioil ~ "]/x-position");
 }
 # Payload
 for(var iload = 0; iload < 8; iload += 1){
  weight += getprop("fdm/jsbsim/inertia/pointmass-weight-lbs[" ~ iload ~ "]");
  X +=
   getprop("fdm/jsbsim/inertia/pointmass-weight-lbs[" ~ iload ~ "]") *
   getprop("fdm/jsbsim/inertia/pointmass-location-X-inches[" ~ iload ~ "]");
 }
 X /= weight;
 X = 100.0 * (X - getprop("fdm/jsbsim/metrics/aero-rp-x-in")) / (12 * getprop("fdm/jsbsim/metrics/cbarw-ft"));
 # AeroRP at 40%MAC, see "metrics.xml".
 X += 40.0;
 setprop("payload/landing-weight-kg", weight * LB2KG);
 setprop("payload/landing-cg-pMAC", X);
}

var calcDialogFuelPayload = func() {
 setprop("systems/ground-supply/dialog/fuel/total-kg", getprop("systems/ground-supply/dialog/fuel/main-kg") +
                                                       getprop("systems/ground-supply/dialog/fuel/t1-kg") +
                                                       getprop("systems/ground-supply/dialog/fuel/t2-kg") +
                                                       getprop("systems/ground-supply/dialog/fuel/t8-kg"));
 var levels =  Tu144D.CalculateFuel(
        getprop("/systems/ground-supply/dialog/fuel/main-kg"),
        getprop("/systems/ground-supply/dialog/fuel/t1-kg"),
        getprop("/systems/ground-supply/dialog/fuel/t2-kg"),
        getprop("/systems/ground-supply/dialog/fuel/t8-kg")
 );
 # Empty CG x = 2.610 m, see "mass.xml".
 var weight = 0.0;
 var payload_weight = 0.0;
 weight += (getprop("fdm/jsbsim/inertia/empty-weight-lbs") + getprop("fdm/jsbsim/inertia/pointmass-weight-lbs[8]"));
 var X =
  (getprop("fdm/jsbsim/inertia/empty-weight-lbs") + getprop("fdm/jsbsim/inertia/pointmass-weight-lbs[8]")) *
  2.610 * M2IN;
 # Ca. 2142 kg in feed tanks
 for(var itank = 0; itank < 4; itank += 1){
  weight += levels[itank] * KG2LB;
  X +=
   levels[itank] * KG2LB *
   getprop("fdm/jsbsim/propulsion/tank[" ~ itank ~ "]/x-position");
 }
 # Oil tanks
 for(var ioil = 14; ioil < 19; ioil += 1){
  weight += getprop("fdm/jsbsim/propulsion/tank[" ~ ioil ~ "]/contents-lbs");
  X +=
   getprop("fdm/jsbsim/propulsion/tank[" ~ ioil ~ "]/contents-lbs") *
   getprop("fdm/jsbsim/propulsion/tank[" ~ ioil ~ "]/x-position");
 }
 # Payload
 for(var iload = 0; iload < 8; iload += 1){
  payload_weight += getprop("systems/ground-supply/dialog/payload/payload-kg[" ~ iload ~ "]");
  weight += getprop("systems/ground-supply/dialog/payload/payload-kg[" ~ iload ~ "]") * KG2LB;
  X +=
   getprop("systems/ground-supply/dialog/payload/payload-kg[" ~ iload ~ "]") * KG2LB *
   getprop("fdm/jsbsim/inertia/pointmass-location-X-inches[" ~ iload ~ "]");
 }
 var X_taxi = X;
 var weight_taxi = weight;
 X /= weight;
 X = 100.0 * (X - getprop("fdm/jsbsim/metrics/aero-rp-x-in")) / (12 * getprop("fdm/jsbsim/metrics/cbarw-ft"));
 # AeroRP at 40%MAC, see "metrics.xml".
 X += 40.0;
 setprop("systems/ground-supply/dialog/payload/landing-weight-kg", weight * LB2KG);
 setprop("systems/ground-supply/dialog/payload/total-kg", payload_weight);
 setprop("systems/ground-supply/dialog/payload/landing-cg-pMAC", X);
 for(var itank = 4; itank < 14; itank += 1){
  weight_taxi += levels[itank] * KG2LB;
  X_taxi +=
   levels[itank] * KG2LB *
   getprop("fdm/jsbsim/propulsion/tank[" ~ itank ~ "]/x-position");
 }
 X_taxi /= weight_taxi;
 X_taxi = 100.0 * (X_taxi - getprop("fdm/jsbsim/metrics/aero-rp-x-in")) / (12 * getprop("fdm/jsbsim/metrics/cbarw-ft"));
 # AeroRP at 40%MAC, see "metrics.xml".
 X_taxi += 40.0;
 setprop("systems/ground-supply/dialog/payload/taxi-weight-kg", weight_taxi * LB2KG);
 setprop("systems/ground-supply/dialog/payload/taxi-cg-pMAC", X_taxi);
}

foreach(var tanks; ["main-kg", "t1-kg", "t2-kg", "t8-kg"]){
 setlistener("systems/ground-supply/dialog/fuel/" ~ tanks, func() {
  calcDialogFuelPayload();
 }, 1, 0);
}

for(var iload = 0; iload < 8; iload += 1){
 setlistener("systems/ground-supply/dialog/payload/payload-kg[" ~ iload ~ "]", func() {
  calcDialogFuelPayload();
 }, 1, 0);
}

var calcFlightLevels = func() {
 var routeDistance = getprop("systems/ground-supply/dialog/fuelprompt/distance-km");
 var routeFuel = math.round(15.67 * routeDistance + 50.0, 100.0);
 setprop("systems/ground-supply/dialog/fuelprompt/route-fuel-kg", routeFuel);
 setprop("systems/ground-supply/dialog/fuelprompt/takeoff-fuel-kg", routeFuel + 13700);
 setprop("systems/ground-supply/dialog/fuelprompt/fuel-kg", routeFuel + 13700 + 2700);
 # M2.0 is ~2124.75 km/h at 16 km.
 # ~ 500 km to climb.
 var endClimb = routeDistance - 500.0;
 setprop("systems/ground-supply/dialog/fuelprompt/end-of-climb-km", endClimb);
 var timeClimb = 500.0 / (0.5 * 2124.75);
 var hoursClimb = int(timeClimb);
 var minutesClimb = int(math.mod(timeClimb * 60.0, 60));
 var secondsClimb = int(math.mod(timeClimb * 3600.0, 60));
 setprop("systems/ground-supply/dialog/fuelprompt/time-climb-string", sprintf("%0u:%02u:%02u", hoursClimb, minutesClimb, secondsClimb));
 # ~ 600 km to brake and descend.
 var cruiseDist = endClimb - getprop("systems/ground-supply/dialog/fuelprompt/end-of-cruise-km");
 var timeCruise = cruiseDist / 2124.75;
 var hoursCruise = int(timeCruise);
 var minutesCruise = int(math.mod(timeCruise * 60.0, 60));
 var secondsCruise = int(math.mod(timeCruise * 3600.0, 60));
 setprop("systems/ground-supply/dialog/fuelprompt/time-cruise-string", sprintf("%0u:%02u:%02u", hoursCruise, minutesCruise, secondsCruise));
 var timeBrakingDescent = getprop("systems/ground-supply/dialog/fuelprompt/end-of-cruise-km") / (0.5 * 2124.75);
 var hoursBrakingDescent = int(timeBrakingDescent);
 var minutesBrakingDescent = int(math.mod(timeBrakingDescent * 60.0, 60));
 secondsBrakingDescent = int(math.mod(timeBrakingDescent * 3600.0, 60));
 var timeETA = timeClimb + timeCruise + timeBrakingDescent;
 var hoursETA = int(timeETA);
 var minutesETA = int(math.mod(timeETA * 60.0, 60));
 secondsETA = int(math.mod(timeETA * 3600.0, 60));
 setprop("systems/ground-supply/dialog/fuelprompt/time-ETA-string", sprintf("%0u:%02u:%02u", hoursETA, minutesETA, secondsETA));
 setprop("systems/ground-supply/dialog/fuelprompt/time-braking-descent-string", sprintf("%0u:%02u:%02u", hoursBrakingDescent, minutesBrakingDescent, secondsBrakingDescent));
 var stepDist = cruiseDist / 5;
 for(var ii = 0; ii < 4; ii += 1){
  setprop("systems/ground-supply/dialog/fuelprompt/level-change-km[" ~ ii ~ "]", endClimb - (ii + 1) * stepDist);
 }
 var climbRate = (18000.0 - 16000.0) / (3600.0 * cruiseDist / 2124.75);
 setprop("systems/ground-supply/dialog/fuelprompt/constant-climb-m_s", climbRate);
}
setlistener("systems/ground-supply/dialog/fuelprompt/distance-km", calcFlightLevels, 1, 0);
setlistener("systems/ground-supply/dialog/fuelprompt/end-of-cruise-km", calcFlightLevels, 0, 0);

# FGBUG Broken!
#for(var iweight = 0; iweight < numweights; iweight += 1){
# setlistener("/payload/weight[" ~ iweight ~ "]/weight-lb", func() {
#  calcWeight();
# });
#}
