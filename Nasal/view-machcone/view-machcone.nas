# Mach cone calculator for supersonic aircraft sounds
# Usage: see Nasal/view-machcone/README.view-machcone

# A T T E N T I O N !
# WHEN USING IN MP MODEL, MAKE SURE TO ADD THE FOLLOWING LINE:
# machconeTimer.stop();
# TO THE <nasal><unload> SCRIPT OF THE MP MODEL, OTHERWISE THE TIMER
# WILL KEEP RUNNING AFTER THE MODEL DISAPPEARS AND LEAK RESOURCES!

# XXX: The calculations here are inherently unstable because of
# XXX: properties, Nasal etc. being updated out of sync. The problem
# XXX: may disappear if this model is ported to FG or SG C++


# BEGIN SETTINGS

# Property tree root
# CHANGE THIS WHEN COPYING INTO model.xml!
var model = props.globals; # Local aircraft property tree
#var model = cmdarg();     # Model subtree (for use with AI/MP
#                          # <nasal><load>). If this line is already
#                          # there, simply comment out both
#                          #"var model=" here.

# Checking interval
var machconeDt = 0.05;

# Shift the Mach cone from the model's center, to the front along the
# speed vector. Tune for convincing results. A good initial value is
# "minus the x coordinate of nose tip" or slightly behind it
#var machconeShiftForward = 0.0;
var machconeShiftForward = 36.5;

# Length of the shocks (an order of aircraft length)
var machconeShockLength = 65.7;
# Play separate shocks for nose and tail
# (only works for large aircraft and low machconeDt)
var machconeDoubleShock = 1;
# Length of the shock + remaining shock sound after the shock
# Tu-144D: 1.25 s for M = 2.0 (~2124.75 km/h at 16 km)
var machconeSoundLength = 738.0;

# Mach number property. If you have MP sounds, either use here another
# property that is synchronized over MP, aliased in -set.xml to the
# Mach number, or alias velocities/mach of the MP model back to that
# MP property using <nasal><load>
#var machconeMachProp = model.getNode("velocities/mach");
var machconeMachProp = model.getNode("engines/engine[9]/rpm");

# END SETTINGS


var machconePos = nil;
var machconeLatProp = model.getNode("position/latitude-deg");
var machconeLonProp = model.getNode("position/longitude-deg");
var machconeAltProp = model.getNode("position/altitude-ft");

var machconeSpeedNorthProp = model.getNode("velocities/speed-north-fps");
var machconeSpeedEastProp = model.getNode("velocities/speed-east-fps");
var machconeSpeedDownProp = model.getNode("velocities/speed-down-fps");
var machconeLocalPosSpeed = ((machconeSpeedNorthProp != nil) and  (machconeSpeedEastProp != nil) and (machconeSpeedDownProp != nil));
var machconeSpeedBodyUProp = model.getNode("velocities/uBody-fps");
var machconeSpeedBodyVProp = model.getNode("velocities/vBody-fps");
var machconeSpeedBodyWProp = model.getNode("velocities/wBody-fps");
# XXX: This property is named differently in SP and MP, because FG can't into consistency...
var machconeHeadingProp = model.getNode("orientation/heading-deg");
if (machconeHeadingProp == nil) {
  machconeHeadingProp = model.getNode("orientation/true-heading-deg");
}
var machconePitchProp = model.getNode("orientation/pitch-deg");
var machconeRollProp = model.getNode("orientation/roll-deg");
var machconeSpeedNorth = nil;
var machconeSpeedEast = nil;
var machconeSpeedDown = nil;
var machconeSpeedNorm = nil;
var machconeSa = nil;
var machconeCa = nil;
var machconeSb = nil;
var machconeCb = nil;
var machconeSc = nil;
var machconeCc = nil;

var machconeViewPos = nil;
var machconeViewToAcNorth = nil;
var machconeViewToAcEast = nil;
var machconeViewToAcUp = nil;

var machconeLonD2M = nil;
var machconeLatD2M = geo.ERAD * math.pi / 180;

var machconeMachAngle = nil;
var machconeViewBehindCone = nil;
var machconeCrossX = nil;
var machconeCrossY = nil;
var machconeCrossZ = nil;
var machconeCrossNorm = nil;
var machconeInCone = 0;

# The length of shocks is ca. 0.16 of the aircraft, but we use 0.5 to give Nasal enough time to cycle them
var machconeNoseShockEnd = machconeShockLength * 0.5;
var machconeTailShockEnd = machconeShockLength * 1.5;
var machconeSoundSlope = 0.25 / (machconeSoundLength - machconeTailShockEnd);

var machconeInConeProp = model.getNode("sim/current-view/viewer-in-mach-cone", 1);
var machconeShockVolumeProp = model.getNode("sim/current-view/viewer-shock-wave-norm", 1);

var machconeClockProp = model.getNode("/sim/time/steady-clock-sec");
var machconeClock = nil;
var _machconeClock = nil;
var machconeFrameTime = nil;

var machconeTimer = maketimer(machconeDt, func(){

  # Skip calculation in subsonic
  if (machconeMachProp.getValue() < 1.0) {
    machconeInConeProp.setValue(1);
    machconeShockVolumeProp.setValue(0.0);
    return;
  }

  # Aircraft's speed vector
  if (machconeLocalPosSpeed) {
    machconeSpeedNorth = machconeSpeedNorthProp.getValue();
    machconeSpeedEast = machconeSpeedEastProp.getValue();
    machconeSpeedDown = machconeSpeedDownProp.getValue();
  } else {
    # If not available as properties (e.g. in AI/MP model), restore
    # them from body-frame speeds
    machconeSa = math.sin(machconeHeadingProp.getValue() * D2R);
    machconeCa = math.cos(machconeHeadingProp.getValue() * D2R);
    machconeSb = math.sin(machconePitchProp.getValue() * D2R);
    machconeCb = math.cos(machconePitchProp.getValue() * D2R);
    machconeSc = math.sin(machconeRollProp.getValue() * D2R);
    machconeCc = math.cos(machconeRollProp.getValue() * D2R);
    # The sign of W is inverted here because then 1) UVW and NED match at zero angles 2) the heading and angle "c" have the same sign
    machconeSpeedNorth =  machconeSpeedBodyUProp.getValue() * machconeCa * machconeCb + machconeSpeedBodyVProp.getValue() * (machconeCa * machconeSb * machconeSc - machconeSa * machconeCc) - machconeSpeedBodyWProp.getValue() * (machconeCa * machconeSb * machconeCc + machconeSa * machconeSc);
    machconeSpeedEast  =  machconeSpeedBodyUProp.getValue() * machconeSa * machconeCb + machconeSpeedBodyVProp.getValue() * (machconeSa * machconeSb * machconeSc + machconeCa * machconeCc) - machconeSpeedBodyWProp.getValue() * (machconeSa * machconeSb * machconeCc - machconeCa * machconeSc);
    machconeSpeedDown  = -machconeSpeedBodyUProp.getValue() * machconeSb              + machconeSpeedBodyVProp.getValue() * machconeCb * machconeSc                                          - machconeSpeedBodyWProp.getValue() * machconeCb * machconeCc;
  }
  machconeSpeedNorm = math.sqrt(machconeSpeedNorth * machconeSpeedNorth + machconeSpeedEast * machconeSpeedEast + machconeSpeedDown * machconeSpeedDown);

  # View vector (vector from viewer to the aircraft's VRP):
  machconeLonD2M = math.cos(machconeLatProp.getValue() * D2R) * machconeLatD2M;
  machconeViewPos = geo.viewer_position();
  if (machconeLocalPosSpeed) {
    #  We get aircraft position from Nasal when we can, because
    # properties are updated out of sync, making the vector jump all over the place
    machconePos = geo.aircraft_position();
  }
  machconeViewToAcNorth = geo.normdeg180(
    (machconeLocalPosSpeed ? machconePos.lat() : machconeLatProp.getValue())
    - machconeViewPos.lat()
  ) * machconeLatD2M;
  machconeViewToAcEast = geo.normdeg180(
    (machconeLocalPosSpeed ? machconePos.lon() : machconeLonProp.getValue())
    - machconeViewPos.lon()
  ) * machconeLonD2M;
  machconeViewToAcUp =
    (machconeLocalPosSpeed ? machconePos.alt() : machconeAltProp.getValue() * FT2M)
    - machconeViewPos.alt();

  # Determine if the viewer is within Mach angle,
  # for which we calculate the distance along the speed fector
  # betweeen the viewer and the Mach cone
  machconeMachAngle = math.asin(1.0 / machconeMachProp.getValue());
  # The "parallel distance" from viewer to VRP parallel to the speed
  # vector is the projection of view vector onto speed vector, which
  # is the dot product divided by machconeSpeedNorm (we divide later)
  machconeViewBehindCone = (machconeSpeedNorth * machconeViewToAcNorth + machconeSpeedEast * machconeViewToAcEast - machconeSpeedDown * machconeViewToAcUp);
  # Skip the slant calculation for angles greater than 89 deg to avoid
  # instability
  if (machconeMachAngle < 1.553343) {
    # The "perpendicular distance" from viewer VRP perpendicular to
    # the speed vector is the cross product of view and speed vectors
    # divided by machconeSpeedNorm (again, we divide later)
    machconeCrossX = machconeSpeedEast * machconeViewToAcUp + machconeSpeedDown * machconeViewToAcEast;
    machconeCrossY = -machconeSpeedDown * machconeViewToAcNorth - machconeSpeedNorth * machconeViewToAcUp;
    machconeCrossZ = machconeSpeedNorth * machconeViewToAcEast - machconeSpeedEast * machconeViewToAcNorth;
    machconeCrossNorm = math.sqrt(machconeCrossX * machconeCrossX + machconeCrossY * machconeCrossY + machconeCrossZ * machconeCrossZ);
    # Then the distance between viewer and the surface of Mach cone
    # (which would be originating from VRP here, we shift later) is
    # the viewer's parallel distance to VRP minus the cone surface's
    # parallel distance to VRP at the same perpendicular distance
    machconeViewBehindCone -= machconeCrossNorm / math.tan(machconeMachAngle);
  }
  # And finally:
  # - convert to real distances by dividing by machconeSpeedNorm;
  # - shift the Mach cone forward from VRP along the speed vector
  #   (which just adds a constant here);
  # - compensate for the extra apparent forward shift of Mach cone
  #   at low FPS (probably because of properties being out of sync)
  machconeClock = machconeClockProp.getValue();
  machconeFrameTime = math.fmod((machconeClock - _machconeClock + 86400), 86400);
  _machconeClock = machconeClock;
  machconeViewBehindCone = machconeViewBehindCone / machconeSpeedNorm + machconeShiftForward - machconeSpeedNorm * FT2M * machconeFrameTime;
  # Hide the instability of view vector by using hysteresis, larger
  # value for MP
  if (machconeLocalPosSpeed) {
    machconeInCone = (machconeViewBehindCone > (machconeInCone ? 0.1 : -0.1));
  } else {
    machconeInCone = (machconeViewBehindCone > (machconeInCone ? 0.25 : -0.25));
  }
  machconeInConeProp.setValue(machconeInCone);

  # Calculate shockwave and noise intensity from the distance
  if (machconeInCone) {
    if (machconeViewBehindCone > machconeSoundLength) {
      machconeShockVolumeProp.setValue(0.0);
    } else if (machconeViewBehindCone < machconeTailShockEnd) {
      # 2 shocks with full pitch and volume with a gap with 1/4 pitch and volume
      if ((machconeViewBehindCone < machconeNoseShockEnd) or (machconeViewBehindCone > machconeShockLength)) {
        machconeShockVolumeProp.setValue(1.0);
      } else {
        machconeShockVolumeProp.setValue(machconeDoubleShock ? 0.25 : 1.0);
      }
    } else {
      # Fade out from 0.25 pitch and volume to 0
      machconeShockVolumeProp.setValue(0.25 - (machconeViewBehindCone - machconeTailShockEnd) * machconeSoundSlope);
    }
  } else {
    machconeShockVolumeProp.setValue(0.0);
  }
});

if (model == props.globals) {
  setlistener("/sim/signals/fdm-initialized", func(p) {
    if(p.getValue() == 0){
      return;
    }
    _machconeClock = machconeClockProp.getValue();
    machconeTimer.start();
  })
} else {
  settimer(func(){machconeTimer.start()}, 1);
}
