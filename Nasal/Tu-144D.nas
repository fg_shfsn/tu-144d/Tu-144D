# Tu-144D Nasal interface.
#
# This class handles some FlightGear initializations and provides
# the tools for scripting states or procedures in Nasal.

# FIXME Replace getprop and setprop with nodes!

# TODO When animating the cockpit or adding switch sounds, simply
# TODO replace setprop() with a func that checks if the property will
# TODO change, avoids redundant setprop() and sets it with a delay.


# @brief Print message to event log.
# @param emphasis Emphasis: 0: none (default), 1: green, 2: amber, 3: red.
var chat = func(text, emphasis = 0) {
 if(emphasis == 1){
  screen.log.write(text, 0.416, 1.0, 0.196);
 }else if(emphasis == 2){
  screen.log.write(text, 1.0, 0.416, 0.196);
 }else if(emphasis == 3){
  screen.log.write(text, 1.0, 0.196, 0.416);
 }else{
  screen.log.write(text, 0.969, 0.953, 0.969);
 }
}


# @brief Calculate takeoff elevator trim.
# @param CG (default: current CG).
# 80 mm for CG < 40.5 %MAC, 90 mm for higher.
var TakeoffTrim = func (cg = nil) {
 if(cg == nil){
  cg = getprop("fdm/jsbsim/propulsion/fuel/trim/cg-pMAC");
 }
 return (80.0 + (cg > 40.5) * 10.0);
}

# @brief Calculate decision speed (V1).
# @param wetRunway Taking off from a wet runway (default: current value set in Crew Orders).
var V1 = func(wetRunway = nil) {
 if(wetRunway == nil){
  wetRunway = getprop("sim/crew/orders/takeoff/wet-runway") * LB2KG;
 }
 return (245.0 - (wetRunway * 25.0));
}

# @brief Calculate rotation speed (V_R).
# @param tow Take-off weight, kg (default: current weight).
var Vr = func(tow = nil) {
 return (320.0 + _deltaVrV2(tow));
}

# @brief Calculate safety speed (V_2).
# @param tow Take-off weight, kg (default: current weight).
var V2 = func(tow = nil) {
 return (380.0 + _deltaVrV2(tow));
}

# @brief Internal: calculate the increase of V_R and V_2 for higher-than-normal TOW.
# 0 km/h for TOW=180 tonnes, +20 km/h for 200.
# @param tow Take-off weight, kg (default: current weight).
var _deltaVrV2 = func(tow = nil) {
 if(tow == nil){
  tow = getprop("fdm/jsbsim/inertia/weight-lbs") * LB2KG;
 }
 return math.round(20.0 * math.min(math.max((tow - 180000.0)/(200000.0 - 180000.0), 0.0), 1.0), 1.0);
}

# @brief Calculate reference speed (V_ref).
# Approach speed V_app = V_ref.
# 290 km/h with canard, 330 without.
# + 80 km/h when landing with MTOW (e.g. right after take-off).
# @param canard Canard extension, 0.0..86.0 degrees (default: current position).
# @param lw Landing weight, kg (only affects above MLW, default: current weight).
var Vref = func(canard = nil, lw = nil) {
 if(canard == nil){
  canard = getprop("fdm/jsbsim/fcs/canard/pos-deg");
 }
 if(lw == nil){
  lw = getprop("fdm/jsbsim/inertia/weight-lbs") * LB2KG;
 }
 # For Tu-144 sans D, the increase interval would be 120000..195000 kg instead of 132000..207000.
 return math.round(290.0 + 40.0 * (87.0 - canard)/87.0 + 80.0 * math.min(math.max((lw - 132000.0)/(207000.0 - 132000.0), 0.0), 1.0), 1.0);
}

# @brief Engine derate index for takeoff.
# -1: bump     alpha_v = 90.0, n ~ 95.5  (unused)
# 0:  rated    alpha_v = 85.0, n ~ 87.0
# 1:  derated  alpha_v = 62.0, n ~ 72.0  for TOW < 180 tonnes and air temp < 20 degC,
# @param temp Air temperature, degC (default: current temperature).
# @param tow Take-off weight, kg (default: current weight).
var TakeoffDerateIndex = func(temp = nil, tow = nil) {
 if(temp == nil){
  temp = getprop("fdm/jsbsim/atmosphere/T-degC");
 }
 if(tow == nil){
  tow = getprop("fdm/jsbsim/inertia/weight-lbs") * LB2KG;
 }
 return (
  (tow < 180000.0) and
  (temp < 20.0)
 );
}

# @brief Power lever angle (degrees) for takeoff.
# @param temp Air temperature, degC (default: current temperature).
# @param tow Take-off weight, kg (default: current weight).
var TakeoffPLA = func(temp = nil, tow = nil) {
 return [90.0, 85.0, 62.0][TakeoffDerateIndex(temp, tow) + 1];
}

# @brief N (%RPM) for takeoff.
# @param temp Air temperature, degC (default: current temperature).
# @param tow Take-off weight, kg (default: current weight).
var TakeoffN = func(temp = nil, tow = nil) {
 return [95.5, 87.0, 72.0][TakeoffDerateIndex(temp, tow) + 1];
}

# @brief Thrust in kN for takeoff.
# @param temp Air temperature, degC (default: current temperature).
# @param tow Take-off weight, kg (default: current weight).
var TakeoffThrust = func(temp = nil, tow = nil) {
 return math.round((TakeoffPLA(temp, tow) / 90.0) * [20.0, 21.0][getprop("fdm/jsbsim/simulation/settings/with-RD-36-61") or 0], 1.0);
}

# @brief Calculate fuel levels with proper distribution.
# Returns array of fuel levels in kg per tank, size=14.
# @param main Fuel in main tanks (feed tanks filled first), kg.
# @param tanks1 Fuel in tanks 1 (coarse front trim), kg.
# @param tanks2 Fuel in tanks 2 (fine front trim), kg.
# @param tank8 Fuel in tank 8 (tail trim), kg.
CalculateFuel = func(main, tanks1, tanks2, tank8) {
 # FIXME This fueling panel logic belongs to JSBSim.
 var maxmain = 84462.0;
 var maxfeed = 2142.0;
 var maxtanks1 = 7392.0;
 var maxtanks2 = 10500.0;
 var maxtank8 = 11760.0;
 if(main > maxmain){
  main = maxmain;
 }
 var feedfill = main / 4;
 if(feedfill > maxfeed){
  feedfill = maxfeed;
 }
 main -= 4 * feedfill;
 # fill = max_kg / (21798 + 2 * 7770 + 2 * 7602 + 11592 + 11760)
 var tanks3 = main * 0.2872;
 var tank4 = main * 0.1024;
 var tank5 = main * 0.1002;
 var tank6 = main * 0.1527;
 var tank7 = main * 0.1549;
 if(tanks1 > maxtanks1){
  tanks1 = maxtanks1;
 }
 if(tanks2 > maxtanks2){
  tanks2 = maxtanks2;
 }
 if(tank8 > maxtank8){
  tank8 = maxtank8;
 }
 var output = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
 for(var ii = 0; ii < 4; ii +=1){
  output[ii] = feedfill;
 }
 output[4] = tanks1;
 output[5] = tanks2;
 output[6] = tanks3;
 output[7] = tank4;
 output[8] = tank4;
 output[9] = tank5;
 output[10] = tank5;
 output[11] = tank6;
 output[12] = tank7;
 output[13] = tank8;
 return output;
}

# @brief Set fuel levels.
# @param levels Array of fuel levels in kg for each tank, size=14.
SetFuelLevels = func(levels) {
 for(var ii = 0; ii < 14; ii +=1){
  setprop("/consumables/fuel/tank[" ~ ii ~ "]/level-kg", levels[ii]);
 }
}

# @brief Set fuel levels with proper distribution.
# @param main Fuel in main tanks (feed tanks filled first), kg.
# @param tanks1 Fuel in tanks 1 (coarse front trim), kg.
# @param tanks2 Fuel in tanks 2 (fine front trim), kg.
# @param tank8 Fuel in tank 8 (tail trim), kg.
SetFuel = func(main, tanks1, tanks2, tank8) {
 # FIXME Central fueling belongs to JSBSim.
 SetFuelLevels(CalculateFuel(main, tanks1, tanks2, tank8));
}

# @brief Set payload levels.
# @param arguments are payload levels in kg (see -set.xml for description and limits).
SetPayload = func(vest0, cab0, cab1, vest1, cab2, vest2, cargo0, cargo1) {
 setprop("/payload/weight[0]/weight-lb", vest0 * KG2LB);
 setprop("/payload/weight[1]/weight-lb", cab0 * KG2LB);
 setprop("/payload/weight[2]/weight-lb", cab1 * KG2LB);
 setprop("/payload/weight[3]/weight-lb", vest1 * KG2LB);
 setprop("/payload/weight[4]/weight-lb", cab2 * KG2LB);
 setprop("/payload/weight[5]/weight-lb", vest2 * KG2LB);
 setprop("/payload/weight[6]/weight-lb", cargo0 * KG2LB);
 setprop("/payload/weight[7]/weight-lb", cargo1 * KG2LB);
}


# @brief Set target trim.
# Set target trim.
# No actual trimming, unless during the first few seconds of FDM run
# with /fdm/jsbsim/simulation/settings/trim != 0.
# @param column Column trim, max -132..+150.0 mm.
# @param wheel Wheel trim, max +-25 deg.
# @param pedal Pedal trim, max +48.0 mm.
TrimTarget = func (column, yoke, pedal) {
 setprop("/fdm/jsbsim/fcs/panel/pot/pitch-trim/set-mm", math.min(math.max(column, -132.0), 150.0));
 setprop("/fdm/jsbsim/fcs/panel/switch/roll-trim_set-deg", math.min(math.max(yoke, -25.0), 25.0));
 setprop("/fdm/jsbsim/fcs/panel/switch/yaw-trim_set-deg", math.min(math.max(pedal, -48.0), 48.0));
}

# @brief Target trim for parking.
TrimTargetParking = func () {
 TrimTarget(150.0, 0.0, 0.0);
}

# @brief Target trim for takeoff, as per POH.
TrimTargetTakeoff = func () {
 TrimTarget(TakeoffTrim(), 0.0, 0.0);
}

# @brief Trim to the target trim.
# Trim to the target trim. Takes 65 seconds.
TrimDo = func () {
 # FIXME Roll and yaw switches not moving, requires extra logic right here.
 setprop("/fdm/jsbsim/fcs/panel/pot/pitch-trim/set", 1);
 setprop("/fdm/jsbsim/fcs/panel/switch/roll-trim_set", 1);
 setprop("/fdm/jsbsim/fcs/panel/switch/yaw-trim_set", 1);
 settimer(func {
  setprop("/fdm/jsbsim/fcs/panel/pot/pitch-trim/set", 0);
  setprop("/fdm/jsbsim/fcs/panel/switch/roll-trim_set", 0);
  setprop("/fdm/jsbsim/fcs/panel/switch/yaw-trim_set", 0);
 }, 65.0);
}


# @brief Set throttle.
# @param eng Engine, 0..3.
# @param throttle Throttle, 0..90 degrees.
# @param disconnect_at  Always disconnect AT, even if throttle is the same.
Throttle = func (eng, throttle, disconnect_at = 1) {
 var diff = (throttle / 90.0) - getprop("/controls/engines/engine[" ~ eng ~ "]/throttle");
 setprop("/controls/engines/engine[" ~ eng ~ "]/throttle", throttle / 90.0);
 setprop("/fdm/jsbsim/fcs/throttle-moving-up[" ~ eng ~ "]", (diff > 0.001));
 setprop("/fdm/jsbsim/fcs/throttle-moving-down[" ~ eng ~ "]", ((diff < 0.001) or disconnect_at));
 settimer(func{
  setprop("/fdm/jsbsim/fcs/throttle-moving-up[" ~ eng ~ "]", 0);
  setprop("/fdm/jsbsim/fcs/throttle-moving-down[" ~ eng ~ "]", 0);
 }, 0.5);
}

# @brief Set throttle for all engines.
# @param throttle Throttle, 0..90 degrees.
# @param disconnect_at  Always disconnect AT, even if throttle is the same.
ThrottleAll = func (throttle, disconnect_at = 1) {
 for(var ii = 0; ii < 4; ii += 1){
  Throttle(ii, throttle, disconnect_at);
 }
}

# @brief Set reverse.
# @param angle Reverse lever angle 0..30 deg.
ReversersAll = func (angle) {
 for(var ii = 0; ii < 4; ii += 3){
  setprop("/fdm/jsbsim/propulsion/engine[" ~ ii ~ "]/panel/lever/reverser/pos-deg", angle);
 }
}


# @brief Canard power switch.
CanardPower = func (on) {
 setprop("/fdm/jsbsim/fcs/panel/switch/canard-power", on);
}

# @brief Canard command.
# @param cmd 0: Retract, latched (default), 1: Retract, 2: Stop, 3: Extend.
CanardCmd = func (cmd) {
 setprop("/fdm/jsbsim/fcs/panel/switch/canard", cmd);
}


# @brief Cone power switch.
ConePower = func (on) {
 setprop("/fdm/jsbsim/fcs/panel/switch/cone-power", on);
}

# @brief Cone command.
# @param cmd 0: Retract, latched (default) 1: Retract (default), 2: Takeoff, 3: Landing.
ConeCmd = func (cmd) {
 setprop("/fdm/jsbsim/fcs/panel/switch/cone", cmd);
}

# @brief Gear command.
# @param cmd -2: Retract, +-1: Neutral, 0: Neutral latched (default), 2: Extend.
GearCmd = func (cmd) {
 setprop("/fdm/jsbsim/gear/panel/lever/gear", cmd);
}

# @brief Gear command increment.
# Increments the position of gear switch, with sanity check.
GearCmdInc = func (inc) {
 var old = getprop("/fdm/jsbsim/gear/panel/lever/gear");
 var cmd = math.min(math.max(old + inc, -2), 2);
 setprop("/fdm/jsbsim/gear/panel/lever/gear", cmd);
}

# @brief Steering profile.
# @param profile 0: Off, 1: 8 deg, 2: 60 deg.
Steering = func (profile) {
 setprop("/fdm/jsbsim/gear/panel/switch/steering", math.min(profile, 1));
 setprop("/fdm/jsbsim/gear/panel/switch/steering-60deg", (profile == 2));
}


# @brief Enable/disable start brake.
Startbrake = func (on) {
 # XXX Non-JSBSim.
 setprop("/controls/gear/brake-parking", on);
}


# @brief Enable/disable parking brake.
Parkingbrake = func (on) {
 setprop("/fdm/jsbsim/gear/panel/switch/parking-brake", on);
}


# @brief Deploy parachute.
# @param cmd 0: Drop, 1: Deploy.
Chute = func (cmd) {
 if(cmd){
  setprop("/fdm/jsbsim/fcs/panel/button/chute-deploy", 1);
  settimer(func {
   setprop("/fdm/jsbsim/fcs/panel/button/chute-deploy", 0);
  }, 2.0);
 }else{
  setprop("/fdm/jsbsim/fcs/panel/button/chute-drop", 1);
  settimer(func {
   setprop("/fdm/jsbsim/fcs/panel/button/chute-drop", 0);
  }, 2.0);
 }
}


# @brief Engine cutoff.
# @param eng Engine number 0..3.
Cutoff = func(eng, cutoff) {
 setprop("/fdm/jsbsim/propulsion/engine[" ~ eng ~ "]/panel/lever/cutoff", cutoff);
}

# @brief Set cutoff for all engines.
CutoffAll = func(cutoff) {
 for(var ii = 0; ii < 4; ii += 1){
  Cutoff(ii, cutoff);
 }
}

# @brief Turn APD on/off and select engine.
# @param eng 0..3: Engine 0..3, -1: off.
APDSelect = func (eng) {
 if(eng == -1){
  setprop("/fdm/jsbsim/propulsion/air/APD/panel/switch/master", 0);
  setprop("/fdm/jsbsim/propulsion/air/APD/panel/switch/ignition", 0);
 }else{
  setprop("/fdm/jsbsim/propulsion/air/APD/panel/switch/master", 1);
  setprop("/fdm/jsbsim/propulsion/air/APD/panel/switch/ignition", 1);
 }
 setprop("/fdm/jsbsim/propulsion/air/APD/panel/switch/engine-select", eng + 1);
}

# @brief Start engine on the ground.
# @param eng Engine 0..3.
# @param pump -1: backup, 0: off, 1: main.
# @param relight Relight. If not, start normally.
EngineStart = func (eng, pump, relight = 0) {
 Generator(eng, 0);
 AirconOff();
 Throttle(eng, 0.0);
 FireValve(eng, 1);
 Cutoff(eng, 0);
 Boostpump(eng, -1);
 if(!relight){
  APDSelect(eng);
  setprop("/fdm/jsbsim/propulsion/air/APD/panel/button/start", 1);
  settimer(func {
   setprop("/fdm/jsbsim/propulsion/air/APD/panel/button/start", 0);
  }, 3.0);
  settimer(func {
   Boostpump(eng, pump);
   Generator(eng, 1);
   AirconEngines(1);
   APDSelect(-1);
  }, 82.0);
 }else{
  # FIXME Disconnect AT, save throttle, relight, restore throttle and reconnect AT
  APDSelect(-1);
  setprop("/fdm/jsbsim/propulsion/engine[" ~ eng ~ "]/panel/button/relight", 1);
  settimer(func {
   setprop("/fdm/jsbsim/propulsion/engine[" ~ eng ~ "]/panel/button/relight", 0);
  }, 3.0);
  settimer(func {
   Boostpump(eng, pump);
   Generator(eng, 1);
   AirconEngines(1);
   APDSelect(-1);
  }, 12.0);
 }
}

# @brief Stop engine.
# @param eng Engine 0..3.
EngineStop = func (eng) {
 Boostpump(eng, 0);
 Generator(eng, 0);
 AirconEngine(eng, 0);
 Throttle(eng, 0.0);
 Cutoff(eng, 0);
 FireValve(eng, 0);
}


# @brief Turn on/off power to the APU.
APUPower = func(on) {
 setprop("/fdm/jsbsim/propulsion/engine[4]/panel/switch/master", on);
 setprop("/fdm/jsbsim/propulsion/engine[4]/panel/switch/ignition", on);
}

# Start/Stop APU. Starting takes 60 seconds.
# @brief Start/Stop APU.
APUStartStop = func(start) {
 var btn = "stop";
 if(start){
  btn = "start";
 }
 setprop("/fdm/jsbsim/propulsion/engine[4]/panel/button/" ~ btn, 1);
 settimer(func {
  setprop("/fdm/jsbsim/propulsion/engine[4]/panel/button/start", 0);
  setprop("/fdm/jsbsim/propulsion/engine[4]/panel/button/stop", 0);
 }, 3.0);
}

# Turn on/off APU air bleed. Needs 8 seconds.
# @brief APU air bleed.
APUBleed = func(on) {
 setprop("/fdm/jsbsim/propulsion/engine[4]/panel/switch/bleed", 2 * on - 1);
 settimer(func {
  setprop("/fdm/jsbsim/propulsion/engine[4]/panel/switch/bleed", 0);
 }, 8.0);
}


# @brief Turn on/off all batteries.
Batteries = func(on) {
 for(var ii = 0; ii < 4; ii += 1){
  setprop("/fdm/jsbsim/systems/elec/panel/switch/bat[" ~ ii ~"]", on);
 }
}

# @brief Turn on/off a generator.
# @param eng Engine number 0..3.
Generator = func(eng, on) {
 setprop("/fdm/jsbsim/systems/elec/panel/switch/gen[" ~ eng ~ "]", on);
}

# @brief Turn on/off all generators.
Generators = func(on) {
 for(var ii = 0; ii < 4; ii += 1){
  Generator(ii, on);
 }
}

# @brief APU generator.
GenAPU = func(on) {
 setprop("/fdm/jsbsim/systems/elec/panel/switch/APU", on);
}

# @brief Ground power unit.
GPU = func(on) {
 setprop("/fdm/jsbsim/systems/elec/panel/switch/GPU", on);
}

# @brief Electrical profile: off.
ElecOff = func() {
 Batteries(0);
 GenAPU(0);
 GPU(0);
 Generators(0);
}

# @brief Electrical profile: batteries.
ElecBat = func() {
 Batteries(1);
 GenAPU(0);
 GPU(0);
 Generators(0);
}

# @brief Electrical profile: APU.
ElecAPU = func() {
 Batteries(1);
 GenAPU(1);
 GPU(0);
 Generators(0);
}

# @brief Electrical profile: GPU.
ElecGPU = func() {
 Batteries(1);
 GPU(1);
 GenAPU(0);
 Generators(0);
}

# @brief Electrical profile: battery and all generators.
ElecDefault = func() {
 Batteries(1);
 Generators(1);
 GPU(0);
 GenAPU(0);
}

# @brief Fire panel: open/close engine and IDG valve.
# @param nacelle Nacelle number 0..3.
FireValve = func(nacelle, open) {
 setprop("/fdm/jsbsim/propulsion/fire/panel/switch/engine-valve[" ~ nacelle ~ "]", open);
 setprop("/fdm/jsbsim/propulsion/fire/panel/switch/KSA-valve[" ~ nacelle ~ "]", open);
}

# @brief Fire panel: open/close all valves.
FireValveAll = func(open) {
 for(var ii = 0; ii < 4; ii += 1){
  FireValve(ii, open);
 }
}


# @brief Turn on/off auto CG.
Autocg = func(on) {
 setprop("/fdm/jsbsim/propulsion/fuel/panel/switch/auto-CG", on);
}

# @brief Boost pump.
# @param eng Engine number 0..3.
# @param pump -1: backup, 0: off, 1: main.
Boostpump = func(eng, pump) {
 setprop("/fdm/jsbsim/propulsion/fuel/panel/switch/boost-pump[" ~ eng ~ "]", pump);
}

# @brief Set boost pump for all engines.
BoostpumpAll = func(pump) {
 for(var ii = 0; ii < 4; ii += 1){
  Boostpump(ii, pump);
 }
}

# @brief Turn on/off all crossfeed valves.
Crossfeeds = func(on) {
 for(var ii = 0; ii < 4; ii += 1){
  setprop("/fdm/jsbsim/propulsion/fuel/panel/switch/crossfeed[" ~ ii ~ "]", on);
 }
}

# @brief Set digital fuel meter fill. Takes 1 second.
# @param kg Fuel in kg, max. 200000. Will be rounded to 100.
# @param reset  Reset used fuel count.
FuelMeterFill = func(kg, reset = 0) {
 setprop("/fdm/jsbsim/systems/instr/RT/init/fill-t", math.min(math.max(math.round(kg * 0.001, 0.1), 0.0), 200.0));
 var on = 0;
 if(reset){
  on = getprop("/fdm/jsbsim/systems/instr/RT/panel/switch/run");
  FuelMeter(0);
 }
 setprop("/fdm/jsbsim/systems/instr/RT/init/fill-set", 1);
 settimer(func {
  setprop("/fdm/jsbsim/systems/instr/RT/init/fill-set", 0);
  if(reset){
   FuelMeter(on);
  }
 }, 1.0);
}

# @brief Set digital fuel meter fill equal to total fuel level. Takes 1 second.
# @param reset  Reset used fuel count.
FuelMeterAutofill = func(reset = 0) {
  FuelMeterFill(getprop("/fdm/jsbsim/propulsion/fuel/contents-kg"), reset);
}

# @brief Toggle digital fuel meter.
FuelMeter = func(on) {
 setprop("/fdm/jsbsim/systems/instr/RT/panel/switch/run", on);
}

var _FueltrimState = [0, 0];
# @brief Start fuel trim. Stop: call the second time with the same args.
# @param direction  1: tail heavy, 0: stop pumping, -1: nose heavy.
# @param flight phase  0: climb and cruise, 1: descent
Fueltrim = func(direction, phase = 0) {
 setprop("fdm/jsbsim/propulsion/fuel/panel/switch/auto-cg", 0);
 if((direction == _FueltrimState[0]) and (phase == _FueltrimState[1])){
  chat("Fuel trim stopped.");
  Fueltrim(0, 0);
  return;
 }
 _FueltrimState = [direction, phase];
 setprop("/fdm/jsbsim/propulsion/fuel/panel/switch/trim-1-4_8", 0);
 setprop("/fdm/jsbsim/propulsion/fuel/panel/switch/trim-2-4_8", 0);
 setprop("/fdm/jsbsim/propulsion/fuel/panel/switch/trim-8-4", 0);
 setprop("/fdm/jsbsim/propulsion/fuel/panel/switch/trim-8-6", 0);
 setprop("/fdm/jsbsim/propulsion/fuel/panel/switch/trim-1_2-6", 0);
 setprop("/fdm/jsbsim/propulsion/fuel/panel/switch/trim-8-1_2", 0);
 setprop("/fdm/jsbsim/propulsion/fuel/panel/switch/trim-elec-6-8", 0);
 setprop("/fdm/jsbsim/propulsion/fuel/panel/switch/trim-elec-8-6_1", 0);
 setprop("/fdm/jsbsim/propulsion/fuel/panel/switch/climb-descent", 0);
 if(!direction){
  return;
 }
 var tank1fuel = (getprop("/fdm/jsbsim/propulsion/tank[4]/pct-full") != 0.0);
 var tank1space = (getprop("/fdm/jsbsim/propulsion/tank[4]/pct-full") < 98.0);
 var tank4space = ((getprop("/fdm/jsbsim/propulsion/tank[7]/pct-full") < 98.0) or (getprop("/fdm/jsbsim/propulsion/tank[8]/pct-full") < 98.0));
 var tank6space = (getprop("/fdm/jsbsim/propulsion/tank[11]/pct-full") < 98.0);
 var tank8space = (getprop("/fdm/jsbsim/propulsion/tank[13]/pct-full") < 98.0);
 if(!phase){
  if(direction > 0){
   if(tank8space){
    if(tank1fuel){
     # 1 to 8
     chat("Fuel trim: 1 -> 8.");
     setprop("/fdm/jsbsim/propulsion/fuel/panel/switch/trim-1-4_8", -1);
    }else{
     # 2 to 8
     chat("Fuel trim: 2 -> 8.");
     setprop("/fdm/jsbsim/propulsion/fuel/panel/switch/trim-2-4_8", -1);
    }
   }else{
    if(tank1fuel){
     # 1 to 4
     chat("Fuel trim: 1 -> 4.");
     setprop("/fdm/jsbsim/propulsion/fuel/panel/switch/trim-1-4_8", 1);
    }else{
     # 2 to 4
     chat("Fuel trim: 2 -> 4.");
     setprop("/fdm/jsbsim/propulsion/fuel/panel/switch/trim-2-4_8", 1);
    }
   }
  }else if(direction < 0){
   if(tank1space){
    # 8 to 1
    chat("Fuel trim: 8 -> 1.");
    setprop("/fdm/jsbsim/propulsion/fuel/panel/switch/trim-8-1_2", 1);
   }else{
    # 8 to 2
    chat("Fuel trim: 8 -> 2.");
    setprop("/fdm/jsbsim/propulsion/fuel/panel/switch/trim-8-1_2", -1);
   }
  }
  setprop("/fdm/jsbsim/propulsion/fuel/panel/switch/climb-descent", 1);
 }else{
  if(direction > 0){
   if(tank1fuel){
    # 1 to 6
    chat("Fuel trim: 1 -> 6.");
    setprop("/fdm/jsbsim/propulsion/fuel/panel/switch/trim-1_2-6", 1);
   }else{
    # 2 to 6
    chat("Fuel trim: 2 -> 6.");
    setprop("/fdm/jsbsim/propulsion/fuel/panel/switch/trim-1_2-6", -1);
   }
  }else if(direction < 0){
   if(tank6space){
    # 8 to 6
    chat("Fuel trim: 8 -> 6.");
    setprop("/fdm/jsbsim/propulsion/fuel/panel/switch/trim-8-6", 1);
   }else{
    # 8 to 4
    chat("Fuel trim: 8 -> 4.");
    setprop("/fdm/jsbsim/propulsion/fuel/panel/switch/trim-8-4", 1);
   }
  }
  setprop("/fdm/jsbsim/propulsion/fuel/panel/switch/climb-descent", -1);
 }
}

FueltrimAutoNext = func() {
 var auto = getprop("fdm/jsbsim/propulsion/fuel/panel/switch/auto-cg");
 var phase = getprop("fdm/jsbsim/propulsion/fuel/panel/switch/climb-descent");
 if(!auto){
  auto = 1;
  phase = 1;
  chat("Auto CG: Climb.");
 }else{
  if(phase == 1){
   phase = 0;
   chat("Auto CG: Cruise.");
  }else if(phase == 0){
   phase = -1;
   chat("Auto CG: Descent.");
  }else{
   auto = 0;
   phase = 0;
   chat("Auto CG off.");
  }
 }
 setprop("fdm/jsbsim/propulsion/fuel/panel/switch/auto-cg", auto);
 setprop("fdm/jsbsim/propulsion/fuel/panel/switch/climb-descent", phase);
 _FueltrimState = [0, 0];
 setprop("/fdm/jsbsim/propulsion/fuel/panel/switch/trim-1-4_8", 0);
 setprop("/fdm/jsbsim/propulsion/fuel/panel/switch/trim-2-4_8", 0);
 setprop("/fdm/jsbsim/propulsion/fuel/panel/switch/trim-8-4", 0);
 setprop("/fdm/jsbsim/propulsion/fuel/panel/switch/trim-8-6", 0);
 setprop("/fdm/jsbsim/propulsion/fuel/panel/switch/trim-1_2-6", 0);
 setprop("/fdm/jsbsim/propulsion/fuel/panel/switch/trim-8-1_2", 0);
 setprop("/fdm/jsbsim/propulsion/fuel/panel/switch/trim-elec-6-8", 0);
 setprop("/fdm/jsbsim/propulsion/fuel/panel/switch/trim-elec-8-6_1", 0);
}


# @brief Air conditioning from engine.
# @param eng Engine number 0..3.
AirconEngine = func(eng, open) {
 setprop("/fdm/jsbsim/propulsion/air/panel/switch/engine[" ~ eng ~ "]", open * getprop("fdm/jsbsim/propulsion/engine[" ~ eng ~ "]/set-running"));
}

# @brief Air conditioning from all engines.
AirconEngines = func(open) {
 for(var ii = 0; ii < 4; ii += 1){
  AirconEngine(ii, open);
 }
}

# @brief Air conditioning from APU or ASU.
AirconAux = func(open) {
 for(var ii = 1; ii < 3; ii += 1){
  setprop("/fdm/jsbsim/propulsion/air/panel/switch/APU_ASU[" ~ ii ~ "]", open * (getprop("fdm/jsbsim/propulsion/engine[4]/set-running") or getprop("fdm/jsbsim/propulsion/air/source/APU_ASU/ASU-attached")));
 }
}

# @brief Air conditioning profile: off.
AirconOff = func() {
 AirconEngines(0);
 AirconAux(0);
}

# @brief Air conditioning profile: from APU or ASU.
AirconGate = func() {
 AirconEngines(0);
 AirconAux(1);
}

# @brief Air conditioning profile: from engines.
AirconDefault = func() {
 AirconEngines(1);
 AirconAux(0);
}


# @brief NPK-144 mode switch.
# @param mode Mode. 0: Off, 1: Powered, 2: Prepare, 3: Run.
NPKMode = func(mode) {
 setprop("/fdm/jsbsim/fcs/NPK/panel/PIK/switch/mode", mode);
}

# @brief NPK-144 departure airport switch.
# @param AP airport number (1..8).
NPKDepartureAirport = func(AP) {
 AP = math.min(math.max(AP, 1), 8);
 setprop("/fdm/jsbsim/fcs/NPK/panel/TsUNP/switch/AP-departure", AP - 1);
}

# @brief NPK-144 landing airport switch.
# @param AP airport number (1..9).
NPKLandingAirport = func(AP) {
 AP = math.min(math.max(AP, 1), 9);
 setprop("/fdm/jsbsim/fcs/NPK/panel/TsUNP/switch/AP", AP - 1);
}

# @brief Stabilize speed.
# @param M Stabilize Mach number instead of airspeed.
ABSUSpeed = func(M = 0) {
 if(!M){
  if(!getprop("/fdm/jsbsim/fcs/NPK/panel/PR/light/V")){
   setprop("/fdm/jsbsim/fcs/NPK/panel/PR/button/V", 1);
  }
 }else{
  if(!getprop("/fdm/jsbsim/fcs/NPK/panel/PR/light/M")){
   setprop("/fdm/jsbsim/fcs/NPK/panel/PR/button/M", 1);
  }
 }
 settimer(func {
  setprop("/fdm/jsbsim/fcs/NPK/panel/PR/button/H", 0);
  setprop("/fdm/jsbsim/fcs/NPK/panel/PR/button/roll", 0);
  setprop("/fdm/jsbsim/fcs/NPK/panel/PR/button/V", 0);
  setprop("/fdm/jsbsim/fcs/NPK/panel/PR/button/M", 0);
 }, 3.0);
}

# @brief Stabilize altitude and speed.
# @param M Stabilize Mach number instead of airspeed.
ABSULevel = func(M = 0) {
 setprop("/fdm/jsbsim/fcs/NPK/panel/PR/button/H", 1);
 setprop("/fdm/jsbsim/fcs/NPK/panel/PR/button/roll", 1);
 settimer(func {
  setprop("/fdm/jsbsim/fcs/NPK/panel/PR/button/H", 0);
  setprop("/fdm/jsbsim/fcs/NPK/panel/PR/button/roll", 0);
 }, 3.0);
 ABSUSpeed(M);
}

# @brief NPK-144 coordinate integration switch.
# @param on
NPKIntegration = func(on) {
 setprop("/fdm/jsbsim/fcs/NPK/panel/TsUNP/switch/integration", on);
}

# @brief Set QNH on left/right altimeters.
# Set QNH on left/right altimeters within 1 mmHg.
# @param memb Crew member: 0: pilot, 1: copilot.
# @param mmHg Pressure in mmHg.
SetQNH = func (memb, mmHg) {
 mmHg = math.round(mmHg, 1.0);
 setprop("/fdm/jsbsim/fcs/NPK/instr/UVO[" ~ memb ~ "]/pot/P0-mmHg", mmHg);
 setprop("/fdm/jsbsim/systems/instr/VM[" ~ memb ~ "]/pot/mmHg", mmHg);
}

# @brief Set QNH on all altimeters.
# @param mmHg Pressure in mmHg.
SetQNHAll = func (mmHg) {
 for(var ii = 0; ii < 2; ii += 1){
  SetQNH(ii, mmHg);
 }
}

# @brief Set magnetic variation.
# Set magnetic variation within 0.1 deg.
SetMagvar = func (magvar) {
 setprop("/fdm/jsbsim/fcs/NPK/panel/PK/pot/variation-deg", math.round(math.min(math.max(magvar, -90.0), 90.0), 0.1));
}

# @brief Reset G meter.
# Reset G meters. Takes 1 second.
# @param memb Crew member: 0: pilot, 1: copilot.
ResetGmeter = func (memb) {
 setprop("/fdm/jsbsim/systems/AUASP/instr/UAP[" ~ memb ~ "]/pot/reset", 1);
 settimer(func {
  setprop("/fdm/jsbsim/systems/AUASP/instr/UAP[" ~ memb ~ "]/pot/reset", 0);
 }, 1.0);
}

# @brief Acknowledge master warning and caution.
# Acknowledge master warning and caution. Takes 1 second.
AckMaster = func () {
 setprop("/fdm/jsbsim/systems/warning/panel/button/ack-caution", 1);
 setprop("/fdm/jsbsim/systems/warning/panel/button/ack-warning", 1);
 settimer(func {
  setprop("/fdm/jsbsim/systems/warning/panel/button/ack-caution", 0);
  setprop("/fdm/jsbsim/systems/warning/panel/button/ack-warning", 0);
 }, 1.0);
}

# @brief Start/stop/reset stopwatch.
# Acknowledge master warning and caution. Takes 2 seconds.
# @param memb Crew member: 0: pilot, 1: copilot, 2: engineer, 3: navigator.
# @param cmd Command: 0: reset, 1: start, 2: stop.
Stopwatch = func (memb, cmd) {
 setprop("/fdm/jsbsim/systems/instr/AChS[" ~ memb ~ "]/button/stopwatch", 0);
 var err = 0;
 settimer(func{
  err = math.mod((cmd - getprop("/fdm/jsbsim/systems/instr/AChS[" ~ memb ~ "]/status/stopwatch") + 3), 3);
  if(err){
   setprop("/fdm/jsbsim/systems/instr/AChS[" ~ memb ~ "]/button/stopwatch", 1);
   settimer(func{
    setprop("/fdm/jsbsim/systems/instr/AChS[" ~ memb ~ "]/button/stopwatch", 0);
    if(err > 1){
     settimer(func{
      setprop("/fdm/jsbsim/systems/instr/AChS[" ~ memb ~ "]/button/stopwatch", 1);
      settimer(func{
       setprop("/fdm/jsbsim/systems/instr/AChS[" ~ memb ~ "]/button/stopwatch", 0);
      }, 0.4);
     }, 0.4);
    }
   }, 0.4);
  }
 }, 0.4);
}

# @brief Prepare NPK settings.
PrepareNPK = func () {
 SetMagvar(magvar());
 NPKLandingAirport(getprop("/fdm/jsbsim/fcs/NPK/Orbita/flightplan/AP/count") or 1);
}

# @brief Prepare all instruments.
PrepareInstruments = func () {
 QNH = math.floor(getprop("/environment/metar/pressure-sea-level-inhg") * 25.4);
 setprop("/sim/crew/orders/altimeter-mmHg", QNH);
 SetQNHAll(QNH);
 for(var ii = 0; ii < 2; ii += 1){
  ResetGmeter(ii, QNH);
 }
 PrepareNPK();
 setprop("/instrumentation/PKP[0]/pot/theta_0-deg", 0.0);
 setprop("/instrumentation/PKP[1]/pot/theta_0-deg", 0.0);
 setprop("/fdm/jsbsim/fcs/NPK/instr/UUT/pot/theta_max-deg", 11.0);
 setprop("/instrumentation/AGR/pot/theta_0-deg", 0.0);
 setprop("/fdm/jsbsim/fcs/NPK/instr/PZVE/switch/H", 16000);
 setprop("/fdm/jsbsim/fcs/NPK/instr/PZVE/switch/units", 0);
}

# @brief Prompt about VOR1 magvar.
MagvarPrompt = func () {
 var beacon = findNavaidByFrequencyMHz(getprop("fdm/jsbsim/fcs/NPK/KURS-MP/SDK/SDK[0]/f-MHz"));
 if(beacon != nil){
  var magvar = beacon.magvar;
  if(magvar != nil){
   return geo.normdeg180(beacon.magvar);
  }
 }
 return nil;
}

# @brief Print VOR1 magvar to the chat.
# @param set 0: Do nothing (default); 1: Also set on PK panel.
MagvarPromptCopilot = func (set = 0) {
 var magvar = MagvarPrompt();
 if(magvar != nil){
  var msg = sprintf("(copilot): VOR1 variation: %1.1f deg.", magvar);
  if(set){
   SetMagvar(magvar);
   msg = msg ~ " Magvar set.";
  }
  chat(msg);
  return;
 }
 chat("(copilot): I don't know VOR1 magvar!");
}


# @brief Collision light.
Strobelight = func (on) {
 setprop("/fdm/jsbsim/systems/lighting/panel/switch/collision", on);
}

# @brief Nav light.
Navlight = func (on) {
 setprop("/fdm/jsbsim/systems/lighting/panel/switch/nav", on);
}

# @brief Taxi/landing light.
# @param cmd -1: Taxi light; 0: Off; 1: Landing light.
Frontlight = func (cmd) {
 if(cmd != getprop("/fdm/jsbsim/systems/lighting/panel/switch/front")){
  setprop("/fdm/jsbsim/systems/lighting/panel/switch/front", 0);
  # TODO This delay must go once switch function is implemented.
  if(cmd != 0) {
   settimer(func {
    setprop("/fdm/jsbsim/systems/lighting/panel/switch/front", cmd);
   }, 0.2);
  }
 }
}

# @brief Light profile: off.
LightsOff = func () {
 Strobelight(0);
 Navlight(0);
 Frontlight(0);
}

# @brief Light profile: gate.
LightsGate = func (night = 0) {
 Strobelight(0);
 Navlight(night);
 Frontlight(0);
}

# @brief Light profile: startup.
LightsStartup = func (night = 0) {
 Strobelight(1);
 Navlight(night);
 Frontlight(0);
}

# @brief Light profile: taxi.
LightsTaxi = func (night = 0) {
 Strobelight(1);
 Navlight(night);
 Frontlight(-1);
}

# @brief Light profile: takeoff/landing.
LightsTOL = func (night = 0, fog = 0) {
 Strobelight(1);
 Navlight(night);
 Frontlight(!fog);
}

# @brief Light profile: cruise.
LightsDefault = func (night = 0) {
 Strobelight(1);
 Navlight(night);
 Frontlight(0);
}

# @brief Cabin light.
Cabinlight = func (main, backup = 0) {
 setprop("/fdm/jsbsim/systems/lighting/panel/switch/cabin", main);
 setprop("/fdm/jsbsim/systems/lighting/panel/switch/cabin_backup", backup);
}

# @brief Cargo hold light.
Cargolight = func (on) {
 setprop("/fdm/jsbsim/systems/lighting/panel/switch/cargo", on);
}

# @brief Internal light profile: default
IntlightsDefault = func () {
 Cabinlight(1, 0);
 Cargolight(0);
}

# @brief Internal light profile: takeoff/landing
IntlightsTOL = func () {
 Cabinlight(0, 1);
 Cargolight(0);
}

# @brief Internal light profile: gate
IntlightsGate = func () {
 Cabinlight(1, 0);
 Cargolight(1);
}

# @brief Internal light profile: default
IntlightsOff = func () {
 Cabinlight(0, 0);
 Cargolight(0);
}


# @brief Set window command
# @param window Window number. 0: Left; 1: Right.
# @param cmd  Command norm (0.0 .. 1.0).
Window = func(window, cmd) {
 setprop("fdm/jsbsim/systems/doors/window[" ~ window ~ "]/cmd-norm", cmd);
}

# @brief Set all windows' command at once
# @param cmd  Command norm (0.0 .. 1.0).
WindowsImmed = func(cmd) {
 for(var ii = 0; ii < 2; ii += 1){
  Window(ii, cmd);
 }
}

# @brief Set door command
# @param door Door number. 0: Front; 1: Front catering; 2: Back; 3: Back catering.
# @param cmd  Command norm (0.0 .. 1.0).
Door = func(door, cmd) {
 setprop("fdm/jsbsim/systems/doors/door[" ~ door ~ "]/cmd-norm", cmd);
}

# @brief Set all doors' command at once
# @param cmd  Command norm (0.0 .. 1.0).
DoorsImmed = func(cmd) {
 for(var ii = 0; ii < 4; ii += 1){
  Door(ii, cmd);
 }
}

# @brief Set all doors' command with realistic delay
# @param cmd  Command norm (0.0 .. 1.0).
Doors = func(cmd) {
 Door(0, cmd);
 settimer(func{
  Door(1, cmd);
 }, 10.0);
 settimer(func{
  Door(2, cmd);
 }, 1.0);
 settimer(func{
  Door(3, cmd);
 }, 15.0);
}

# @brief Set cargo door command
# @param door Cargo door number. 0: Front; 1: Back.
# @param cmd  Command norm (0.0 .. 1.0).
Cargodoor = func(door, cmd) {
 setprop("fdm/jsbsim/systems/doors/cargo-door[" ~ door ~ "]/cmd-norm", cmd);
}

# @brief Set all cargo doors' command at once
# @param cmd  Command norm (0.0 .. 1.0).
CargodoorsImmed = func(cmd) {
 for(var ii = 0; ii < 4; ii += 1){
  Cargodoor(ii, cmd);
 }
}

# @brief Set all cargo doors' command with realistic delay
# @param cmd  Command norm (0.0 .. 1.0).
Cargodoors = func(cmd) {
  Cargodoor(0, cmd);
  settimer(func{
   Cargodoor(1, cmd);
  }, 5.0);
}

# @brief Select SPU radio.
# @param seat  0: Pilot, 1: Copilot, 2: Engineer, 3: Navigator.
# XXX Seat number unused for now, as only pilot's SPU is implemented.
# @param radio 0: KW1, 1: KW2. 2: UKW1, 3: UKW2
SPU = func(seat, radio) {
 setprop("/fdm/jsbsim/systems/comm/SPU/SPU[0]/panel/switch/radio", radio);
}

# @brief SPU push to talk button.
# @param seat  0: Pilot, 1: Copilot, 2: Engineer, 3: Navigator.
# XXX Seat number unused for now, as only pilot's SPU is implemented.
# @param ptt  PTT pressed.
PTT = func(seat, ptt) {
 setprop("/fdm/jsbsim/systems/comm/SPU/SPU[0]/panel/button/ptt", ptt);
}

# @brief Set COM or SW frequency
# @param id  0: COM1, 1: COM2
# @param freq  frequency: 118.0..135.975 MHz (COM), 2..23.9999 MHz (SW)
COM = func(id, freq) {
 if (freq < 100) {
  SW(freq);
 } else {
  freq = math.min(math.max(freq, 118.0), 135.975);
  freq -= 118.0;
  var channel = [0, 0];
  channel[0] = math.round(math.mod(freq * 40.0, 40), 1.0);
  channel[1] = int(freq);
  for(var ich = 0; ich < 2; ich += 1){
   setprop("/fdm/jsbsim/systems/comm/Landysh/UKW[" ~ id ~ "]/panel/switch/channel[" ~ ich ~ "]", channel[ich]);
  }
 }
}

# @brief Set SW frequency
# @param freq  frequency: 2..23.9999 MHz
SW = func(freq) {
 var channel = math.round(math.min(math.max(freq, 2.0), 23.9999), 0.0001) * 10000.0;
 for(var ich = 0; ich < 6; ich += 1){
  setprop("/fdm/jsbsim/systems/comm/Micron/panel/P7V/switch/f[" ~ ich ~ "]", math.mod(int(channel * math.pow(0.1, ich)), 10));
 }
}

# @brief Set NAV or RSBN frequency
# @param id  0: NAV1 or RSBN, 1: NAV2 or Katet
# @param freq  frequency or channel: 108.0..118.0 MHz (NAV), 1..88 (RSBN), 1..40 (Katet)
# @param radial  radial: 0..360
# @param reciprocal  invert radial: 0: no, 1: yes
NAV = func(id, freq, radial = -1, reciprocal = 0) {
 if (freq < 100) {
  if (!id) {
   RSBN(freq);
  } else {
   Katet(freq);
  }
 } else {
  freq = math.min(math.max(freq, 108.0), 118.0);
  freq -= 108.0;
  var channel = [0, 0];
  channel[0] = math.round(math.mod(freq * 20.0, 20), 1.0);
  channel[1] = int(freq);
  for(var ich = 0; ich < 2; ich += 1){
   setprop("/fdm/jsbsim/fcs/NPK/instr/SDK[" ~ id ~ "]/switch/channel[" ~ ich ~ "]", channel[ich]);
  }
 }
 if (radial != -1) {
  SK(id, radial, reciprocal);
 }
}

# @brief Set radial on SK
# @param id  0: NAV1 or RSBN, 1: NAV2 or Katet
# @param radial  radial: 0..360
# @param reciprocal  invert radial: 0: no, 1: yes
SK = func(id, radial, reciprocal = 0) {
 radial = math.mod(math.round(radial, 1.0), 360.0);
 setprop("/fdm/jsbsim/fcs/NPK/instr/SK[" ~ id ~ "]/switch/radial-deg", radial);
 setprop("/fdm/jsbsim/fcs/NPK/instr/SK[" ~ id ~ "]/switch/reciprocal", reciprocal);
}

# @brief Set ADF frequency
# @param beacon  0: outer beacon, 1: inner beacon
# @param freq  frequency, 150.0..1799.5 kHz
ADF = func(beacon, freq) {
 freq = math.min(math.max(freq, 150.0), 1799.5);
 freq -= 100.0;
 var channel = [0, 0, 0];
 channel[0] = math.round(math.mod(freq * 2.0, 20), 1.0);
 channel[1] = math.mod(int(freq * 0.1), 10) - 5;
 # 0 is after 9 on the dial, not before 1:
 if(channel[1] == -5){
  channel[1] = 5;
 }
 channel[2] = int(freq * 0.01);
 for(var ich = 0; ich < 3; ich += 1){
  setprop("/fdm/jsbsim/fcs/NPK/instr/ARK/switch/channel" ~ beacon ~ "[" ~ ich ~ "]", channel[ich]);
 }
}

# @brief Set RSBN channel
# @param ch  Channel, 1..88
RSBN = func(ch) {
 ch = math.min(math.max(int(ch), 1), 88);
 setprop("/fdm/jsbsim/fcs/NPK/instr/RSBN/switch/RSBN-ch", ch);
}

# @brief Set Katet channel
# @param ch  Channel, 1..40
Katet = func(ch) {
 ch = math.min(math.max(int(ch), 1), 40);
 setprop("/fdm/jsbsim/fcs/NPK/instr/RSBN/switch/Katet-ch", ch);
}

# @brief Set autopilot course
# @param heading  radial: 0.0..360.0
Crs = func(heading) {
 heading = math.mod(math.round(heading, 0.1), 360.0);
 setprop("/fdm/jsbsim/fcs/NPK/panel/PSU/pot/course-deg", heading);
}

# @brief Set runway heading
# @param heading  runway heading: 0..360
# @param reciprocal  invert heading: 0: no, 1: yes
Rwy = func(heading, reciprocal = 0) {
 heading = math.mod(math.round(heading, 1.0), 360.0);
 setprop("/fdm/jsbsim/fcs/NPK/panel/switch/runway-reciprocal", reciprocal);
 setprop("/fdm/jsbsim/fcs/NPK/panel/switch/runway-hdg-deg", heading);
}


# @brief Chocks
Chocks = func (on) {
 setprop("/systems/ground-supply/dialog/wheel-chocks", on);
}


# @brief Prepare for fueling.
PreFueling = func() {
 gui.popupTip("Pre-fueling checks: 2 m 30 s to go.");
 Parkingbrake(1);
 Chocks(1);
 StarterKey(4);
 settimer(func {
  StarterKey(5);
  LightsGate(1);
  IntlightsTOL();
 }, 1.0);
 settimer(func {
  if(!getprop("fdm/jsbsim/propulsion/engine[4]/set-running")){
   return;
  }
  gui.popupTip("Pre-fueling checks: 1 m 20 s to go.");
  IntlightsGate();
 }, 70.0);
 settimer(func {
  SelectAllEngines();
  gui.popupTip("Pre-fueling checks finished.");
 }, 150.0);
}

# @brief Prepare for taxi.
PreTaxi = func() {
 if(
  (
   getprop("fdm/jsbsim/propulsion/air/as[0]/capacity-norm") +
   getprop("fdm/jsbsim/propulsion/air/as[1]/capacity-norm") +
   getprop("fdm/jsbsim/propulsion/air/as[2]/capacity-norm") < 0.75
  ) or
  !getprop("fdm/jsbsim/systems/elec/DC27-reserve/U-ok") or
  !getprop("fdm/jsbsim/systems/elec/AC36-reserve[0]/U-ok") or
  (getprop("fdm/jsbsim/fcs/NPK/panel/PIK/switch/mode") < 2)
 ){
  gui.popupTip('Not ready to autostart. Run Pre-fueling/Loading or complete procedures:\n"Preparing the Aircraft" and\n"Instruments, Fuel and Payload".');
  return;
 }
 gui.popupTip("Startup: 5 m 40 s to go.");
 Startbrake(1);
 Chocks(0);
 Parkingbrake(0);
 WindowsImmed(0.0);
 Doors(0.0);
 Cargodoors(0.0);
 LightsStartup(1);
 IntlightsDefault();
 TrimTargetTakeoff();
 TrimDo();
 StarterKey(0);
 settimer(func {
  if(!getprop("fdm/jsbsim/propulsion/engine[0]/set-running")){
   return;
  }
  gui.popupTip("Startup: 4 m 15 s to go.");
  StarterKey(1);
 }, 85.0);
 settimer(func {
  if(!getprop("fdm/jsbsim/propulsion/engine[1]/set-running")){
   return;
  }
  gui.popupTip("Startup: 2 m 50 s to go.");
  StarterKey(2);
 }, 170.0);
 settimer(func {
  if(!getprop("fdm/jsbsim/propulsion/engine[2]/set-running")){
   return;
  }
  gui.popupTip("Startup: 1 m 25 s to go.");
  StarterKey(3);
 }, 255.0);
 settimer(func {
  if(
   !getprop("fdm/jsbsim/propulsion/engine[0]/set-running") or
   !getprop("fdm/jsbsim/propulsion/engine[1]/set-running") or
   !getprop("fdm/jsbsim/propulsion/engine[2]/set-running") or
   !getprop("fdm/jsbsim/propulsion/engine[3]/set-running")
  ){
   return;
  }
  SelectAllEngines();
  gui.popupTip("Startup finished.");
 }, 340.0);
}


# FG control bindings

# @brief Trim pitch with the mouse wheel.
# Apply trim command proportionally to the number of consecutive mouse wheel ticks in the same direction, abort on the opposite.
# Stops trimming if the trim switch property is overwritten by anything else.
# @param cmd Trim command: -1: nose up, 0: stop, +1: nose down.
# @param ticksLimit maximum number of ticks allowed to build up.
# @param tickTimeout Trim time per tick, seconds. Non-expired ticks will use the new timeout.
MouseTrim = func (cmd, ticksLimit, tickTime) {
 if(((_MouseTrimCmd != 0) and (cmd != _MouseTrimCmd)) or (cmd == 0)){
  _MouseTrimStop(0);
  return;
 }
 _MouseTrimCmd = cmd;
 _MouseTrimTicks = math.min(_MouseTrimTicks + 1, ticksLimit);
 _MouseTrimTimer.restart(tickTime);
 if(_MouseTrimListener != nil) {
  removelistener(_MouseTrimListener);
  _MouseTrimListener = nil;
 }
 setprop("/fdm/jsbsim/fcs/panel/pot/pitch-trim/cmd", cmd);
 _MouseTrimListener = setlistener("/fdm/jsbsim/fcs/panel/pot/pitch-trim/cmd", func{_MouseTrimStop(1)});
}
var _MouseTrimTimer = maketimer(0.1, func{
 if(_MouseTrimTicks > 0) {
  _MouseTrimTicks -= 1;
  return;
 }
 _MouseTrimStop(0);
}, 1);
_MouseTrimTimer.simulatedTime = 1;
_MouseTrimStop = func (fromListener) {
 if(_MouseTrimListener != nil) {
  removelistener(_MouseTrimListener);
  _MouseTrimListener = nil;
 }
 _MouseTrimTimer.stop();
 _MouseTrimCmd = 0;
 _MouseTrimTicks = 0;
 if(!fromListener){
  setprop("/fdm/jsbsim/fcs/panel/pot/pitch-trim/cmd", 0);
 }
}
var _MouseTrimCmd = 0;
var _MouseTrimTicks = 0;
var _MouseTrimListener = nil;

controls.elevatorTrim = func(speed) {
 Tu144D.MouseTrim(speed > 0.09 ? 1 : speed < -0.09 ? -1 : 0, 1, 0.1);
}

controls.aileronTrim = func(speed) {
 setprop("fdm/jsbsim/fcs/panel/switch/roll-trim", speed > 0.09 ? 1 : speed < -0.09 ? -1 : 0);
 _aileronTrimTimer.restart(0.1);
}
var _aileronTrimTimer = maketimer(0.1, func{
 setprop("fdm/jsbsim/fcs/panel/switch/roll-trim", 0);
});
_aileronTrimTimer.singleShot = 1;
_aileronTrimTimer.simulatedTime = 1;

controls.rudderTrim = func(speed) {
 setprop("fdm/jsbsim/fcs/panel/switch/yaw-trim", speed > 0.09 ? 1 : speed < -0.09 ? -1 : 0);
 _rudderTrimTimer.restart(0.1);
}
var _rudderTrimTimer = maketimer(0.1, func{
 setprop("fdm/jsbsim/fcs/panel/switch/yaw-trim", 0);
});
_rudderTrimTimer.singleShot = 1;
_rudderTrimTimer.simulatedTime = 1;

# arg[0] is the throttle increment
# arg[1] is unused
controls.incThrottle = func() {
 var down = (arg[0] < -0.001 ? 1 : 0);
 var up = (arg[0] > 0.001 ? 1 : 0);
 for(var ii = 0; ii < 4; ii += 1){
  if(getprop("/sim/input/selected/engine[" ~ ii ~ "]")){
   var val = getprop("/controls/engines/engine[" ~ ii ~ "]/throttle") + arg[0];
   setprop("/controls/engines/engine[" ~ ii ~ "]/throttle", val < 0.0 ? 0.0 : val > 1.0 ? 1.0 : val);
   setprop("/fdm/jsbsim/fcs/throttle-moving-up[" ~ ii ~ "]", up);
   setprop("/fdm/jsbsim/fcs/throttle-moving-down[" ~ ii ~ "]", down);
  }
 }
 settimer(func{
  for(var ii = 0; ii < 4; ii += 1){
   setprop("/fdm/jsbsim/fcs/throttle-moving-up[" ~ ii ~ "]", 0);
   setprop("/fdm/jsbsim/fcs/throttle-moving-down[" ~ ii ~ "]", 0);
  }
 }, 0.5);
}

controls.throttleMouse = func {
 if(!getprop("/devices/status/mice/mouse[0]/button[1]")) return;
 var delta = cmdarg().getNode("offset").getValue() * -4;
 controls.incThrottle(delta, 0.0);
}

controls.ptt = func(ptt){
 if(getprop("/sim/ptt-autoselect")){
  if(ptt){
   # SIC Moving UKW radios to be first, to match FG numbering.
   SPU(0, math.mod((ptt + 1), 4));
  }
 }
 PTT(0, ptt > 1 ? 1 : ptt);
}

controls.gearDown = func(increment){
 GearCmdInc(increment);
}

# XXX Does not support neutral position.
setlistener("/controls/gear/gear-down", func(p) {
});

setlistener("/controls/flight/flaps", func(p) {
 CanardCmd(math.round(3 * p.getValue(), 1));
});

setlistener("/controls/flight/wing-sweep", func(p) {
 ConeCmd(math.round(3 * p.getValue(), 1));
});

# XXX Does not press and release keys directly, we use timers.
setlistener("/controls/flight/drag-chute", func(p) {
 Chute(p.getValue());
});


# Disable unsupported or irrelevant menu items.
gui.menuEnable ("failure-submenu", 0);
gui.menuEnable ("random-failures", 0);
gui.menuEnable ("system-failures", 0);
gui.menuEnable ("instrument-failures", 0);
gui.menuEnable ("random-attitude", 0);
gui.menuEnable ("autopilot-settings", 0);
gui.menuEnable ("previous-waypoint", 0);
gui.menuEnable ("next-waypoint", 0);
gui.menuEnable ("radio", 0);
gui.menuEnable ("gps", 0);
gui.menuEnable ("instrument-settings", 0);
gui.menuEnable ("carrier", 0);
# FGBUG MapStructure crashes FG in the end of the flight if you open Canvas map: https://sourceforge.net/p/flightgear/codetickets/2356/
gui.menuEnable ("map-canvas", 0);
# Disable the annoying default greeting that will get in the way when converting crew chat to /sim/messages.
gui.do_welcome = 0;

setlistener("/sim/signals/fdm-initialized", func(p) {
 if(p.getValue() == 0){
  return;
 }
 # FIXME This assumes that keys always update engine[0]
 setlistener("sim/input/selected/engine[0]", func(p) {
  settimer(func {
   _EngineSelectMessage();
  }, 0.01);
 });
});

aircraft.livery.init("Models/Liveries");

var mpclash_nas = resolvepath("Aircraft/MPClash/Nasal/MPClash.nas");
if(io.stat(mpclash_nas) != nil){
 print("Load MPClash: " ~ mpclash_nas);
 globals.io.load_nasal(mpclash_nas, "mpclash");
}

if(
 getprop("/controls/flight/auto-coordination")
){
 fgcommand("dialog-show", {
  'dialog-name' :'Tu-144D-config-error'
 });
}

# FGCOM interface.

# FGCOM logic is listener-based, so connecting JSBSim directly would not work.
var _fgcom_timer = maketimer(0.1, func{
 # Comm selection: 2: UKW1, 3: UKW2.
 var tu144d_select = getprop("/fdm/jsbsim/systems/comm/SPU/SPU[0]/panel/switch/radio") or 0;
 var select = math.max(0, tu144d_select - 1);
 if(select){
  select *= getprop("/fdm/jsbsim/systems/comm/Landysh/UKW[" ~  (select - 1) ~ "]/status/on") or 0;
 }
 if(select != getprop("/controls/radios/comm-radio-selected")){
  setprop("/controls/radios/comm-radio-selected", select);
  _fgcom_select = select;
 }
 for(var ii = 0; ii < 2; ii = ii + 1){
  var ptt = getprop("/instrumentation/comm[" ~ ii ~ "]/ptt_jsbsim") or 0;
  var ptt_diff = ptt - getprop("/instrumentation/comm[" ~ ii ~ "]/ptt");
  if(ptt_diff > 0.5){
   setprop("/instrumentation/comm[" ~ ii ~ "]/ptt", 1);
  } else if(ptt_diff < -0.0001) {
   setprop("/instrumentation/comm[" ~ ii ~ "]/ptt", 0);
  }
  var freq = getprop("/instrumentation/comm[" ~ ii ~ "]/frequencies/selected-mhz_jsbsim") or 0.0;
  var freq_diff = freq - getprop("/instrumentation/comm[" ~ ii ~ "]/frequencies/selected-mhz");
  if(math.abs(freq_diff) > 0.0000001){
   setprop("/instrumentation/comm[" ~ ii ~ "]/frequencies/selected-mhz", freq);
  }
  var vol = getprop("/instrumentation/comm[" ~ ii ~ "]/volume_jsbsim") or 0.0;
  var vol_diff = vol - (getprop("/instrumentation/comm[" ~ ii ~ "]/volume"));
  if(math.abs(vol_diff) > 0.001){
   setprop("/instrumentation/comm[" ~ ii ~ "]/volume", vol);
  }
 }
});

# FGBUG FGCOM becomes inaudible if frequency is changed without restart.
var _fgcom_restart = func(){
 if(getprop("/sim/fgcom/enabled")){
  setprop("/sim/fgcom/enabled", 0);
  settimer(func {
   setprop("/sim/fgcom/enabled", 1);
  }, 0.1);
 }
}
var _fgcom_selected = 0;
setlistener("/controls/radios/comm-radio-selected", func(p) {
 var selected = p.getValue();
 if(selected != _fgcom_selected){
  _fgcom_restart();
 }
 _fgcom_selected = selected;
});
setlistener("/instrumentation/comm[0]/frequencies/selected-mhz", func(p) {
 if(getprop("/controls/radios/comm-radio-selected") == 1){
  _fgcom_restart();
 }
});
setlistener("/instrumentation/comm[1]/frequencies/selected-mhz", func(p) {
 if(getprop("/controls/radios/comm-radio-selected") == 2){
  _fgcom_restart();
 }
});

_fgcom_timer.start();



var _EngineSelectMessage = func() {
 var nSelected = 0;
 var names = ["Engine 1", "Engine 2", "Engine 3", "Engine 4", "APU", "Avionics"];
 var list = "";
 for(var ii = 0; ii < 6; ii += 1){
  if(getprop("sim/input/selected/engine[" ~ ii ~ "]")) {
   if(nSelected) {
    list ~= ", ";
   }
   list = list ~ names[ii];
   nSelected += 1;
  }
 }
 if(nSelected == 6) {
  list = "all engines"
 }else if(nSelected == 0) {
  list = "no engines"
 }
 chat("Selected: " ~ list ~ ".");
}

var SelectEngine = func(engine) {
 for(var ii = 0; ii < 6; ii += 1){
  setprop("sim/input/selected/engine[" ~ ii ~ "]", (engine == ii));
 }
}

var SelectAllEngines = func() {
 for(var ii = 4; ii < 6; ii += 1){
  setprop("sim/input/selected/engine[" ~ ii ~ "]", 0);
 }
 for(var ii = 0; ii < 4; ii += 1){
  setprop("sim/input/selected/engine[" ~ ii ~ "]", 1);
 }
}

var _StarterKeyBusy = 0;
var StarterKey = func(engine = -1) {
 var select = [0, 0, 0, 0, 0, 0];
 var nSelected = 0;
 if(engine == -1){
  for(var ii = 0; ii < 6; ii += 1){
   select[ii] = getprop("sim/input/selected/engine[" ~ ii ~ "]");
   nSelected += select[ii];
  }
 }else{
  select[engine] = 1;
 }
 if(nSelected > 1){
  chat("(engineer): Not so fast! One thing at a time!", 3);
  return;
 }
 if(select[5]){
  StartAvionics();
  return;
 }
 if(_StarterKeyBusy){
  chat("(engineer): Busy!", 3);
  return;
 }
 for(var ii = 0; ii < 4; ii += 1){
  if(select[ii]){
   var wow = getprop("fdm/jsbsim/gear/wow");
   if(getprop("fdm/jsbsim/propulsion/engine[" ~ ii ~ "]/set-running")){
    Boostpump(ii, 1);
    Generator(ii, 1);
    AirconEngines(1);
    chat("(engineer): Engine " ~ (ii + 1) ~ " is already running, systems checked.", 2);
    return;
   }
   var relight = (getprop("fdm/jsbsim/fcs/NPK/instr/US-I[1]/gauge/V-km_h") > 500.0);
   if(relight){
    if(!getprop("fdm/jsbsim/systems/elec/AC36-reserve[0]/U-ok")){
     chat("(engineer): Need 36V for ignition!", 3);
     return;
    }
    chat("(engineer): Relighting engine " ~ (ii + 1) ~ ", wait 15 seconds.", 1);
   }else{
    if(
     (
      getprop("fdm/jsbsim/propulsion/air/as[0]/capacity-norm") +
      getprop("fdm/jsbsim/propulsion/air/as[1]/capacity-norm") +
      getprop("fdm/jsbsim/propulsion/air/as[2]/capacity-norm") < 0.75
     ) or
     !getprop("fdm/jsbsim/systems/elec/AC36-reserve[0]/U-ok")
    ){
     chat("(engineer): Need air for starter and 36V for ignition!", 3);
     return;
    }
    if(wow and ((getprop("fdm/jsbsim/fcs/NPK/IS/status/func/ready-norm") < 0.9) or _StartAvionicsBusy)) {
     chat("(engineer): Wait for Copilot to start our avionics!", 3);
     return;
    }
    if(
     wow and
     !getprop("fdm/jsbsim/propulsion/engine[0]/set-running") and
     !getprop("fdm/jsbsim/propulsion/engine[1]/set-running") and
     !getprop("fdm/jsbsim/propulsion/engine[2]/set-running") and
     !getprop("fdm/jsbsim/propulsion/engine[3]/set-running")
    ){
     if(getprop("fdm/jsbsim/fcs/NPK/panel/PIK/switch/mode") < 3){
      StartAvionics();
     }
     FuelMeter(1);
     FuelMeterAutofill(1);
    }
    chat("(engineer): Starting engine " ~ (ii + 1) ~ ", wait 85 seconds.", 1);
   }
   _StarterKeyBusy = 1;
   EngineStart(ii, 1, relight);
   if(relight){
    settimer(func {
     if(getprop("fdm/jsbsim/propulsion/engine[" ~ ii ~ "]/set-running")) {
      chat("(engineer): Engine " ~ (ii + 1) ~ " relighted.", 1);
     }else{
      chat("(engineer): Engine " ~ (ii + 1) ~ " did not relight.", 3);
     }
     _StarterKeyBusy = 0;
    }, 12.0);
   }else{
    settimer(func {
     if(getprop("fdm/jsbsim/propulsion/engine[" ~ ii ~ "]/set-running")) {
      chat("(engineer): Engine " ~ (ii + 1) ~ " started.", 1);
     }else{
      chat("(engineer): Engine " ~ (ii + 1) ~ " did not start.", 3);
     }
    }, 82.0);
    settimer(func {
     if(
      wow and
      getprop("fdm/jsbsim/propulsion/engine[0]/set-running") and
      getprop("fdm/jsbsim/propulsion/engine[1]/set-running") and
      getprop("fdm/jsbsim/propulsion/engine[2]/set-running") and
      getprop("fdm/jsbsim/propulsion/engine[3]/set-running")
     ){
      chat("(engineer): All engines started. Stopping APU, wait 60 seconds.", 1);
      GenAPU(0);
      AirconAux(0);
      APUBleed(0);
      settimer(func {
       APUStartStop(0);
      }, 60.0);
      settimer(func {
       APUPower(0);
       chat("(engineer): APU stopped. Ready to taxi!", 1);
      }, 70.0);
     }
     _StarterKeyBusy = 0;
    }, 83.0);
   }
   return;
  }
 }
 if(select[4]){
  if(getprop("fdm/jsbsim/propulsion/engine[4]/set-running")){
   _StarterKeyBusy = 1;
   chat("(engineer): APU is already running. Checking consumers.", 2);
   APUBleed(1);
   settimer(func {
    GenAPU(1);
    AirconAux(1);
    chat("(engineer): APU consumers checked.", 2);
    _StarterKeyBusy = 0;
   }, 10.0);
   return;
  }
  _StarterKeyBusy = 1;
  Batteries(1);
  GenAPU(0);
  APUBleed(0);
  APUPower(1);
  APUStartStop(1);
  chat("(engineer): Starting APU, wait 70 seconds.", 1);
  settimer(func {
   if(getprop("fdm/jsbsim/propulsion/engine[4]/set-running")) {
    APUBleed(1);
    settimer(func {
     GenAPU(1);
     AirconAux(1);
     chat("(engineer): APU started, bleed open, generator online.", 1);
     _StarterKeyBusy = 0;
    }, 10.0);
   }else{
    chat("(engineer): APU did not start.", 3);
    APUStartStop(0);
    GenAPU(0);
    AirconAux(0);
    settimer(func {
     APUPower(0);
    }, 10.0);
   }
  }, 60.0);
 }
}

var _StartAvionicsBusy = 0;
var StartAvionics = func() {
 if(_StartAvionicsBusy){
  chat("(copilot): Wait, I'm busy!..", 3);
  return;
 }
 if(
  !getprop("fdm/jsbsim/systems/elec/DC27-reserve/U-ok") or
  !getprop("fdm/jsbsim/systems/elec/AC36-reserve[0]/U-ok")
 ){
  chat("(copilot): Need 27V and 36V for avionics!", 3);
  return;
 }
 var mode = getprop("fdm/jsbsim/fcs/NPK/panel/PIK/switch/mode");
 if(mode < 2){
  _StartAvionicsBusy = 1;
  chat("(copilot): Preparing avionics, give me 2.5 minutes.", 1);
  NPKMode(1);
  PrepareInstruments();
  settimer(func {
   NPKMode(2);
  }, 85.0);
  settimer(func {
   AckMaster();
   chat("(copilot): Avionics in \"Prepare\". You can load flight plan.", 2);
   settimer(func {
    chat("(copilot): But wait another minute before fueling and boarding!", 2);
   }, 2.0);
  }, 87.0);
  settimer(func {
   chat("(copilot): Avionics ready. You can load flight plan, fuel and board.", 1);
   _StartAvionicsBusy = 0;
  }, 150.0);
 }else if(mode == 2){
  _StartAvionicsBusy = 1;
  chat("(copilot): Checking and starting avionics, 30 sec.", 1);
  setprop("fdm/jsbsim/fcs/NPK/panel/PIK/button/IS-test", 1);
  settimer(func {
   setprop("fdm/jsbsim/fcs/NPK/panel/PIK/button/IS-test", 0);
  }, 1.0);
  settimer(func {
   setprop("fdm/jsbsim/fcs/NPK/panel/PIK/button/IS-test", 1);
   settimer(func {
    setprop("fdm/jsbsim/fcs/NPK/panel/PIK/button/IS-test", 0);
   }, 1.0);
  }, 10.0);
  settimer(func {
   setprop("fdm/jsbsim/fcs/NPK/panel/PIK/button/SVS-test", 1);
   settimer(func {
    setprop("fdm/jsbsim/fcs/NPK/panel/PIK/button/SVS-test", 0);
   }, 1.0);
  }, 11.0);
  settimer(func {
   setprop("fdm/jsbsim/fcs/NPK/panel/PIK/button/SVS-test", 1);
   settimer(func {
    setprop("fdm/jsbsim/fcs/NPK/panel/PIK/button/SVS-test", 0);
   }, 1.0);
  }, 21.0);
  settimer(func {
   NPKMode(3);
   NPKIntegration(1);
   chat("(copilot): Avionics running.", 1);
   _StartAvionicsBusy = 0;
  }, 25.0);
 }else{
  chat("(copilot): But... Avionics are already up... Right?", 2);
 }
}


setlistener("fdm/jsbsim/fcs/panel/switch/canard", func(p) {
 var message = ["retract, latched", "retract", "neutral", "extend"][p.getValue()];
 var accent = [0, 3, 2, 2][p.getValue()];
 chat("Canard: " ~ message ~ ".", accent);
}, 0, 0);

setlistener("fdm/jsbsim/fcs/panel/switch/cone", func(p) {
 var message = ["retract, latched", "retract", "11 deg", "17 deg"][p.getValue()];
 var accent = [0, 3, 2, 2][p.getValue()];
 chat("Cone: " ~ message ~ ".", accent);
}, 0, 0);

setlistener("fdm/jsbsim/gear/panel/lever/gear", func(p) {
 var message = ["retract", "neutral. g to retract, G to latch", "neutral, latched", "neutral. G to extend, g to latch", "extend"][p.getValue() + 2];
 var accent = [2, 3, 0, 3, 2][p.getValue() + 2];
 chat("Gear: " ~ message ~ ".", accent);
}, 0, 0);

setlistener("controls/gear/brake-parking", func(p) {
 var message = ["off", "on"][p.getValue()];
 var accent = [0, 3][p.getValue()];
 chat("Start brake: " ~ message ~ ".", accent);
}, 0, 0);

setlistener("fdm/jsbsim/gear/panel/switch/parking-brake", func(p) {
 var message = ["off", "on"][p.getValue()];
 var accent = [0, 3][p.getValue()];
 chat("Parking brake: " ~ message ~ ".", accent);
}, 0, 0);

setlistener("fdm/jsbsim/systems/lighting/panel/switch/front", func(p) {
 var message = ["taxi", "off", "landing"][p.getValue() + 1];
 var accent = [2, 0, 2][p.getValue() + 1];
 chat("Front light: " ~ message ~ ".", accent);
}, 0, 0);


setlistener("sim/model/with-RD-36-61", func(p) {
 setprop("fdm/jsbsim/simulation/settings/with-RD-36-61", (p.getValue() or 0));
}, 1, 0);
setlistener("sim/model/with-fmc", func(p) {
 setprop("fdm/jsbsim/simulation/settings/with-fmc", (p.getValue() or 0));
}, 1, 0);
setlistener("sim/model/with-tcas", func(p) {
 setprop("fdm/jsbsim/simulation/settings/with-tcas", (p.getValue() or 0));
}, 1, 0);
