var _filename = nil;
var _recording = 0;

var _bufferSize = 12;
var _buffer = [];
setsize(_buffer, _bufferSize);
var _bufferInd = 0;
var _bufferFlush = func(){
 if(_filename != nil){
  var file = io.open(_filename, "a");
  for(var ii = 0; ii < _bufferInd; ii += 1) {
   io.write(file, _buffer[ii]);
  }
  io.close(file);
 }
 _bufferInd = 0;
}
var _setBufferSize = func(size) {
 _bufferFlush();
 _bufferSize = size;
 setsize(_buffer, _bufferSize);
}

var _timer = maketimer(0.08333333333333333, func{
 if(getprop("/sim/freeze/replay-state") and !getprop("/instrumentation/MSRP/record-from-replay")){
  return;
 }
 var recording = ((getprop("/instrumentation/MSRP/recording") or 0));
 if((recording > _recording)){
  _startFile();
 } else if(recording < _recording){
  _stopFile();
 }
 var _recording = recording;
 if(!recording){
  return;
 }
 var output = sprintf("%.4f", (getprop("/instrumentation/MSRP/time-sec") or 0.0));
 for(var ii = 0; ii < 12; ii += 1) {
  output = output ~ sprintf("\t%.4f", (getprop("instrumentation/MSRP/analog["~ ii ~"]") or 0.0));
 }
 for(var ii = 0; ii < 12; ii += 1) {
  output = output ~ sprintf("\t%u", (getprop("instrumentation/MSRP/pulse["~ ii ~"]") or 0));
 }
 output = output ~ "\n";
 _appendFile(output);
});
_timer.simulatedTime = 1;

var _appendFile = func(line){
 _buffer[_bufferInd] = line;
 _bufferInd += 1;
 if(_bufferInd >= _bufferSize) {
  _bufferFlush();
 }
}

var _startFile = func(){
 _stopFile();
 var timestamp = sprintf("%4u%02u%02u-%02u%02u%02u", (getprop("/sim/time/utc/year") or 0), (getprop("/sim/time/utc/month") or 0), (getprop("/sim/time/utc/day") or 0), (getprop("/sim/time/utc/hour") or 0), (getprop("/sim/time/utc/minute") or 0), (getprop("/sim/time/utc/second") or 0));
 _filename = getprop("/sim/fg-home") ~ "/Export/Tu-144D-MSRP-" ~ timestamp ~ ".txt";
 var file = io.open(_filename, "w");
 io.close(file);
 _timer.start();
 print("MSRP-12 recording: " ~ _filename);
}

var _stopFile = func(){
 if(_filename != nil){
  _bufferFlush();
  print("MSRP-12 stopped: " ~ _filename);
  _filename = nil;
 }
}

var restart = func(startIfStopped = 0){
 var rate = math.max((getprop("/instrumentation/MSRP/record-hz") or 12), 1);
 _setBufferSize(rate);
 if(startIfStopped or _timer.isRunning){
  _timer.restart(1.0 / rate);
 }
}

var stop = func(){
 _timer.stop();
 _stopFile();
}

setlistener("/instrumentation/MSRP/record-to-file", func(p) {
 if(p.getValue()){
  restart(1);
 } else {
  stop();
 }
}, 1, 0);

setlistener("/instrumentation/MSRP/record-hz", func(p) {
  restart();
}, 0, 0);
