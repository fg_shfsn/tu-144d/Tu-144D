# FIXME The whole "crew" thing is a trainwreck with epicycles on top of epicycles -- rewrite!
# FIXME Split up the main loop into one function per task and make an on/off option for each of them.
# FIXME Airspeed gauge is not a reliable source, e.g. in gusty wind.

# Settings.

var _control = 1;
var _callouts = 1;

# Orders.

var _Hzone = 10000;
var _Htransition = 3000;
var _Happroach = 2000;
var _retract_gear = 0;
var _retard_throttle = 0;
var _deploy_parachute = 0;
var _deploy_reversers = 0;


# State.

var _speaking = 0;

var _WOW = 1;
var _ready = 0;
var _readyfortaxi = 0;
var _Vind = 0.0;
var _rolling = 0;
var _braking = 0;
var _Vx_callout = "";
var _V_callout = "";
var _Hagl = 0.0;
var _Hx_callout = "";
var _H_callout = "";
var _threshold = 0;
var _landthrottle = 0;
var _chute = 0;
var _reversers = 0;
var _reversers_full = 0;
var _zone = 1;
var _transition = 1;
var _approach = 1;
var _my_frontlight = 0;

var _PPM = 1;
var _correction = 0;
var _correction_start = 0.0;
var _S_valid = 0;
var _S_valid_start = 0.0;
var _FL = 0;
var _cruise_end = 0;

var _gearpos = 0.0;
var _gear_callout = "gear_out";
var _canardpos = 0;
var _canard_callout = "canard_in";

var _engine = 0;
var _APU = 0;

var _Hdec = 0.0;



var say = func(sound, length, allowskip = 0) {
 if((sound == "") or !sound or !_callouts){
  return;
 }
 if(_speaking){
  if(allowskip){
   print("copilot: sound ", sound, " skipped.");
   return;
  }
  settimer(func{say(sound, length)}, 0.5, 1);
  print("copilot: sound ", sound, " in queue.");
  return;
 }
 _speaking = 1;
 setprop("/sim/crew/copilot/sound/" ~ sound, 1);
 settimer(func{
  setprop("/sim/crew/copilot/sound/" ~ sound, 0);
  _speaking = 0;
 }, length, 1);
}



var chat = func(text, emphasis = 0) {
 if(!_callouts){
  return;
 }
 Tu144D.chat("(copilot): " ~ text, emphasis);
}



var do = func(action) {
 if(!_control){
  return;
 }
 action();
}



var approx = func(val1, val2, tol) {
 return (abs(val1 - val2) < tol);
}



var CheckThreshold = func() {
 var ap = airportinfo();
 foreach(var i; keys(ap.runways)){
  var wp = geo.Coord.new().set_latlon(ap.runways[i].lat, ap.runways[i].lon);
  wp.apply_course_distance(
   ap.runways[i].heading,
   ap.runways[i].threshold
  );
  var (psi_brg, s) = courseAndDistance(wp);
  if(s < 0.024298056){ # 45 m
   return 1;
  }
 }
 return 0;
}



var LandingPrompt = func() {
 var emphasis = 0;
 var text = "DH: " ~ _Hdec ~ " m.";
 var actions = "";
 var question = "";
 if (_retard_throttle) {
  actions = actions ~ "retard at 10 m";
 }
 if (_deploy_parachute and _deploy_reversers) {
  if (actions != "") {
   actions = actions ~ " and ";
  }
  actions = actions ~ "use chute and reverse on touchdown";
  question = "Are you sure we need both?";
  emphasis = 1;
 } else if (_deploy_parachute) {
  if (actions != "") {
   actions = actions ~ " and ";
  }
  actions = actions ~ "deploy parachute on touchdown";
 } else if (_deploy_reversers) {
  if (actions != "") {
   actions = actions ~ " and ";
  }
  actions = actions ~  "deploy reversers on touchdown";
 }
 if (actions != "") {
  text = text ~ " I will " ~ actions ~ ".";
 }
 if (question != "") {
  text = text ~ " " ~ question;
 }
 chat(text, emphasis);
 if(!_WOW){
  settimer(func{
   var landing_tonnes = math.ceil(getprop("/fdm/jsbsim/inertia/weight-lbs") * 0.001 * LB2KG);
   var weighttext = "Vref(Vapp): " ~ Tu144D.Vref() ~ " km/h, LW: " ~ landing_tonnes ~ " tonnes.";
   var emphasis = 0;
   if(landing_tonnes > 132){
    weighttext = weighttext ~ " Listen, we're too heavy! Get under 132 tonnes!";
    emphasis = 2;
   }
   chat(weighttext, emphasis);
  }, 10.0);
 }
}


var TakeoffPrompt = func() {
 var emphasis = 0;
 var actions = "";
 var question = "";
 if (_retard_throttle) {
  actions = actions ~ "retard throttle";
 }
 if (_deploy_parachute and _deploy_reversers) {
  if (actions != "") {
   actions = actions ~ ", ";
  }
  actions = actions ~ "use chute and reverse";
  question = "Do we need both?";
  emphasis = 1;
 } else if (_deploy_parachute) {
  if (actions != "") {
   actions = actions ~ " and ";
  }
  actions = actions ~ "deploy parachute";
 } else if (_deploy_reversers) {
  if (actions != "") {
   actions = actions ~ " and ";
  }
  actions = actions ~ "deploy reversers";
 }
 if (actions != "") {
  actions = "If you need to stop, I will " ~ actions ~ ".";
 } else {
  return
 }
 if (question != "") {
  actions = actions ~ " " ~ question;
 }
 chat(actions, emphasis);
}


var LandingLight = func() {
 var my_frontlight = 1;
 if(my_frontlight == _my_frontlight) {
  return;
 }
 if(getprop("environment/effective-visibility-m") < 2500.0){
  chat("Too foggy -- landing light at pilot's discretion.", 2);
 } else if((getprop("environment/snow-norm") > 0.5) or (getprop("environment/rain-norm") > 0.5)){
  chat("Too much precipitation -- landing light at pilot's discretion.", 2);
 } else {
  Tu144D.Frontlight(my_frontlight);
 }
 _my_frontlight = my_frontlight;
}

var TaxiLight = func() {
 var my_frontlight = -1;
 if(my_frontlight == _my_frontlight) {
  return;
 }
 if(getprop("environment/effective-visibility-m") < 2500.0){
  chat("Too foggy -- taxi light at pilot's discretion.", 2);
 } else if((getprop("environment/snow-norm") > 0.75) or (getprop("environment/rain-norm") > 0.75)){
  chat("Too much precipitation -- taxi light at pilot's discretion.", 2);
 } else {
  Tu144D.Frontlight(my_frontlight);
 }
 _my_frontlight = my_frontlight;
}

var FrontLightOff = func() {
 var my_frontlight = 0;
 if(my_frontlight == _my_frontlight) {
  return;
 }
 Tu144D.Frontlight(my_frontlight);
 _my_frontlight = my_frontlight;
}



var InAirportZone = func() {
 var msg = "";
 var count = 0;
 var tmp = (getprop("sim/crew/orders/airport/comm[0]") or "");
 if(tmp != ""){
  Tu144D.COM(0, tmp);
  msg = msg ~ "COMM1: " ~ tmp ~ " MHz";
  count +=1;
 }
 tmp = (getprop("sim/crew/orders/airport/comm[1]") or "");
 if(tmp != ""){
  Tu144D.COM(1, tmp);
  if(count){
   msg = msg ~ ", ";
  }
  msg = msg ~ "COMM2: " ~ tmp ~ " MHz";
  count +=1;
 }
 tmp = getprop("sim/crew/orders/airport/magvar-deg");
 if((tmp != nil) and (tmp != "")){
  Tu144D.SetMagvar(tmp);
  if(count){
   msg = msg ~ ", ";
  }
  msg = msg ~ "variation: " ~ tmp ~ " deg";
  count +=1;
 }
 if(count){
  chat("Setting comm and nav for airport zone: " ~ msg ~ ".");
  settimer(func {
   chat("Radio and HSI source selection are on you!", 2);
  }, 1.0);
 }
}

var OutOfAirportZone = func(force = 0) {
 var msg = "";
 var count = 0;
 if(force or (getprop("sim/crew/orders/airport/comm[1]") or "") != ""){
  Tu144D.COM(1, 121.5);
  msg = msg ~ "COMM2: 121.5 MHz";
  count +=1;
 }
 var tmp = getprop("sim/crew/orders/airport/magvar-deg");
 if(force or (tmp != nil) and (tmp != "")){
  Tu144D.SetMagvar(0.0);
  if(count){
   msg = msg ~ ", ";
  }
  msg = msg ~ "variation: 0 deg";
  count +=1;
 }
 chat("Setting comm and nav for outside of airport zone: " ~ msg ~ ".");
 settimer(func {
  chat("Radio and HSI source selection are on you!", 2);
 }, 1.0);
}

var BelowTransition = func() {
 var QNH = (getprop("sim/crew/orders/airport/qnh-mmHg") or "");
 if(QNH != ""){
  say("altimeter", 2.0);
  chat(QNH ~ " mmHg.");
  Tu144D.SetQNHAll(QNH);
 }
}

var AboveTransition = func(force = 0) {
 if(force or (getprop("sim/crew/orders/airport/qnh-mmHg") or "") != ""){
  say("altimeter", 2.0);
  chat("760 mmHg.");
  Tu144D.SetQNHAll(760.0);
 }
}

var BeforeApproach = func() {
 var msg = "";
 var count = 0;
 var tmp = getprop("sim/crew/orders/airport/runway-heading");
 if((tmp != nil) and (tmp != "")){
  Tu144D.Rwy(tmp);
  msg = msg ~ "Runway heading: " ~ tmp;
  count +=1;
 }
 tmp = getprop("sim/crew/orders/airport/ap-course");
 if((tmp != nil) and (tmp != "")){
  Tu144D.Crs(tmp);
  if(count){
   msg = msg ~ ", ";
  }
  msg = msg ~ "AP course: " ~ tmp;
  count +=1;
 }
 tmp = (getprop("sim/crew/orders/airport/nav[0]") or "");
 if(tmp != ""){
  Tu144D.NAV(0, tmp);
  if(count){
   msg = msg ~ ", ";
  }
  if(tmp < 100.0){
   msg = msg ~ "RSBN ch. " ~ tmp;
  }else{
   msg = msg ~ "NAV1: " ~ tmp ~ " MHz";
  }
  count +=1;
 }
 tmp = getprop("sim/crew/orders/airport/nav-radial[0]");
 if((tmp != nil) and (tmp != "")){
  Tu144D.SK(0, tmp);
  if(count){
   msg = msg ~ ", ";
  }
  msg = msg ~ "radial 1: " ~ tmp;
  count +=1;
 }
 tmp = (getprop("sim/crew/orders/airport/nav[1]") or "");
 if(tmp != ""){
  Tu144D.NAV(1, tmp);
  if(!math.mod(count, 4)){
   msg = msg ~ sprintf(",\n");
  }else if(count){
   msg = msg ~ ", ";
  }
  if(tmp < 100.0){
   msg = msg ~ "Katet ch. " ~ tmp;
  }else{
   msg = msg ~ "NAV2: " ~ tmp ~ " MHz";
  }
  count +=1;
 }
 tmp = getprop("sim/crew/orders/airport/nav-radial[1]");
 if((tmp != nil) and (tmp != "")){
  Tu144D.SK(1, tmp);
  if(!math.mod(count, 4)){
   msg = msg ~ sprintf(",\n");
  }else if(count){
   msg = msg ~ ", ";
  }
  msg = msg ~ "radial 2: " ~ tmp;
  count +=1;
 }
 tmp = (getprop("sim/crew/orders/airport/adf-outer") or getprop("sim/crew/orders/airport/adf-inner") or "");
 var tmp2 = (getprop("sim/crew/orders/airport/adf-inner") or getprop("sim/crew/orders/airport/adf-outer") or "");
 if((tmp != "") and (tmp2 != "")){
  Tu144D.ADF(0, tmp);
  Tu144D.ADF(1, tmp2);
  if(!math.mod(count, 4)){
   msg = msg ~ sprintf(",\n");
  }else if(count){
   msg = msg ~ ", ";
  }
  msg = msg ~ "ADF outer " ~ tmp ~ " kHz, inner " ~ tmp2 ~ " kHz";
  count +=1;
 }
 if(count){
  chat("Setting nav for approach: " ~ msg ~ ".");
  settimer(func {
   chat("Navaid selection and AP modes are on you!", 2);
  }, 1.0);
 }
}



var _loop = func {

 if(
  getprop("/sim/freeze/replay-state") or
  getprop("/fdm/jsbsim/systems/airframe/damage/break")
 ){
  return;
 }

 var WOW = getprop("/gear/gear[1]/wow") or getprop("/gear/gear[2]/wow");
 var Vind = getprop("/fdm/jsbsim/fcs/NPK/instr/US-I[1]/gauge/V-km_h");
 var ready = (!getprop("/fdm/jsbsim/fcs/NPK/status/takeoff-notready") or (Vind > 100.0));
 if(WOW){
  for(var ii = 0; ii < 4; ii += 1){
   if(getprop("/fdm/jsbsim/propulsion/engine[" ~ ii ~ "]/set-running") == 0){
    ready = 0;
    break;
   }
  }
 }
 var readyfortaxi = !getprop("/fdm/jsbsim/fcs/NPK/status/taxi-notready") and getprop("fdm/jsbsim/fcs/NPK/elec/DC27-ok");
 var Hagl = getprop("/fdm/jsbsim/position/h-agl-ft") * FT2M - 4.53;
 var Habs = getprop("/fdm/jsbsim/atmosphere/kludge/Habs-m");
 var rolling = (Vind > 160.0);
 var chute = ((getprop("/fdm/jsbsim/fcs/chute/pos-norm") > 0.01) and !getprop("/fdm/jsbsim/fcs/chute/dropped"));
 var reversers = 0;
 for(var ii = 0; ii < 4; ii += 3){
  if(
   (getprop("/fdm/jsbsim/propulsion/engine[" ~ ii ~ "]/panel/lever/throttle/pos-deg") < 0.1) and
   ((getprop("/fdm/jsbsim/propulsion/engine[" ~ ii ~ "]/panel/lever/reverser/pos-deg") or 0.0) > 5.0)
  ){
   reversers = 1;
   break;
  }
 }
 var braking = (WOW and rolling) or (_braking and (Vind > 50.0));
 if(!reversers and !chute){
  for(var ii = 0; ii < 4; ii += 1){
   if(getprop("/fdm/jsbsim/propulsion/engine[" ~ ii ~ "]/panel/lever/throttle/pos-deg") > 60.0){
    braking = 0;
    break;
   }
  }
 }
 var PPM = !WOW and  getprop("/fdm/jsbsim/fcs/NPK/panel/light/turn");
 var correction_type = getprop("fdm/jsbsim/fcs/NPK/panel/switch/correction");
 var correction = 0;
 if(getprop("/fdm/jsbsim/fcs/NPK/panel/light/correction") and (
  # 0: RSBN
  # 1: RSDN (instead of unimplemented VOR-VOR DME-DME)
  # 2. VOR-DME
  ((correction_type == 0) and (getprop("fdm/jsbsim/fcs/NPK/KURS-MP/RSBN/switch/RSBN-ch") == getprop("fdm/jsbsim/fcs/NPK/Orbita/func/corr/RM/f"))) or
  (correction_type == 1) or
  ((correction_type == 2) and (getprop("fdm/jsbsim/fcs/NPK/KURS-MP/SDK/SDK[0]/f-MHz") == getprop("fdm/jsbsim/fcs/NPK/Orbita/func/corr/RM/f")) and (getprop("fdm/jsbsim/fcs/NPK/panel/PK/pot/variation-deg") == Tu144D.MagvarPrompt()))
 )){
  if(getprop("fdm/jsbsim/simulation/sim-time-sec") - _correction_start > 10.0){
   correction = 1;
  }
 }else{
  _correction_start = getprop("fdm/jsbsim/simulation/sim-time-sec");
 }
 var S = abs(getprop("fdm/jsbsim/fcs/NPK/Orbita/func/dist/dist/distance-km"));
 var S_valid = 0;
 if(!(getprop("fdm/jsbsim/fcs/NPK/panel/TsUNP/status/report-route") or getprop("fdm/jsbsim/fcs/NPK/panel/TsUNP/status/report-direct"))){
  if(getprop("fdm/jsbsim/simulation/sim-time-sec") - _S_valid_start > 10.0){
   S_valid = 1;
  }
 }else{
  _S_valid_start = getprop("fdm/jsbsim/simulation/sim-time-sec");
 }
 var FL = !WOW * (
  (S < (getprop("systems/ground-supply/dialog/fuelprompt/level-change-km[0]") or 0.0)) +
  (S < (getprop("systems/ground-supply/dialog/fuelprompt/level-change-km[1]") or 0.0)) +
  (S < (getprop("systems/ground-supply/dialog/fuelprompt/level-change-km[2]") or 0.0)) +
  (S < (getprop("systems/ground-supply/dialog/fuelprompt/level-change-km[3]") or 0.0))
 );
 var cruise_end = !WOW and (S < getprop("systems/ground-supply/dialog/fuelprompt/end-of-cruise-km"));
 var ngear = 0;
 var gearpos = 0.0;
 for(var ii = 0; ii < 4; ii += 1){
  gearpos += getprop("/fdm/jsbsim/gear/unit[" ~ ii ~ "]/actuator-norm");
 }
 gearpos *= 0.25;
 var canardpos = getprop("/fdm/jsbsim/fcs/canard/pos-norm");
 var engine = 0;
 for(var ii = 0; ii < 4; ii += 1){
  if(getprop("/fdm/jsbsim/propulsion/engine[" ~ ii ~ "]/set-running")){
   engine = 1;
   break;
  }
 }
 var APU = getprop("/fdm/jsbsim/propulsion/engine[4]/set-running");
 var marker = getprop("/fdm/jsbsim/fcs/NPK/panel/light/marker-middle") and !getprop("/fdm/jsbsim/fcs/NPK/ABSU/status/test-lights");
 var frontlight = getprop("/fdm/jsbsim/systems/lighting/panel/switch/front");

 if(WOW > _WOW){
  say("touchdown", 1.0, 1);
 }

 if(ready > _ready){
  do(func{
   Tu144D.Window(1, 0.0);
  });
  settimer(func {
   do(func{
    LandingLight();
    Tu144D.IntlightsTOL();
   });
  }, 7.0);
  settimer(func {
   say("ready", 3.0);
   TakeoffPrompt();
  }, 10.0);
 }

 if(rolling > _rolling){
  say("rolling", 1.5);
  do(func{
   Tu144D.Stopwatch(1, 1);
   LandingLight();
   Tu144D.IntlightsTOL();
   Tu144D.Window(1, 0.0);
  });
 } else if(rolling < _rolling){
  do(func{
   Tu144D.Stopwatch(1, 2);
   TaxiLight();
   Tu144D.IntlightsDefault();
  });
 }

 if(rolling and !braking){
  var V_callout = "";
  if(Vind > _Vind){
   var Vx_callout = "";
   if((gearpos > 0.999) and (Vind > Tu144D.V2())){
    Vx_callout = "safety";
   }else if(WOW){
    if(Vind > Tu144D.Vr()){
     Vx_callout = "rotate";
    }else if(Vind > Tu144D.V1()){
     Vx_callout = "decision";
    }
   }
   if((Vx_callout != _Vx_callout)){
    say(Vx_callout, 0.7);
   }
   _Vx_callout = Vx_callout;
  }
  if(WOW){
   if(Vind > 320.0){
    V_callout = "V_320";
   }else if(Vind > 300.0){
    V_callout = "V_300";
   }else if(Vind > 280.0){
    V_callout = "V_280";
   }else if(Vind > 260.0){
    V_callout = "V_260";
   }else if(Vind > 240.0){
    V_callout = "V_240";
   }
   if((V_callout != _V_callout)){
    say(V_callout, 1.0, 1);
   }
  }
  _V_callout = V_callout;
 }

 if(WOW < _WOW){
  settimer(func {
   if (_Vind > Tu144D.V2() and _retract_gear) {
    do(func {
     Tu144D.GearCmd(-2);
    });
   }
  }, 5.0);
 }

 var gear_callout = "";
 if(gearpos < _gearpos){
  gear_callout = "gear_ret";
 }else if(gearpos > _gearpos){
  gear_callout = "gear_ext";
 }else{
  if(gearpos < 0.001){
   gear_callout = "gear_in";
  };
  if(gearpos > 0.999){
   gear_callout = "gear_out";
  };
 }
 if(gear_callout != _gear_callout){
  say(gear_callout, 2.0);
  if(gear_callout == "gear_in"){
   settimer(func{
    do(func{
     Tu144D.GearCmd(0);
     FrontLightOff();
     Tu144D.IntlightsDefault();
    });
   }, 1.0);
  }else if(gear_callout == "gear_out"){
   Tu144D.GearCmd(0);
   LandingPrompt();
  }else if(gear_callout == "gear_ret"){
    do(func{
     FrontLightOff();
    });
  }
 }
 _gear_callout = gear_callout;

 var canard_callout = "";
 if(canardpos < _canardpos){
  canard_callout = "canard_ret";
 }else if(canardpos > _canardpos){
  canard_callout = "canard_ext";
 }else{
  if(canardpos < 0.001){
   canard_callout = "canard_in";
  };
  if(canardpos > 0.999){
   canard_callout = "canard_out";
  };
 }
 if(canard_callout != _canard_callout){
  say(canard_callout, 2.0);
 }
 _canard_callout = canard_callout;

 var zone = Habs < _Hzone;
 if(zone > _zone){
  InAirportZone();
 }else if(zone < _zone){
  OutOfAirportZone();
 }
 _zone = zone;

 var transition = Habs < _Htransition;
 if(transition > _transition){
  BelowTransition();
 }else if(transition < _transition){
  AboveTransition();
 }
 _transition = transition;

 var approach = Habs < _Happroach;
 if(approach > _approach){
  BeforeApproach();
 }
 _approach = approach;

 if((PPM > _PPM) and getprop("sim/crew/orders/reminder/PPM")){
  say("PPM", 1.75);
 }
 if((correction > _correction) and getprop("sim/crew/orders/reminder/correction")){
  say("correction", 2.0);
 }
 if(S_valid and !getprop("fdm/jsbsim/simulation/settings/with-fmc")){
  if((FL > _FL) and getprop("sim/crew/orders/reminder/FL")){
   say("FL", 2.65);
   if(FL == 1){
    chat("16 -> 16.5 km");
   }else if(FL == 2){
    chat("16.5 -> 17 km");
   }else if(FL == 3){
    chat("17 -> 17.5 km");
   }else if(FL == 4){
    chat("17.5 -> 18 km");
   }
  }
  if((cruise_end > _cruise_end) and getprop("sim/crew/orders/reminder/cruise-end")){
   say("cruise_end", 2.10);
  }
 }

 _Hdec = 0.0;
 for(var ii = 0; ii < 2; ii +=1){
  var tmp = getprop("/fdm/jsbsim/fcs/RV[" ~ ii ~ "]/instr/UV/pot/Hset-m");
  _Hdec = math.max(_Hdec, tmp);
 }
 var Hpredec = _Hdec + 30.0;

 if(!WOW and (Hagl < _Hagl)){
  var Hx_callout = "";
  var H_callout = "";
  if(approx(Hagl, _Hdec, 10.0)){
   Hx_callout = "decision";
   do(func{
    LandingLight();
   });
  }else if(approx(Hagl, Hpredec, 10.0)){
   Hx_callout = "predecision";
  }
  if(Hx_callout != _Hx_callout){
   say(Hx_callout, 1.2);
  }
  var skip = 1;
  var length = 1.92;
  if(approx(Hagl, 1.0, 0.75)){
   H_callout = "H_1";
   skip = 0;
   length = 1.1;
  }else if(approx(Hagl, 3.0, 0.75)){
   H_callout = "H_3";
   skip = 0;
   length = 1.1;
  }else if(approx(Hagl, 6.0, 1.5)){
   H_callout = "H_6";
   skip = 0;
   length = 1.1;
  }else if(approx(Hagl, 10.0, 1.5)){
   H_callout = "H_10";
   skip = 0;
   length = 1.1;
  }else if(approx(Hagl, 20.0, 3.0)){
   H_callout = "H_20";
  }else if(approx(Hagl, 30.0, 3.0)){
   H_callout = "H_30";
  }else if(approx(Hagl, 40.0, 3.0)){
   H_callout = "H_40";
  }else if(approx(Hagl, 60.0, 5.0)){
   H_callout = "H_60";
  }else if(approx(Hagl, 80.0, 5.0)){
   H_callout = "H_80";
  }else if(approx(Hagl, 100.0, 5.0)){
   H_callout = "H_100";
  }else if(approx(Hagl, 120.0, 5.0)){
   H_callout = "H_120";
  }else if(approx(Hagl, 150.0, 10.0)){
   H_callout = "H_150";
  }else if(approx(Hagl, 200.0, 10.0)){
   H_callout = "H_200";
  }else if(approx(Hagl, 250.0, 10.0)){
   H_callout = "H_250";
  }
  if(H_callout != _H_callout){
   say(H_callout, length, skip);
  }
  _Hx_callout = Hx_callout;
  _H_callout = H_callout;
  if(gearpos > 0.999){
   var landthrottle = (Hagl < 10.0);
   if (landthrottle > _landthrottle and _retard_throttle) {
    do(func {
     Tu144D.ThrottleAll(0.0);
    });
   }
   _landthrottle = landthrottle;
  }
  var threshold = 0;
  if(Hagl < 20.0){
   threshold = CheckThreshold();
   if(threshold > _threshold){
    say("threshold", 1.0);
   }
   _threshold = threshold;
  }
 }

 if (braking > _braking and _retard_throttle) {
  do(func {
   Tu144D.ThrottleAll(0.0);
  });
 }

 if(braking){
  do(func {
   if(!chute and _deploy_parachute){
     Tu144D.Chute(1);
   }
   if(!reversers and _deploy_reversers){
    Tu144D.ReversersAll(25.0);
   }
   if(reversers and _deploy_reversers and (Vind < 70.0)){
    Tu144D.ReversersAll(10.0);
   }
  });
 }

 if(chute > _chute){
  settimer(func {
   say("parachute", 2.0);
  }, 1.0);
 }else if(chute < _chute){
  settimer(func {
   say("parachute_drop", 2.0);
  }, 1.0);
 }
 if(reversers > _reversers){
  settimer(func {
   say("reversers", 1.0);
  }, 1.0);
 }

 if(braking < _braking){
  do(func{
   if(chute){
     Tu144D.Chute(0);
   }
   Tu144D.ReversersAll(0.0);
  });
 }

 if((engine > _engine) or (APU > _APU)){
  do(func{
   Tu144D.Navlight(1);
  });
 }else if(WOW and ((engine or APU) < (_engine or _APU))){
  do(func{
   Tu144D.Navlight(0);
  });
 }

 if(engine > _engine){
  do(func{
   Tu144D.Strobelight(1);
  });
 }else if(WOW and (engine < _engine)){
  do(func{
   Tu144D.Strobelight(0);
   Tu144D.Navlight(0);
  });
 }

 if(WOW){
  if(readyfortaxi > _readyfortaxi){
   do(func{
    TaxiLight();
    Tu144D.IntlightsDefault();
   });
  }else if(readyfortaxi < _readyfortaxi){
   do(func{
    FrontLightOff();
   });
  }
 }else if((Hagl < 3000.0) and (gearpos > 0.999)){
  if(!frontlight){
   do(func{
    TaxiLight();
    Tu144D.IntlightsTOL();
   });
  }else if(marker){
   do(func{
    LandingLight();
   });
  }
 }

 _WOW = WOW;
 _ready = ready;
 _readyfortaxi = readyfortaxi;
 _Vind = Vind;
 _Hagl = Hagl;
 _rolling = rolling;
 _braking = braking;
 _chute = chute;
 _reversers = reversers;
 _PPM = PPM;
 _correction = correction;
 if(S_valid){
  _FL = FL;
  _cruise_end = cruise_end;
 }
 _gearpos = gearpos;
 _canardpos = canardpos;
 _engine = engine;
 _APU = APU;

}


var _timer = maketimer(1.0, func{_loop()});
_timer.simulatedTime = 1;


var node_active_accel = props.globals.getNode("/fdm/jsbsim/crew/profile-track/active-accel");
var node_active_decel = props.globals.getNode("/fdm/jsbsim/crew/profile-track/active-decel");
var node_pitch_force_in = props.globals.getNode("/fdm/jsbsim/crew/profile-track/pitch-force");
var node_pitch_force_out = props.globals.getNode("/fdm/jsbsim/fcs/NPK/panel/PSU/pot/pitch-deg");
var profile_track_loop = func {
 if (node_active_accel.getValue() or node_active_decel.getValue()) node_pitch_force_out.setValue(node_pitch_force_in.getValue() * 155.0);
}
var profile_track_timer = maketimer(0.1, profile_track_loop);



setlistener("/sim/signals/fdm-initialized", func(p) {
 if(p.getValue() == 0){
  return;
 }
 _gearpos = getprop("/fdm/jsbsim/gear/gear-pos-norm");
 _canardpos = getprop("/fdm/jsbsim/fcs/canard/pos-norm");
 var overlay = getprop("/nasal/overlay/name") or "";
 if((overlay == "subsonic") or (overlay == "cruise") or (overlay == "approach")){
  _ready = 1;
  _rolling = 1;
  _Vx_callout = "safety";
 }
  _canard_callout = "canard_in";
 if((overlay == "subsonic") or (overlay == "cruise")){
  _gear_callout = "gear_in";
 }
 if((overlay == "takeoff") or (overlay == "approach")){
  _canard_callout = "canard_out";
 }
 settimer(func {
  _timer.start();
  profile_track_timer.start();
 }, 2.0);
});

setlistener("/sim/crew/copilot/control", func(p) {
 _control = p.getValue();
}, 1, 0);

setlistener("/sim/crew/copilot/callouts", func(p) {
 _callouts = p.getValue();
}, 1, 0);

setlistener("/sim/crew/orders/airport/approach-m", func(p) {
 _Happroach = p.getValue();
}, 1, 0);

setlistener("/sim/crew/orders/airport/transition-m", func(p) {
 _Htransition = p.getValue();
}, 1, 0);

setlistener("/sim/crew/orders/airport/zone-m", func(p) {
 _Hzone = p.getValue();
}, 1, 0);

setlistener("/sim/crew/orders/set-qnh", func(p) {
 _set_qnh = p.getValue();
}, 1, 0);

setlistener("/sim/crew/orders/altimeter-mmHg", func(p) {
 _set_qnh_mmhg = p.getValue();
}, 1, 0);

setlistener("/sim/crew/orders/takeoff/retract-gear", func(p) {
 _retract_gear = p.getValue();
}, 1, 0);

setlistener("/sim/crew/orders/landing/retard-throttle", func(p) {
 _retard_throttle = p.getValue();
}, 1, 0);

setlistener("/sim/crew/orders/landing/parachute", func(p) {
 _deploy_parachute = p.getValue();
}, 1, 0);

setlistener("/sim/crew/orders/landing/reversers", func(p) {
 _deploy_reversers = p.getValue();
}, 1, 0);
