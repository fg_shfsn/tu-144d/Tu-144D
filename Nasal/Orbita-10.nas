# Nasal helpers for Orbita-10 FMC

var ClearFlightplan = func() {
 if(props.globals.getNode("/sim/temp/Orbita") != nil){
  props.globals.getNode("/sim/temp/Orbita").remove();
 }
 CopyLoaded();
 setprop("/fdm/jsbsim/fcs/NPK/Orbita/flightplan/ANZ-kg", 7000.0);
}

var LoadFlightplan = func(file) {
 ClearFlightplan();
 # First load the plan somewhere else, then copy values one by one, to
 # prevent a malformed plan from corrupting JSBSim tree.
 fgcommand("loadxml", {
  'filename'   : file,
  'targetnode' : '/sim/temp/Orbita'
 });
 CopyLoaded();
 Tu144D.NPKLandingAirport(getprop("/fdm/jsbsim/fcs/NPK/Orbita/flightplan/AP/count"));
 Plot(getprop("/sim/gui/dialog/Orbita10/plottype") or "PPM");
}


# @brief Visualise points on the FlightGear map.
# @param type Points. Can be "PPM", "OPM", "RM", "AP". Any other value clears the plot.
#
var Plot = func(type) {
 if(getprop("fdm/jsbsim/simulation/settings/with-fmc") or 0) {
  return;
 }
 var nfp = flightplan();
 nfp.cleanPlan();
 if((type != "PPM") and (type != "OPM") and (type != "RM") and (type != "AP")){
  return;
 }
 var count = getprop("/fdm/jsbsim/fcs/NPK/Orbita/flightplan/" ~ type ~ "/count");
 for(var i = 1; i <= count; i += 1){
  var phi = getprop("/fdm/jsbsim/fcs/NPK/Orbita/flightplan/" ~ type ~ "/" ~ type ~ "[" ~ i ~ "]/phi-deg");
  var lam = getprop("/fdm/jsbsim/fcs/NPK/Orbita/flightplan/" ~ type ~ "/" ~ type ~ "[" ~ i ~ "]/lam-deg");
  var wp = createWP(phi, lam, type ~ " " ~ i);
  nfp.appendWP(wp);
 }
 # FGBUG Map can't plot just one point.
 if(count == 1){
  var phi = getprop("/fdm/jsbsim/fcs/NPK/Orbita/flightplan/" ~ type ~ "/" ~ type ~ "[1]/phi-deg");
  var lam = getprop("/fdm/jsbsim/fcs/NPK/Orbita/flightplan/" ~ type ~ "/" ~ type ~ "[1]/lam-deg");
  var wp = createWP(phi, lam, type ~ " 1");
  nfp.appendWP(wp);
 }
 # END FGBUG
 if(
  (type == "AP" or type == "RM") and
  getprop("/fdm/jsbsim/fcs/NPK/Orbita/flightplan/" ~ type ~ "/oper-entered")
 ){
  # type == "RM":
  var i = 10;
  if(type == "AP"){
   i = 9;
  }
  var phi = getprop("/fdm/jsbsim/fcs/NPK/Orbita/flightplan/" ~ type ~ "/" ~ type ~ "[" ~ i ~ "]/phi-deg");
  var lam = getprop("/fdm/jsbsim/fcs/NPK/Orbita/flightplan/" ~ type ~ "/" ~ type ~ "[" ~ i ~ "]/lam-deg");
  var ins = count + 1;
  var wp = createWP(phi, lam, type ~ " " ~ i);
  nfp.appendWP(wp);
 }
 # Show departure and destination airport in the PPM mode.
 if(type == "PPM"){
  var phi = getprop("/fdm/jsbsim/fcs/NPK/Orbita/func/departure/AP/phi-deg") or 0.0;
  var lam = getprop("/fdm/jsbsim/fcs/NPK/Orbita/func/departure/AP/lam-deg") or 0.0;
  if((phi != 0.0) and (lam != 0.0)){
   nfp.departure = airportinfo(phi, lam);
  }
  phi = getprop("/fdm/jsbsim/fcs/NPK/Orbita/func/airport/AP/phi-deg") or 0.0;
  lam = getprop("/fdm/jsbsim/fcs/NPK/Orbita/func/airport/AP/lam-deg") or 0.0;
  if((phi != 0.0) and (lam != 0.0)){
   nfp.destination = airportinfo(phi, lam);
   if(nfp.destination != nil){
    var psi = getprop("/fdm/jsbsim/fcs/NPK/Orbita/func/airport/AP/psi-deg");
    var rwy = findrwy(nfp.destination, psi);
    if(rwy != nil){
     nfp.destination_runway = nfp.destination.runways[rwy];
    }
   }
  }
 }
}

# @brief Find the longest runway in the airport with the heading.
# @param ap Airport object.
# @param psi Heading of the runway, deg.
# @param tol Magnetic heading tolerance, deg. Default: 0.1.
var findrwy = func(ap, psi, tol = 0.1) {
 var rwy = nil;
 var score = 0.0;
 foreach(var i; keys(ap.runways)){
  if(abs(geo.normdeg180(ap.runways[i].heading - psi)) < tol){
   # Select the longest runway if there are multiple.
   if(ap.runways[i].length > score){
    rwy = i;
    score = ap.runways[i].length;
   }
  }
 }
 return rwy;
}


# @brief Copy the loaded flightplan to JSBSim (sanitised).
var CopyLoaded = func {
 var PPMparams = ["phi-deg", "lam-deg", "H-km"];
 var APparams = [
 "phi-deg", "lam-deg", "psi-deg[0]", "psi-deg[1]",
 "psi_twy-deg[0]", "psi_twy-deg[1]"
 ];
 var RMparams = ["phi-deg", "lam-deg", "f"];
 JSBSafeCopy("PPM/count");
 JSBSafeCopy("OPM/count");
 JSBSafeCopy("AP/count");
 JSBSafeCopy("AP/oper-entered");
 JSBSafeCopy("RM/count");
 JSBSafeCopy("RM/oper-entered");
 JSBSafeCopy("ANZ-kg");
 for(var i = 1; i < 31; i += 1){
  foreach(var param; PPMparams){
   JSBSafeCopy("PPM/PPM[" ~ i ~ "]/" ~ param);
  }
 }
 for(var i = 1; i < 11; i += 1){
  foreach(var param; PPMparams){
   JSBSafeCopy("OPM/OPM[" ~ i ~ "]/" ~ param);
  }
 }
 for(var i = 1; i < 10; i += 1){
  foreach(var param; APparams){
   JSBSafeCopy("AP/AP[" ~ i ~ "]/" ~ param);
  }
 }
 for(var i = 1; i < 11; i += 1){
  foreach(var param; RMparams){
   JSBSafeCopy("RM/RM[" ~ i ~ "]/" ~ param);
  }
 }
}

# @brief Copy a value to the JSBSim tree, making sure it is a float.
var JSBSafeCopy = func(p) {
 var value = getprop("/sim/temp/Orbita/" ~ p);
 if(value != nil){
  # SIC "+ 0.0" converts to number.
  # FIXME Get "non-numeric string in numeric context" on strings here. Is there any other way to get rid of a string?
  setprop("/fdm/jsbsim/fcs/NPK/Orbita/flightplan/" ~ p, value + 0.0);
 }else{
  setprop("/fdm/jsbsim/fcs/NPK/Orbita/flightplan/" ~ p, 0.0);
 }
}

# @brief Save flightplan to a XML file.
var SaveFlightplan = func(file) {
 io.write_properties(file, "/fdm/jsbsim/fcs/NPK/Orbita/flightplan");
}

var _LoadFlightplanDialogCallback = func(filenode) {
 LoadFlightplan(filenode.getValue());
}

var LoadFlightplanDialog= func() {
 var dir = getprop("/sim/fg-home") ~ "/Export";
 var selector = gui.FileSelector.new(
  callback: _LoadFlightplanDialogCallback,
  dir: dir,
  dotfiles: 1,
  title: "Load Orbita-10 memory",
  button: "Load"
 );
 selector.open();
 selector.del();
}

var _SaveFlightplanDialogCallback = func(filenode) {
 SaveFlightplan(filenode.getValue());
}

var SaveFlightplanDialog= func() {
 var dir = getprop("/sim/fg-home") ~ "/Export";
 var selector = gui.FileSelector.new(
  callback: _SaveFlightplanDialogCallback,
  dir: dir,
  dotfiles: 1,
  title: "Save Orbita-10 memory",
  button: "Save"
 );
 selector.open();
 selector.del();
}


# @brief Import route from FG route manager flight plan.
var ImportFGFPPPM = func(file) {
 var fgfp = flightplan(file);
 var H = fgfp.cruiseAltitudeFt * FT2M * 0.001;
 # Route waypoints.
 var count = 0;
 for(var ii = 0; ii < fgfp.getPlanSize(); ii += 1){
  var wp = fgfp.getWP(ii);
  if(count > 29){
   print("PPM > 30 ignored starting with wp " ~ ii ~ " (" ~ wp.id ~ ").");
   break;
  }
  if((wp.wp_type == "basic") or (wp.wp_type == "navaid")){
   setprop("/fdm/jsbsim/fcs/NPK/Orbita/flightplan/PPM/PPM[" ~ (count + 1) ~ "]/phi-deg", wp.lat);
   setprop("/fdm/jsbsim/fcs/NPK/Orbita/flightplan/PPM/PPM[" ~ (count + 1) ~ "]/lam-deg", wp.lon);
   if(wp.alt_cstr_type != "at" or wp.alt_cstr == nil){
    setprop("/fdm/jsbsim/fcs/NPK/Orbita/flightplan/PPM/PPM[" ~ (count + 1) ~ "]/H-km", H);
   }else{
    setprop("/fdm/jsbsim/fcs/NPK/Orbita/flightplan/PPM/PPM[" ~ (count + 1) ~ "]/H-km", wp.alt_cstr * FT2M * 0.001);
   }
   count += 1;
  }
 }
 setprop("/fdm/jsbsim/fcs/NPK/Orbita/flightplan/PPM/count", count);
 Plot(getprop("/sim/gui/dialog/Orbita10/plottype"));
}

# @brief Import dummy FG plan with airports.
var ImportFGFPAP = func(file) {
 var fgfp = flightplan(file);
 # Diversion airports.
 var count = 0;
 for(var ii = 0; ii < fgfp.getPlanSize(); ii += 1){
  # SIC First and last AP used for departure and destination.
  var wp = fgfp.getWP(ii);
  if(wp.wp_type != "navaid"){
   continue;
  }
  var ap = airportinfo(wp.lat, wp.lon);
  if(count > 5){
   print("AP > 8 ignored starting with wp " ~ ii ~ " (" ~ ap.id ~ ").");
   break;
  }
  if((ap == nil) or (ap.id != wp.wp_name)){
   continue;
  }
  setprop("/fdm/jsbsim/fcs/NPK/Orbita/flightplan/AP/AP[" ~ (count + 2) ~ "]/phi-deg", ap.lat);
  setprop("/fdm/jsbsim/fcs/NPK/Orbita/flightplan/AP/AP[" ~ (count + 2) ~ "]/lam-deg", ap.lon);
  # Find 2 longest runways.
  var psi = [0.0, 0.0];
  var score = [0.0, 0.0];
  foreach(var jj; keys(ap.runways)){
   var area = ap.runways[jj].length * math.min(ap.runways[jj].width, 45.0);
   if(area > score[0]){
    score[1] = score[0];
    psi[1] = psi[0];
    score[0] = area;
    psi[0] = ap.runways[jj].heading;
   }
  }
  for(var jj = 0; jj < 1; jj += 2){
   setprop("/fdm/jsbsim/fcs/NPK/Orbita/flightplan/AP/AP[" ~ (count + 2) ~ "]/psi-deg[" ~ 0 ~ "]", psi[jj]);
  }
  count += 1;
 }
 # Departure and destination airports.
 for(var ii = 0; ii < 2; ii += 1){
  var fprwy = nil;
  var ap = nil;
  if(!ii){
   ap = fgfp.departure;
   fprwy = fgfp.departure_runway;
  }else{
   ap = fgfp.destination;
   fprwy = fgfp.destination_runway;
  }
  if(ap == nil){
   continue;
  }
  setprop("/fdm/jsbsim/fcs/NPK/Orbita/flightplan/AP/AP[" ~ (ii * count + 1) ~ "]/phi-deg", ap.lat);
  setprop("/fdm/jsbsim/fcs/NPK/Orbita/flightplan/AP/AP[" ~ (ii * count + 1) ~ "]/lam-deg", ap.lon);
  # Find 2 longest runways.
  var psi = [0.0, 0.0];
  var score = [0.0, 0.0];
  foreach(var jj; keys(ap.runways)){
   var area = ap.runways[jj].length * math.min(ap.runways[jj].width, 45.0);
   if(area > score[0]){
    score[1] = score[0];
    psi[1] = psi[0];
    score[0] = area;
    psi[0] = ap.runways[jj].heading;
   }
  }
  # If runway defined in flightplan, use it.
  if(fprwy != nil){
   psi[1] = psi[0];
   psi[0] = fprwy.heading;
  }
  for(var jj = 0; jj < 2; jj += 1){
   setprop("/fdm/jsbsim/fcs/NPK/Orbita/flightplan/AP/AP[" ~ (ii * count + 1) ~ "]/psi-deg[" ~ jj ~ "]", psi[jj]);
  }
  count += 1;
 }
 setprop("/fdm/jsbsim/fcs/NPK/Orbita/flightplan/AP/count", count);
 Tu144D.NPKDepartureAirport(1);
 Tu144D.NPKLandingAirport(getprop("/fdm/jsbsim/fcs/NPK/Orbita/flightplan/AP/count"));
 Plot(getprop("/sim/gui/dialog/Orbita10/plottype"));
}

# @brief Import dummy FG plan with beacons.
var ImportFGFPRM = func(file) {
 var fgfp = flightplan(file);
 var count = 0;
 for(var ii = 0; ii < fgfp.getPlanSize(); ii += 1){
  var wp = fgfp.getWP(ii);
  if(count > 8){
   print("RM > 9 ignored starting with wp " ~ ii ~ " (" ~ wp.id ~ ").");
   break;
  }
  if(wp.wp_type == "navaid"){
   var navaids = findNavaidsWithinRange(wp.lat, wp.lon, 1.0);
   if((navaids == nil) or (size(navaids) == 0)){
    continue;
   }
# FGBUG This never shows any DME...
#   var hasDME = 0;
#   for(var jj = 0; jj < size(navaids); jj += 1){
#print(ii ~ " " ~ jj ~ " " ~ navaids[jj].type);
#    if(navaids[jj].type == "DME"){
#     hasDME = 1;
#     break;
#    }
#   }
#   if(!hasDME){
#    continue;
#   }
   for(var jj = 0; jj < size(navaids); jj += 1){
    if((navaids[jj].type != "VOR") or (navaids[jj].id != wp.wp_name)){
     continue;
    }
    setprop("/fdm/jsbsim/fcs/NPK/Orbita/flightplan/RM/RM[" ~ (count + 1) ~ "]/phi-deg", navaids[jj].lat);
    setprop("/fdm/jsbsim/fcs/NPK/Orbita/flightplan/RM/RM[" ~ (count + 1) ~ "]/lam-deg", navaids[jj].lon);
    var freq = navaids[jj].frequency;
    if((freq >= 96000.0) and (freq <= 96435.0)){
     # If RSBN, extract channel.
     freq = int((freq - 95995.0) / 5.0);
    }else{
     freq /= 100.0;
    }
    setprop("/fdm/jsbsim/fcs/NPK/Orbita/flightplan/RM/RM[" ~ (count + 1) ~ "]/f", freq);
    count += 1;
    break;
   }
  }
 }
 setprop("/fdm/jsbsim/fcs/NPK/Orbita/flightplan/RM/count", count);
 Plot(getprop("/sim/gui/dialog/Orbita10/plottype"));
}

# @brief Import diversion route + diversion airport from FG route manager flight plan.
var ImportFGFPOPMOAP = func(file) {
 var fgfp = flightplan(file);
 var H = fgfp.cruiseAltitudeFt * FT2M * 0.001;
 # Route waypoints.
 var count = 0;
 for(var ii = 0; ii < fgfp.getPlanSize(); ii += 1){
  var wp = fgfp.getWP(ii);
  if(count > 9){
   print("OPM > 10 ignored starting with wp " ~ ii ~ " (" ~ wp.id ~ ").");
   break;
  }
  if((wp.wp_type == "basic") or (wp.wp_type == "navaid")){
   setprop("/fdm/jsbsim/fcs/NPK/Orbita/flightplan/OPM/OPM[" ~ (count + 1) ~ "]/phi-deg", wp.lat);
   setprop("/fdm/jsbsim/fcs/NPK/Orbita/flightplan/OPM/OPM[" ~ (count + 1) ~ "]/lam-deg", wp.lon);
   if(wp.alt_cstr_type != "at" or wp.alt_cstr == nil){
    setprop("/fdm/jsbsim/fcs/NPK/Orbita/flightplan/OPM/OPM[" ~ (count + 1) ~ "]/H-km", H);
   }else{
    setprop("/fdm/jsbsim/fcs/NPK/Orbita/flightplan/OPM/OPM[" ~ (count + 1) ~ "]/H-km", wp.alt_cstr * FT2M * 0.001);
   }
   count += 1;
  }
 }
 setprop("/fdm/jsbsim/fcs/NPK/Orbita/flightplan/OPM/count", count);
 # Destination airport.
 var ap = fgfp.destination;
 var fprwy = fgfp.destination_runway;
 if(ap != nil){
  setprop("/fdm/jsbsim/fcs/NPK/Orbita/flightplan/AP/AP[9]/phi-deg", ap.lat);
  setprop("/fdm/jsbsim/fcs/NPK/Orbita/flightplan/AP/AP[9]/lam-deg", ap.lon);
  # Find 2 longest runways.
  var psi = [0.0, 0.0];
  var score = [0.0, 0.0];
  foreach(var jj; keys(ap.runways)){
   var area = ap.runways[jj].length * math.min(ap.runways[jj].width, 45.0);
   if(area > score[0]){
    score[1] = score[0];
    psi[1] = psi[0];
    score[0] = area;
    psi[0] = ap.runways[jj].heading;
   }
  }
  # If runway defined in flightplan, use it.
  if(fprwy != nil){
   psi[1] = psi[0];
   psi[0] = fprwy.heading;
  }
  for(var jj = 0; jj < 2; jj += 1){
   setprop("/fdm/jsbsim/fcs/NPK/Orbita/flightplan/AP/AP[9]/psi-deg[" ~ jj ~ "]", psi[jj]);
  }
  setprop("/fdm/jsbsim/fcs/NPK/Orbita/flightplan/AP/oper-entered", 1);
 }
 Plot(getprop("/sim/gui/dialog/Orbita10/plottype"));
}

var _ImportFGFPPPMDialogCallback = func(filenode) {
 ImportFGFPPPM(filenode.getValue());
}

var ImportFGFPPPMDialog= func() {
 var dir = getprop("/sim/fg-home") ~ "/Export";
 var selector = gui.FileSelector.new(
  callback: _ImportFGFPPPMDialogCallback,
  dir: dir,
  dotfiles: 1,
  title: "Import PPM",
  button: "Import"
 );
 selector.open();
 selector.del();
}

var _ImportFGFPAPDialogCallback = func(filenode) {
 ImportFGFPAP(filenode.getValue());
}

var ImportFGFPAPDialog= func() {
 var dir = getprop("/sim/fg-home") ~ "/Export";
 var selector = gui.FileSelector.new(
  callback: _ImportFGFPAPDialogCallback,
  dir: dir,
  dotfiles: 1,
  title: "Import AP",
  button: "Import"
 );
 selector.open();
 selector.del();
}

var _ImportFGFPRMDialogCallback = func(filenode) {
 ImportFGFPRM(filenode.getValue());
}

var ImportFGFPRMDialog= func() {
 var dir = getprop("/sim/fg-home") ~ "/Export";
 var selector = gui.FileSelector.new(
  callback: _ImportFGFPRMDialogCallback,
  dir: dir,
  dotfiles: 1,
  title: "Import RM",
  button: "Import"
 );
 selector.open();
 selector.del();
}

var _ImportFGFPOPMOAPDialogCallback = func(filenode) {
 ImportFGFPOPMOAP(filenode.getValue());
}

var ImportFGFPOPMOAPDialog= func() {
 var dir = getprop("/sim/fg-home") ~ "/Export";
 var selector = gui.FileSelector.new(
  callback: _ImportFGFPOPMOAPDialogCallback,
  dir: dir,
  dotfiles: 1,
  title: "Import OPM + AP 9",
  button: "Import"
 );
 selector.open();
 selector.del();
}

setlistener("/sim/signals/fdm-initialized", func(p) {
 if(p.getValue() == 0){
  return;
 }
 # XXX Airports don't show up on the PPM plot without this delay.
 # XXX Means something else than this signal should be used.
 settimer(func{
  Orbita10.Plot("PPM");
 }, 10.0);
});
