# SIC Parking is supported, but not used, because it is already the
# SIC default, and in-sim overlay switch does not seem to work.
#var _supported = ["parked", "gate", "taxi", "take-off", "cruise", "approach"];
var _supported = ["gate", "taxi", "take-off", "subsonic", "cruise", "approach"];

setlistener("/sim/signals/fdm-initialized", func(p) {
 if(p.getValue() == 0){
  return;
 }

 var state = getprop("/sim/aircraft-state") or "parked";
 var match = 0;
 foreach(name; _supported){
  if(state == name){
   match = 1;
   break;
  }
 }
 if(!match){
  print("Unsupported state: " ~ state);
  return;
 }

 # XXX Needed for changeable load.
 #if(state == "take-off"){
 # Tu144D.TrimTargetTakeoff();
 # Tu144D.TrimDo();
 #}

 if((state == "subsonic") or (state == "cruise")){
  Tu144D.ABSULevel(1);
 }else if(state == "approach"){
  Tu144D.ABSUSpeed();
 }

 # Reset gear switch (see "Tu-144D-jsbsim/control.xml").
 if((state == "subsonic") or (state == "cruise")){
  Tu144D.GearCmd(0);
 }

 if(state == "subsonic"){
  Tu144D.ThrottleAll(42.0);
 }else if(state == "cruise"){
  Tu144D.ThrottleAll(80.0);
 }else if(state == "approach"){
  Tu144D.ThrottleAll(28.0);
 }

 # FIXME Vary fuel CG on cruise alt/speed!
 if(state == "taxi"){
  Tu144D.SetFuel(22000.0, 2000.0, 5000.0, 3000.0);
 }else if(state == "take-off"){
  Tu144D.SetFuel(20000.0, 2000.0, 5000.0, 3000.0);
 }else if(state == "approach"){
  Tu144D.SetFuel(10000.0, 0.0, 0.0, 0.0);
 }else if(state == "cruise"){
  Tu144D.SetFuel(20000.0, 2000.0, 0.0, 8000.0);
 }else if((state != "parked") and (state != "gate")){
  Tu144D.SetFuel(20000.0, 0.0, 5000.0, 5000.0);
 }

 if((state != "parked")){
  Tu144D.FuelMeterAutofill(0);
 }

 if((state == "parked") or (state == "gate")){
  Tu144D.FuelMeter(0);
 }else{
  Tu144D.FuelMeter(1);
 }

 if(state == "parked"){
  Tu144D.APUPower(0);
  Tu144D.APUBleed(0);
  Tu144D.ElecOff();
  Tu144D.FireValveAll(0);
  Tu144D.CutoffAll(1);
  Tu144D.BoostpumpAll(0);
 }else if(state == "gate"){
  Tu144D.APUPower(1);
  Tu144D.APUBleed(1);
  Tu144D.ElecAPU();
  Tu144D.FireValveAll(0);
  Tu144D.CutoffAll(1);
  Tu144D.BoostpumpAll(0);
 }else{
  Tu144D.APUPower(0);
  Tu144D.APUBleed(0);
  Tu144D.ElecDefault();
  Tu144D.FireValveAll(1);
  Tu144D.CutoffAll(0);
  Tu144D.BoostpumpAll(1);
 }


 if((state == "parked") or (state == "gate")){
  Tu144D.PrepareNPK();
 }else{
  Tu144D.PrepareInstruments();
 };

 if(state == "parked"){
  Tu144D.NPKMode(0);
  Tu144D.NPKIntegration(0);
 } else if (state == "gate") {
  Tu144D.NPKMode(2);
  Tu144D.NPKIntegration(0);
 }else{
  Tu144D.NPKMode(3);
  Tu144D.NPKIntegration(1);
 }

 if((state == "parked") or (state == "gate") or (state == "taxi")){
  Tu144D.Steering(2);
 }else{
  Tu144D.Steering(1);
 }

 if(state == "parked"){
  Tu144D.AirconOff();
 }else if(state == "gate"){
  Tu144D.AirconGate();
 }else{
  Tu144D.AirconDefault();
 }

 if(state == "parked"){
  Tu144D.LightsOff();
 }else if(state == "gate"){
  Tu144D.LightsGate(1);
  Tu144D.IntlightsGate();
 }else if(state == "taxi"){
  Tu144D.LightsTaxi(1);
  Tu144D.IntlightsDefault();
 }else if((state == "take-off") or (state == "approach")){
  Tu144D.LightsTOL(1);
  Tu144D.IntlightsTOL();
 }else{
  Tu144D.LightsDefault(1);
  Tu144D.IntlightsDefault();
 }

 if((state == "parked") or (state == "gate")){
  Tu144D.Chocks(1);
 }else{
  Tu144D.Chocks(0);
 }

 if((state == "parked") or (state == "gate") or (state == "taxi")){
  Tu144D.Parkingbrake(1);
 }else{
  Tu144D.Parkingbrake(0);
 }

 if((state == "take-off")){
  Tu144D.Startbrake(1);
 }else{
  Tu144D.Startbrake(0);
 }

 if((state != "parked") and (state != "gate")){
  Tu144D.WindowsImmed(0.0);
  Tu144D.Doors(0.0);
  Tu144D.Cargodoors(0.0);
 }

});
