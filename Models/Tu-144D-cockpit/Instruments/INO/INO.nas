####################################################### Properties #########################################################
#Relative path to folder, which contains INOs props:
var rel_path = "instrumentation/INO/";
# On/Off
var serviceable = props.globals.initNode(rel_path~"serviceable", 0, "INT");                             # 0 is Off, 1 is On
var boost_fps = props.globals.initNode(rel_path~"boost_fps", 0, "BOOL");
# Modes
var global_mode = props.globals.initNode(rel_path~"global_mode", 3, "INT");                             # 1 -- НУ, 2 -- КОНТР, 3 -- МС, 4 -- РУ, 5 -- СП
# Map options
var vor_dme = props.globals.initNode(rel_path~"info/vor-dme", 1, "BOOL");
var vor_dme_label = props.globals.initNode(rel_path~"info/vor-dme-label", 1, "BOOL");
var ndb = props.globals.initNode(rel_path~"info/ndb", 1, "BOOL");
var ndb_label = props.globals.initNode(rel_path~"info/ndb-label", 1, "BOOL");
var fix = props.globals.initNode(rel_path~"info/fix", 1, "BOOL");
#var fix_label = props.globals.initNode(rel_path~"info/fix-label", 0, "BOOL");
var apt = props.globals.initNode(rel_path~"info/apt", 0, "BOOL");                                       # 0 -- show runways, 1 -- shot airport symbols, automated
# Brightness
var scr_brightness = props.globals.initNode(rel_path~"screen/brightness", 1, "DOUBLE");                 # Input for screen brightness, nnot incl. sercviceable
var lamp_brt = props.globals.initNode(rel_path~"screen/lamp-brt", 0, "DOUBLE");                         # Output for screen lamp, incl. serviceable
var bckl_brightness = props.globals.initNode(rel_path~"backlight/brightness", 1, "DOUBLE");             # Input for backlight brightness, nnot incl. sercviceable
#var bckl_brt = props.globals.initNode(rel_path~"backlight/bckl-brt", 0, "DOUBLE");                      # Output for backlight, incl. serviceable
# Map range
var range = props.globals.initNode(rel_path~"range/range", 320, "INT");                                  # Main ranges is 40km and 160/320km.
var range_threshold = props.globals.initNode(rel_path~"range/threshold", 100, "INT");                   # Threshold for "Aerodrome-Route" autoswitch
var in_aer_range = props.globals.initNode(rel_path~"range/aerodrome", 0, "BOOL");
var aer_range = props.globals.initNode(rel_path~"range/aer_range", 80, "DOUBLE");
# Map centering mode
var ctr_mode = props.globals.initNode(rel_path~"map-centering/ctr-mode", 0, "INT");                     # 0 -- centered, 1 -- lowered
var ctr_move = props.globals.initNode(rel_path~"map-centering/ctr-move", 0, "BOOL");                    # 1 until (de)centering process is finished
var lowering_mode = props.globals.initNode(rel_path~"map-centering/lowering-mode", 1, "INT");           # 0 -- 1/2 radius down, 1 -- 7/8 radius down
# Map orientation mode
var up_azimuth = props.globals.initNode(rel_path~"orientation/up_azimuth", 0, "DOUBLE");
var pu_angle_out = props.globals.initNode(rel_path~"orientation/pu_angle_out", 0, "DOUBLE");
var snos_angle_out = props.globals.initNode(rel_path~"orientation/snos_angle_out", 0, "DOUBLE");
var IPO_angle_out = props.globals.initNode(rel_path~"orientation/IPO_angle_out", 0, "DOUBLE");
var IPO_mode = props.globals.initNode(rel_path~"orientation/IPO_mode", 0, "INT");                       # 0 -- automatic, 1 -- manual input
var IPO_source = props.globals.initNode(rel_path~"orientation/IPO_source", 0, "INT");                   # 0: NV; 1: deltaA1; 2: deltaA2; 3: ZPU1; 4: ZPU2
var hdg_mode = props.globals.initNode(rel_path~"orientation/hdg_mode", 0, "INT");                       # 0 -- north up, 1 -- manual input, 2 -- hdg from Orbita
# Aircraft position
var lat_out = props.globals.initNode(rel_path~"position/lat-out", 0, "DOUBLE");
var lon_out = props.globals.initNode(rel_path~"position/lon-out", 0, "DOUBLE");
#var pos_mode = props.globals.initNode(rel_path~"position/pos-mode", 0, "INT");                          # 0 -- from Orbita, 1 -- manual input, 2 -- true position
# Inputs for true heading and coordinates:
var true_hdg = props.globals.initNode(rel_path~"orientation/true_hdg", 0, "DOUBLE");
var pu_angle_in = props.globals.initNode(rel_path~"orientation/pu_angle_in", 0, "DOUBLE");
var snos_angle_in = props.globals.initNode(rel_path~"orientation/snos_angle_in", 0, "DOUBLE");
var IPO_angle_in = props.globals.initNode(rel_path~"orientation/IPO_angle_in", 0, "DOUBLE");
var lat_in = props.globals.initNode(rel_path~"position/lat-in", 0, "DOUBLE");
var lon_in = props.globals.initNode(rel_path~"position/lon-in", 0, "DOUBLE");
# Frame indicator
var frame = props.globals.initNode(rel_path~"frame_ind/frame", 0, "INT");
var w_1 = props.globals.initNode(rel_path~"frame_ind/w_1", 0, "DOUBLE");
var w_2 = props.globals.initNode(rel_path~"frame_ind/w_2", 0, "DOUBLE");
var w_3 = props.globals.initNode(rel_path~"frame_ind/w_3", 0, "DOUBLE");
var aerodrome_frame = props.globals.initNode(rel_path~"frame_ind/aerodrome", 404, "INT");
var departure = props.globals.getNode("/fdm/jsbsim/fcs/NPK/Orbita/route/departure/AP");
var arrival = props.globals.getNode("/fdm/jsbsim/fcs/NPK/Orbita/route/airport/AP");
var route_len = props.globals.initNode(rel_path~"frame_ind/route_len", 0, "DOUBLE");
var route_last = props.globals.initNode(rel_path~"frame_ind/route_last", 0, "DOUBLE");
# Buttons
var b_hdg = props.globals.initNode(rel_path~"buttons/b_hdg", 0, "DOUBLE");
var b_rng = props.globals.initNode(rel_path~"buttons/b_rng", 0, "DOUBLE");
var b_rng_force = props.globals.initNode(rel_path~"buttons/b_rng_force", 0, "BOOL");
var b_IPO = props.globals.initNode(rel_path~"buttons/b_IPO", 0, "DOUBLE");
var b_hdg_h = props.globals.initNode(rel_path~"buttons/b_hdg_h", 0, "BOOL");
var b_rng_h = props.globals.initNode(rel_path~"buttons/b_rng_h", 0, "BOOL");
var b_IPO_h = props.globals.initNode(rel_path~"buttons/b_IPO_h", 0, "BOOL");
var b_vertical = props.globals.initNode(rel_path~"buttons/b_vertical", 0, "DOUBLE");
var b_horizontal = props.globals.initNode(rel_path~"buttons/b_horizontal", 0, "DOUBLE");
var b_rotation = props.globals.initNode(rel_path~"buttons/b_rotation", 0, "DOUBLE");
var b_enter = props.globals.initNode(rel_path~"buttons/b_enter", 0, "DOUBLE");
var cup_enter = props.globals.initNode(rel_path~"buttons/cup_enter", 0, "DOUBLE");
# Lights
var b_hdg_l1 = props.globals.initNode(rel_path~"lights/b_hdg_l1", 0, "DOUBLE");
var b_hdg_l2 = props.globals.initNode(rel_path~"lights/b_hdg_l2", 0, "DOUBLE");
var b_rng_l1 = props.globals.initNode(rel_path~"lights/b_rng_l1", 0, "DOUBLE");
var b_rng_l2 = props.globals.initNode(rel_path~"lights/b_rng_l2", 0, "DOUBLE");
var b_IPO_l1 = props.globals.initNode(rel_path~"lights/b_IPO_l1", 0, "DOUBLE");
var b_IPO_l2 = props.globals.initNode(rel_path~"lights/b_IPO_l2", 0, "DOUBLE");
var l_NV_fault = props.globals.initNode(rel_path~"lights/l_NV_fault", 0, "DOUBLE");
var l_aerodrome = props.globals.initNode(rel_path~"lights/l_aerodrome", 0, "DOUBLE");
var l_auto = props.globals.initNode(rel_path~"lights/l_auto", 0, "DOUBLE");
var l_marker = props.globals.initNode(rel_path~"lights/l_marker", 0, "DOUBLE");
var l_inst_r_out = props.globals.initNode(rel_path~"lights/l_inst_r", 0, "DOUBLE");
var l_inst_g_out = props.globals.initNode(rel_path~"lights/l_inst_g", 0, "DOUBLE");
var l_inst_b_out = props.globals.initNode(rel_path~"lights/l_inst_b", 0, "DOUBLE");
var l_inst_brt = props.globals.initNode(rel_path~"lights/l_inst_brt", 0, "DOUBLE");
var l_panel_r_in = props.globals.getNode("instrumentation/jsbsim/systems/lighting/lightsrc/cockpit-panels_r");
var l_panel_g_in = props.globals.getNode("instrumentation/jsbsim/systems/lighting/lightsrc/cockpit-panels_g");
var l_panel_b_in = props.globals.getNode("instrumentation/jsbsim/systems/lighting/lightsrc/cockpit-panels_b");
# NPK
var NV_fault = props.globals.getNode("fdm/jsbsim/fcs/NPK/panel/light/TsVM-fault");
var NPK_prepare = props.globals.getNode("fdm/jsbsim/fcs/NPK/status/prepare");
var NPK_run = props.globals.getNode("fdm/jsbsim/fcs/NPK/status/run");








######################################### MapStructure ("Projection" Layer) Init ###########################################
var INO_start = func() {
  var r = func(name,vis=1,zindex=nil) return caller(0)[0];
  var temp = {};
  temp.ino_l3 = canvas.new({
        "name": "l1_INO",
        "size": [1024, 1024],
        "view": [1024, 1024],
        "mipmapping": 1
  });
  temp.ino_l3.addPlacement({"node": "l1_INO"});
  temp.ino_l3.setColorBackground(1,1,1,1);  




  ############################################ Main maps #################################################
  #### FIX projection "Map"   ##### Not used.
  #temp.mapf = temp.ino_l3.createGroup();
  #var MapF = temp.mapf.createChild("map");
  #MapF.setController("T144 Aircraft position");
  #MapF.setRange(10.465 * range.getValue() / 10);
  #MapF.setScale(5.0);
  #MapF.setTranslation(
  #                         temp.ino_l3.get("view[0]")/2,
  #                         temp.ino_l3.get("view[1]")/2
  #                      );
  ## Defining what will be shown on the map
  #foreach(var type; [r('T144FIX')] ) { 
  #  MapF.addLayer(factory: canvas.SymbolLayer, type_arg: type.name, visible: type.vis, priority: type.zindex,);
  #}


  #### FIX Labels Labels Map projection "Map"   ##### Not used.
  #temp.mapfl = temp.ino_l3.createGroup();
  #var MapFL = temp.mapfl.createChild("map");
  #MapFL.setController("T144 Aircraft position");
  #MapFL.setRange(10.465 * range.getValue() / 10);
  #MapFL.setScale(5.0);
  #MapFL.setTranslation(
  #                         temp.ino_l3.get("view[0]")/2,
  #                         temp.ino_l3.get("view[1]")/2
  #                      );
  ## Defining what will be shown on the map
  #foreach(var type; [r('T144FIXL')] ) { 
  #  MapFL.addLayer(factory: canvas.SymbolLayer, type_arg: type.name, visible: type.vis, priority: type.zindex,);
  #}


  ### NDB projection "Map"
  temp.mapn = temp.ino_l3.createGroup();
  var MapN = temp.mapn.createChild("map");
  MapN.setController("T144 Aircraft position");
  MapN.setRange(10.465 * range.getValue() / 10);
  MapN.setScale(5.0);
  MapN.setTranslation(
                           temp.ino_l3.get("view[0]")/2,
                           temp.ino_l3.get("view[1]")/2
                        );
  # Defining what will be shown on the map
  foreach(var type; [r('T144NDB')] ) { 
    MapN.addLayer(factory: canvas.SymbolLayer, type_arg: type.name, visible: type.vis, priority: type.zindex,);
  }


  ### NDB Labels Labels Map projection "Map"
  temp.mapnl = temp.ino_l3.createGroup();
  var MapNL = temp.mapnl.createChild("map");
  MapNL.setController("T144 Aircraft position");
  MapNL.setRange(10.465 * range.getValue() / 10);
  MapNL.setScale(5.0);
  MapNL.setTranslation(
                           temp.ino_l3.get("view[0]")/2,
                           temp.ino_l3.get("view[1]")/2
                        );
  # Defining what will be shown on the map
  foreach(var type; [r('T144NDBL')] ) { 
    MapNL.addLayer(factory: canvas.SymbolLayer, type_arg: type.name, visible: type.vis, priority: type.zindex,);
  }


  ### VOR-DME projection "Map"
  temp.mapv = temp.ino_l3.createGroup();
  var MapV = temp.mapv.createChild("map");
  MapV.setController("T144 Aircraft position");
  MapV.setRange(10.465 * range.getValue() / 10);
  MapV.setScale(5.0);
  MapV.setTranslation(
                           temp.ino_l3.get("view[0]")/2,
                           temp.ino_l3.get("view[1]")/2
                        );
  # Defining what will be shown on the map
  foreach(var type; [r('T144VOR'), r('T144DME')] ) { 
    MapV.addLayer(factory: canvas.SymbolLayer, type_arg: type.name, visible: type.vis, priority: type.zindex,);
  }


  ### VOR-DME Labels Map projection "Map"
  temp.mapvl = temp.ino_l3.createGroup();
  var MapVL = temp.mapvl.createChild("map");
  MapVL.setController("T144 Aircraft position");
  MapVL.setRange(10.465 * range.getValue() / 10);
  MapVL.setScale(5.0);
  MapVL.setTranslation(
                           temp.ino_l3.get("view[0]")/2,
                           temp.ino_l3.get("view[1]")/2
                        );
  # Defining what will be shown on the map
  foreach(var type; [r('T144VORL')] ) { 
    MapVL.addLayer(factory: canvas.SymbolLayer, type_arg: type.name, visible: type.vis, priority: type.zindex,);
  }


  ### Map #0 projection "Map"
  temp.map0 = temp.ino_l3.createGroup();
  var Map0 = temp.map0.createChild("map");
  Map0.setController("T144 Aircraft position");
  Map0.setRange(2.093 * range.getValue() / 10);
  Map0.setScale(1.0);
  Map0.setTranslation(
                           temp.ino_l3.get("view[0]")/2,
                           temp.ino_l3.get("view[1]")/2
                        );
  # Defining what will be shown on the map #1
  foreach(var type; [r('T144RWY')] ) { #
    Map0.addLayer(factory: canvas.SymbolLayer, type_arg: type.name, visible: type.vis, priority: type.zindex,);
  }


  ### Map #1 projection "Map"
  temp.map1 = temp.ino_l3.createGroup();
  var Map1 = temp.map1.createChild("map");
  Map1.setController("T144 Aircraft position");
  Map1.setRange(2.093 * range.getValue() / 10);
  Map1.setScale(1.0);
  Map1.setTranslation(
                           temp.ino_l3.get("view[0]")/2,
                           temp.ino_l3.get("view[1]")/2
                        );
  # Defining what will be shown on the map #1
  foreach(var type; [r('T144APT')] ) { #
    Map1.addLayer(factory: canvas.SymbolLayer, type_arg: type.name, visible: type.vis, priority: type.zindex,);
  }


  ### Map #2 projection "Map"
  temp.map2 = temp.ino_l3.createGroup();
  var Map2 = temp.map2.createChild("map");
  Map2.setController("T144 Aircraft position");
  Map2.setRange(2.093 * range.getValue() / 10);
  Map2.setScale(1.0);
  Map2.setTranslation(
                           temp.ino_l3.get("view[0]")/2,
                           temp.ino_l3.get("view[1]")/2
                        );
  # Defining what will be shown on the map #1
  foreach(var type; [r('T144APTL'), r('T144RTE'), r('T144WPT')] ) { #
    Map2.addLayer(factory: canvas.SymbolLayer, type_arg: type.name, visible: type.vis, priority: type.zindex,);
  }




################################################## Functions ####################################################
  # Centering mode
  var xTrans = temp.ino_l3.get("view[0]")/2;
  var yTrans = temp.ino_l3.get("view[1]")/2;
  var Trans = 512;
  var goal = 0;
  var goaly = 0;
  var goalx = 0;
  var ctr_chng = func() {
    if (serviceable.getValue() == 1) {
      goal = 512 + ctr_mode.getValue() * (256 + lowering_mode.getValue() * 192);
      goaly = 512 + ctr_mode.getValue() * (256 + lowering_mode.getValue() * 192) * math.cos(pu_angle_in.getValue() * D2R);
      goalx = 512 - ctr_mode.getValue() * (256 + lowering_mode.getValue() * 192) * math.sin(pu_angle_in.getValue() * D2R);
      if (yTrans != goaly or xTrans != goalx) {
        if (Trans != goal) {
          ctr_move.setValue(1);
          Trans = Trans + 8 * (goal - Trans) / math.abs(goal - Trans);
        } else { ctr_move.setValue(0); }
        if (math.abs(yTrans - goaly) <= 8 * math.abs(math.cos(pu_angle_in.getValue() * D2R))) { yTrans = goaly; }
        else { yTrans = yTrans + 8 * math.abs(math.cos(pu_angle_in.getValue() * D2R)) * (goaly - yTrans) / math.abs(goaly - yTrans); }
        if (math.abs(xTrans - goalx) <= 8 * math.abs(math.sin(pu_angle_in.getValue() * D2R))) { xTrans = goalx; }
        else { xTrans = xTrans + 8 * math.abs(math.sin(pu_angle_in.getValue() * D2R)) * (goalx - xTrans) / math.abs(goalx - xTrans); }
        if(range.getValue() <= range_threshold.getValue()) {
          Map0.setTranslation(
              xTrans,
              yTrans,
            );
          Map1.setTranslation(
              xTrans,
              yTrans,
            );
        } else {
          Map0.setTranslation(
              xTrans + 20000,
              yTrans,
            );
          Map1.setTranslation(
              xTrans + 20000,
              yTrans,
            );
        }
        Map2.setTranslation(
            xTrans,
            yTrans,
          );
        if(vor_dme.getValue()) {
          MapV.setTranslation(
              xTrans,
              yTrans,
            );
        } else {
          MapV.setTranslation(
              xTrans + 20000,
              yTrans,
            );
        }
        if(vor_dme.getValue() and vor_dme_label.getValue()) {
          MapVL.setTranslation(
              xTrans,
              yTrans,
            );
        } else {
          MapVL.setTranslation(
              xTrans + 20000,
              yTrans,
            );
        }
        if(ndb.getValue()) {
          MapN.setTranslation(
              xTrans,
              yTrans,
            );
        } else {
          MapN.setTranslation(
              xTrans + 20000,
              yTrans,
            );
        }
        if(ndb.getValue() and ndb_label.getValue()) {
          MapNL.setTranslation(
              xTrans,
              yTrans,
            );
        } else {
          MapNL.setTranslation(
              xTrans + 20000,
              yTrans,
            );
        }
        #if(fix.getValue()) {
        #  MapF.setTranslation(
        #      xTrans,
        #      yTrans,
        #    );
        #} else {
        #  MapF.setTranslation(
        #      xTrans + 20000,
        #      yTrans,
        #    );
        #}
        #if(fix.getValue() and fix_label.getValue()) {
        #  MapFL.setTranslation(
        #      xTrans,
        #      yTrans,
        #    );
        #} else {
        #  MapFL.setTranslation(
        #      xTrans + 20000,
        #      yTrans,
        #    );
        #}
      } else {
        ctr_move.setValue(0);
      }
    }
  }
  var timer_ctr_chng = maketimer(0.02, ctr_chng);
  timer_ctr_chng.simulatedTime = 1;
  timer_ctr_chng.start();


  # Map range changer
  var rng_chng = func() {
    Map2.setRange(2.093 * range.getValue() / 10);
    if (!boost_fps.getValue()) {
      MapV.setRange(10.465 * range.getValue() / 10);
      MapVL.setRange(10.465 * range.getValue() / 10);
      if(range.getValue() > range_threshold.getValue()) {
        ndb.setValue(0);
        fix.setValue(0);
        apt.setValue(1);
        Map0.setRange(1);
        Map1.setRange(2.093 * range.getValue() / 10);
        MapN.setRange(1);
        MapNL.setRange(1);
        #MapF.setRange(1);
        #MapFL.setRange(10.465 / 10);   ###### Not used.
      } else {
        ndb.setValue(1);
        fix.setValue(1);
        apt.setValue(0);
        Map0.setRange(2.093 * range.getValue() / 10);
        Map1.setRange(1);
        MapN.setRange(10.465 * range.getValue() / 10);
        MapNL.setRange(10.465 * range.getValue() / 10);
        #MapF.setRange(10.465 * range.getValue() / 10);
        #MapFL.setRange(10.465 * range.getValue() / 10);    ######## Not used.
      }
    } else {
      MapV.setRange(1);
      MapVL.setRange(1);
      Map0.setRange(1);
      Map1.setRange(1);
      MapN.setRange(1);
      MapNL.setRange(1);
      #MapF.setRange(1);
      #MapFL.setRange(1);    ######## Not used.
    }
  }
  setlistener(range.getPath(), rng_chng);
  setlistener(boost_fps.getPath(), rng_chng);
  rng_chng();


  # Showing Information Modes
  var info_chng = func() {
    if (!vor_dme.getValue()) {
      MapV.setTranslation(xTrans + 20000, yTrans,);
      MapVL.setTranslation(xTrans + 20000, yTrans,);
    } else if (!vor_dme_label.getValue()) {
      MapV.setTranslation(xTrans, yTrans,);
      MapVL.setTranslation(xTrans + 20000, yTrans,);
    } else {
      MapV.setTranslation(xTrans, yTrans,);
      MapVL.setTranslation(xTrans, yTrans,);
    }
    if (!ndb.getValue()) {
      MapN.setTranslation(xTrans + 20000, yTrans,);
      MapNL.setTranslation(xTrans + 20000, yTrans,);
    } else if (!ndb_label.getValue()) {
      MapN.setTranslation(xTrans, yTrans,);
      MapNL.setTranslation(xTrans + 20000, yTrans,);
    } else {
      MapN.setTranslation(xTrans, yTrans,);
      MapNL.setTranslation(xTrans, yTrans,);
    }
    if (!fix.getValue()) {
      #MapF.setTranslation(xTrans + 20000, yTrans,);
    #  MapFL.setTranslation(xTrans + 20000, yTrans,);   ##### Not used.
    #} else if (!fix_label.getValue()) {                #
    #  MapF.setTranslation(xTrans, yTrans,);            #
    #  MapFL.setTranslation(xTrans + 20000, yTrans,);   #
    } else {
      #MapF.setTranslation(xTrans, yTrans,);
    #  MapFL.setTranslation(xTrans, yTrans,);   ##### Not used.
    }
    if (apt.getValue()) {
      Map0.setTranslation(xTrans + 20000, yTrans,);
      Map1.setTranslation(xTrans + 20000, yTrans,);
    } else {
      Map0.setTranslation(xTrans, yTrans,);
      Map1.setTranslation(xTrans, yTrans,);
    }
  }
  setlistener(vor_dme.getPath(), info_chng);
  setlistener(vor_dme_label.getPath(), info_chng);
  setlistener(ndb.getPath(), info_chng);
  setlistener(ndb_label.getPath(), info_chng);
  setlistener(fix.getPath(), info_chng);
  #setlistener(fix_label.getPath(), info_chng);   ##### Not used.
  setlistener(apt.getPath(), info_chng);
  info_chng();
};








################################################### Lamp Brightness #########################################################
var Main_Lamp = func() {
  lamp_brt.setValue(scr_brightness.getValue() * serviceable.getValue());
  #bckl_brt.setValue(bckl_brightness.getValue() * serviceable.getValue());
}
setlistener(scr_brightness.getPath(), Main_Lamp);
#setlistener(bckl_brightness.getPath(), Main_Lamp);
setlistener(serviceable.getPath(), Main_Lamp);


################################################### Position Filter #########################################################
var diff_pos = 0;
var Pos_filter = func() {
  if (serviceable.getValue() == 1) {
    if (global_mode.getValue() == 1) {
      lat_in.setValue(getprop("/position/latitude-deg"));
      lon_in.setValue(getprop("/position/longitude-deg"));
    } else if (global_mode.getValue() == 4) {

    } else if (global_mode.getValue() == 2 or global_mode.getValue() == 3) {
      lat_in.setValue(getprop("/fdm/jsbsim/fcs/NPK/Orbita/INS/phi/phi-deg"));
      lon_in.setValue(getprop("/fdm/jsbsim/fcs/NPK/Orbita/INS/lam/lam-deg"));
    } else if (global_mode.getValue() == 5) {
      lat_in.setValue(0.0);
      lon_in.setValue(0.0);
    }

    if (math.abs(lat_in.getValue() - lat_out.getValue()) < 0.001) {
      lat_out.setValue(lat_in.getValue());
    } else {
      diff_pos = lat_in.getValue() - lat_out.getValue();
      lat_out.setValue(lat_out.getValue() + diff_pos / 20 + 0.0005 * diff_pos / math.abs(diff_pos));
    }
    if (math.abs(lon_in.getValue() - lon_out.getValue()) < 0.001) {
      lon_out.setValue(lon_in.getValue());
    } else {
      diff_pos = lon_in.getValue() - lon_out.getValue();
      if (math.abs(diff_pos) > 180) { diff_pos = (math.abs(diff_pos) - 360) * diff_pos / math.abs(diff_pos); }
      lon_out.setValue(math.fmod(lon_out.getValue() + diff_pos / 20 + 0.0005 * diff_pos / math.abs(diff_pos) + 540, 360) - 180);
    }
  }
}
var timer_Pos_filter = maketimer(0.01, Pos_filter);
timer_Pos_filter.simulatedTime = 1;


################################################## Orientation Filter ########################################################
var diff_ori = 0;
var pu_angle_raw = 0;
var snos_angle_raw = 0;
var IPO_angle_in_raw = 0;
var IPO_angle_raw = 0;
var Orient_filter = func() {
  if (serviceable.getValue() == 1) {
    if (global_mode.getValue() == 4) {
    } else if (hdg_mode.getValue() == 0 or global_mode.getValue() == 2) {
      true_hdg.setValue(0);
    } else if (hdg_mode.getValue() == 2 and global_mode.getValue() == 1) {
      true_hdg.setValue(getprop("/orientation/heading-deg"));
    } else if (hdg_mode.getValue() == 2) {
      true_hdg.setValue(getprop("/fdm/jsbsim/fcs/NPK/Orbita/INS/psi/psi-deg"));
    }
    diff_ori = true_hdg.getValue() - up_azimuth.getValue();
    if (true_hdg.getValue() > up_azimuth.getValue()) {
      if (diff_ori >= 180) diff_ori -= 360;
    } else {
      if (diff_ori <= -180) diff_ori += 360;
    }
    if (math.abs(diff_ori) < 0.001) {
      up_azimuth.setValue(true_hdg.getValue());
    } else {
      up_azimuth.setValue(math.fmod(up_azimuth.getValue() + diff_ori / 20 + 0.0005 * diff_ori / math.abs(diff_ori), 360));
    }

    if (global_mode.getValue() == 1) {
      pu_angle_in.setValue(getprop("/orientation/heading-deg"));
    } else if (global_mode.getValue() == 2) {
      pu_angle_in.setValue(math.fmod(up_azimuth.getValue() + 270, 360));
    } else if (global_mode.getValue() == 3 or global_mode.getValue() == 4) {
      pu_angle_in.setValue(getprop("/fdm/jsbsim/fcs/NPK/Orbita/INS/psi/psi-deg"));
    } else if (global_mode.getValue() == 5) {
      pu_angle_in.setValue(up_azimuth.getValue()); 
    }
    pu_angle_raw = pu_angle_out.getValue() + up_azimuth.getValue();
    diff_ori = pu_angle_in.getValue() - pu_angle_raw;
    if (pu_angle_in.getValue() > pu_angle_raw) {
      if (diff_ori >= 180) diff_ori -= 360;
    } else {
      if (diff_ori <= -180) diff_ori += 360;
    }
    if (math.abs(diff_ori) < 0.001) {
      pu_angle_raw = pu_angle_in.getValue();
    } else {
      pu_angle_raw = math.fmod(pu_angle_raw + diff_ori / 20 + 0.0005 * diff_ori / math.abs(diff_ori), 360);
    }
    pu_angle_out.setValue(pu_angle_raw - up_azimuth.getValue());

    if (global_mode.getValue() == 1) {
      snos_angle_in.setValue(getprop("/orientation/heading-deg") + getprop("/fdm/jsbsim/fcs/NPK/DISS/beta-deg"));
    } else if (global_mode.getValue() == 2) {
      snos_angle_in.setValue(math.fmod(up_azimuth.getValue() + 180, 360));
    } else if (global_mode.getValue() == 3 or global_mode.getValue() == 4 or global_mode.getValue() == 5) {
      snos_angle_in.setValue(getprop("/fdm/jsbsim/fcs/NPK/Orbita/INS/psi/psi-deg") + getprop("/fdm/jsbsim/fcs/NPK/DISS/beta-deg"));
    }
    snos_angle_raw = snos_angle_out.getValue() + up_azimuth.getValue();
    diff_ori = snos_angle_in.getValue() - snos_angle_raw;
    if (snos_angle_in.getValue() > snos_angle_out.getValue()) {
      if (diff_ori >= 180) diff_ori -= 360;
    } else {
      if (diff_ori <= -180) diff_ori += 360;
    }
    if (math.abs(diff_ori) < 0.001) {
      snos_angle_raw = snos_angle_in.getValue();
    } else {
      snos_angle_raw = math.fmod(snos_angle_raw + diff_ori / 20 + 0.0005 * diff_ori / math.abs(diff_ori), 360);
    }
    snos_angle_out.setValue(snos_angle_raw - up_azimuth.getValue());

    IPO_angle_in_raw = 0;
    if (global_mode.getValue() == 1 or global_mode.getValue() == 3 or global_mode.getValue() == 4) {
      if (IPO_mode.getValue() == 0) {
        if (IPO_source.getValue() != 0) {
          IPO_angle_in.setValue(getprop("/fdm/jsbsim/fcs/NPK/instr/PNP/gauge/ZPU-deg"));
        } else {
          IPO_angle_in.setValue(getprop("/fdm/jsbsim/fcs/NPK/Orbita/func/leg/direct/psi_start-deg"));
        }
      } else if (IPO_mode.getValue() == 1) {
      }
      IPO_angle_in_raw = IPO_angle_in.getValue();
    } else if (global_mode.getValue() == 2) {
      IPO_angle_in_raw = math.fmod(up_azimuth.getValue() + 90, 360);
    } else if (global_mode.getValue() == 5) {
      IPO_angle_in_raw = up_azimuth.getValue();
    }
    IPO_angle_raw = IPO_angle_out.getValue() + up_azimuth.getValue();
    diff_ori = IPO_angle_in_raw - IPO_angle_raw;
    if (IPO_angle_in_raw > IPO_angle_raw) {
      if (diff_ori >= 180) diff_ori -= 360;
    } else {
      if (diff_ori <= -180) diff_ori += 360;
    }
    if (math.abs(diff_ori) < 0.001) {
      IPO_angle_raw = IPO_angle_in_raw;
    } else {
      IPO_angle_raw = math.fmod(IPO_angle_raw + diff_ori / 20 + 0.0005 * diff_ori / math.abs(diff_ori), 360);
    }
    IPO_angle_out.setValue(IPO_angle_raw - up_azimuth.getValue());
  }
}
var timer_Orient_filter = maketimer(0.01, Orient_filter);
timer_Orient_filter.simulatedTime = 1;


####################################### Temporary reaction on switches and buttons ##########################################
var IPO_Source = func() {
  IPO_source.setValue(getprop("/fdm/jsbsim/fcs/NPK/panel/switch/PNP-source[0]"));
}
setlistener("/fdm/jsbsim/fcs/NPK/panel/switch/PNP-source[0]", IPO_Source);


################################################ Frame indicator updater ####################################################
var Frame_ind_upd = func() {
  interpolate(w_1.getPath(), math.floor(frame.getValue() / 100), 0.5);
  interpolate(w_2.getPath(), math.fmod(math.floor(frame.getValue() / 10), 10), 0.5);
  interpolate(w_3.getPath(), math.fmod(frame.getValue(), 10), 0.5);
}
setlistener(frame.getPath(), Frame_ind_upd);


##################################################### Frame number ##########################################################
var to_dep = 0;
var to_arr = 0;
var Aerodrome_checker = func() {
  if (NPK_prepare.getValue() == 1 or NPK_run.getValue() == 1) {
      c_s = geo.Coord.new();
      #print(lat_out.getValue(), " ", lon_out.getValue());
      if (lat_out.getValue() != nil and lon_out.getValue() != nil) {
        c_s.set_latlon(lat_out.getValue(), lon_out.getValue());
      } else { c_s.set_latlon(0, 0); }
      c_d = geo.Coord.new();
      if (departure.getValue() != 0) {
        c_d.set_latlon(getprop("fdm/jsbsim/fcs/NPK/Orbita/flightplan/AP/AP["~departure.getValue()~"]/phi-deg"), getprop("fdm/jsbsim/fcs/NPK/Orbita/flightplan/AP/AP["~departure.getValue()~"]/lam-deg"));
      } else { c_d.set_latlon(45, 45); }
      c_a = geo.Coord.new();
      if (arrival.getValue() != 0) {
        c_a.set_latlon(getprop("fdm/jsbsim/fcs/NPK/Orbita/flightplan/AP/AP["~arrival.getValue()~"]/phi-deg"), getprop("fdm/jsbsim/fcs/NPK/Orbita/flightplan/AP/AP["~arrival.getValue()~"]/lam-deg"));
      } else { c_a.set_latlon(45, 45); }
      to_dep = c_s.distance_to(c_d);
      to_arr = c_s.distance_to(c_a);
      #print(to_dep, " ", to_arr);
      if (to_arr < aer_range.getValue() * 1000) {
        aerodrome_frame.setValue(arrival.getValue() + 100);
        if (in_aer_range.getValue() == 0) { range.setValue(40); }
        in_aer_range.setValue(1);
      } else if (to_dep < aer_range.getValue() * 1000) {
        aerodrome_frame.setValue(departure.getValue() + 100);
        if (in_aer_range.getValue() == 0) { range.setValue(40); }
        in_aer_range.setValue(1);
      } else {
        if (in_aer_range.getValue() == 1) { range.setValue(320); }
        in_aer_range.setValue(0);
        aerodrome_frame.setValue(404);
      }
    } else {
      in_aer_range.setValue(0);
      aerodrome_frame.setValue(404);
    }
}
var timer_Aerodrome_checker = maketimer(1, Aerodrome_checker);
timer_Aerodrome_checker.simulatedTime = 1;
var _NPK_run = 0;
var Route_len_saver = func() {
  if (NPK_run.getValue() == 1 and _NPK_run == 0) { route_len.setValue(getprop("fdm/jsbsim/fcs/NPK/Orbita/func/dist/dist/distance-km")); }
  _NPK_run = NPK_run.getValue();
}
var timer_Route_len_saver = maketimer(1, Route_len_saver);
timer_Route_len_saver.simulatedTime = 1;
var Frame_route = func() {
  if (getprop("fdm/jsbsim/fcs/NPK/panel/TsUNP/status/report-route") == 0 and getprop("fdm/jsbsim/fcs/NPK/panel/TsUNP/status/report-direct") == 0) {
    route_last.setValue(getprop("fdm/jsbsim/fcs/NPK/Orbita/func/dist/dist/distance-km"));
  }
  return math.max(0, math.min(100, (route_len.getValue() - route_last.getValue()) / route_len.getValue() * 100));
}
var Frame_num = func() {
  if (serviceable.getValue() == 1) {
    if (global_mode.getValue() == 2) {
      frame.setValue(999);
    } else if (global_mode.getValue() == 1 or global_mode.getValue() == 3 or global_mode.getValue() == 4) {
      if (in_aer_range.getValue() == 1 and range.getValue() < range_threshold.getValue()) {
        frame.setValue(aerodrome_frame.getValue());
      } else if (in_aer_range.getValue() == 0 or range.getValue() >= range_threshold.getValue()) {
        frame.setValue(Frame_route());
      }
    } else if (global_mode.getValue() == 5) {
      frame.setValue(111);
    }
  }
}
var timer_Frame_num = maketimer(0.5, Frame_num);
timer_Frame_num.simulatedTime = 1;


####################################################### Buttons #############################################################
var Heading_Button = func() {
  if (serviceable.getValue() == 1) {
    if (hdg_mode.getValue() == 0 and b_hdg.getValue() == 1 and b_hdg_h.getValue() == 0) { hdg_mode.setValue(2); b_hdg_h.setValue(1); }
    else if (hdg_mode.getValue() == 2 and b_hdg.getValue() == 1 and b_hdg_h.getValue() == 0) { hdg_mode.setValue(0); b_hdg_h.setValue(1); }
    else if (b_hdg.getValue() == 0) { b_hdg_h.setValue(0); }
  }
}
setlistener(serviceable.getPath(), Heading_Button);
setlistener(b_hdg.getPath(), Heading_Button);
var Range_Button = func() {
  if (serviceable.getValue() == 1) {
    if (range.getValue() == 40 and b_rng.getValue() == 1 and b_rng_h.getValue() == 0) { range.setValue(320); b_rng_h.setValue(1); }
    else if (range.getValue() == 320 and b_rng.getValue() == 1 and b_rng_h.getValue() == 0 and (in_aer_range.getValue() == 1 or b_rng_force.getValue() == 1)) { range.setValue(40); b_rng_h.setValue(1); }
    #else if (range.getValue() == 320 and b_rng.getValue() == 1 and b_rng_h.getValue() == 0) { range.setValue(40); b_rng_h.setValue(1); }    ##### For debug.
    else if (b_rng.getValue() == 0) { b_rng_h.setValue(0); }
  }
}
setlistener(serviceable.getPath(), Range_Button);
setlistener(b_rng.getPath(), Range_Button);
var IPO_Button = func() {
  if (serviceable.getValue() == 1) {
    if (IPO_mode.getValue() == 0 and b_IPO.getValue() == 1 and b_IPO_h.getValue() == 0) { IPO_mode.setValue(1); b_IPO_h.setValue(1); }
    else if (IPO_mode.getValue() == 1 and b_IPO.getValue() == 1 and b_IPO_h.getValue() == 0) { IPO_mode.setValue(0); b_IPO_h.setValue(1); }
    else if (b_IPO.getValue() == 0) { b_IPO_h.setValue(0); }
  }
}
setlistener(serviceable.getPath(), IPO_Button);
setlistener(b_IPO.getPath(), IPO_Button);


######################################################## Lights #############################################################
var Lights_Manager = func() {
  var dt = 0.2;
  if (serviceable.getValue() == 1) {
    if (global_mode.getValue() == 2) {
      interpolate(b_hdg_l1.getPath(), 1, dt);
      interpolate(b_hdg_l2.getPath(), 1, dt);
      interpolate(b_rng_l1.getPath(), 1, dt);
      interpolate(b_rng_l2.getPath(), 1, dt);
      interpolate(b_IPO_l1.getPath(), 1, dt);
      interpolate(b_IPO_l2.getPath(), 1, dt);
      interpolate(l_NV_fault.getPath(), 1, dt);
      interpolate(l_aerodrome.getPath(), 1, dt);
      interpolate(l_auto.getPath(), 1, dt);
      interpolate(l_marker.getPath(), 1, dt);
      interpolate(l_inst_brt.getPath(), 1, dt);
    } else {
      if (hdg_mode.getValue() == 0) {
        interpolate(b_hdg_l1.getPath(), bckl_brightness.getValue(), dt);
        interpolate(b_hdg_l2.getPath(), 0, dt);
      } else if (hdg_mode.getValue() == 2) {
        interpolate(b_hdg_l1.getPath(), 0, dt);
        interpolate(b_hdg_l2.getPath(), bckl_brightness.getValue(), dt);
      }
      if (range.getValue() < range_threshold.getValue()) {
        interpolate(b_rng_l1.getPath(), bckl_brightness.getValue(), dt);
        interpolate(b_rng_l2.getPath(), 0, dt);
      } else {
        interpolate(b_rng_l1.getPath(), 0, dt);
        interpolate(b_rng_l2.getPath(), bckl_brightness.getValue(), dt);
      }
      if (IPO_mode.getValue() == 0) {
        interpolate(b_IPO_l1.getPath(), bckl_brightness.getValue(), dt);
        interpolate(b_IPO_l2.getPath(), 0, dt);
      } else if (IPO_mode.getValue() == 1) {
        interpolate(b_IPO_l1.getPath(), 0, dt);
        interpolate(b_IPO_l2.getPath(), bckl_brightness.getValue(), dt);
      }
      if (NV_fault.getValue() == 1) {
        interpolate(l_NV_fault.getPath(), bckl_brightness.getValue(), dt);
      } else if (NV_fault.getValue() == 0) {
        interpolate(l_NV_fault.getPath(), 0, dt);
      }
      if (in_aer_range.getValue() == 1 or b_rng_force.getValue() == 1) {
        interpolate(l_aerodrome.getPath(), bckl_brightness.getValue(), dt);
      } else if (in_aer_range.getValue() == 0) {
        interpolate(l_aerodrome.getPath(), 0, dt);
      }
      if (global_mode.getValue() == 4) {
        interpolate(l_auto.getPath(), 0, dt);
      } else {
        interpolate(l_auto.getPath(), bckl_brightness.getValue(), dt);
      }
      if ((lowering_mode.getValue() == 1) and (global_mode.getValue() != 5)) {
        interpolate(l_marker.getPath(), bckl_brightness.getValue(), dt);
      } else {
        interpolate(l_marker.getPath(), 0, dt);
      }
      interpolate(l_inst_brt.getPath(), bckl_brightness.getValue(), dt);
    }
  } else {
    interpolate(b_hdg_l1.getPath(), 0, dt);
    interpolate(b_hdg_l2.getPath(), 0, dt);
    interpolate(b_rng_l1.getPath(), 0, dt);
    interpolate(b_rng_l2.getPath(), 0, dt);
    interpolate(b_IPO_l1.getPath(), 0, dt);
    interpolate(b_IPO_l2.getPath(), 0, dt);
    interpolate(l_NV_fault.getPath(), 0, dt);
    interpolate(l_aerodrome.getPath(), 0, dt);
    interpolate(l_auto.getPath(), 0, dt);
    interpolate(l_marker.getPath(), 0, dt);
    interpolate(l_inst_brt.getPath(), 0, dt);
  }
}
setlistener(serviceable.getPath(), Lights_Manager);
setlistener(hdg_mode.getPath(), Lights_Manager);
setlistener(range.getPath(), Lights_Manager);
setlistener(IPO_mode.getPath(), Lights_Manager);
setlistener(NV_fault.getPath(), Lights_Manager);
setlistener(in_aer_range.getPath(), Lights_Manager);
setlistener(global_mode.getPath(), Lights_Manager);
setlistener(bckl_brightness.getPath(), Lights_Manager);
var timer_individual_lights = maketimer(0.02, func(){
  l_inst_r_out.setValue(math.max(l_inst_brt.getValue() * 1, l_panel_r_in.getValue()));
  l_inst_g_out.setValue(math.max(l_inst_brt.getValue() * 0.8, l_panel_g_in.getValue()));
  l_inst_b_out.setValue(math.max(l_inst_brt.getValue() * 0.58, l_panel_b_in.getValue()));
  });
timer_individual_lights.simulatedTime = 1;


######################################################## Stick ##############################################################
var Stick_pos = func() {
  if (serviceable.getValue() == 1) {
    var speed = 0.01 * range.getValue() / 160;
    if (global_mode.getValue() == 4) {
      if (math.abs(b_vertical.getValue()) > 0.7) {
        lat_in.setValue(math.max(math.min(lat_in.getValue() + b_vertical.getValue() * speed * math.cos(up_azimuth.getValue() * D2R), 89.999), -89.999));
        lon_in.setValue(math.fmod(lon_in.getValue() + b_vertical.getValue() * speed * math.sin(up_azimuth.getValue() * D2R) / math.cos(lat_in.getValue() * D2R) + 540, 360) - 180);
      }
      if (math.abs(b_horizontal.getValue()) > 0.7) {
        lat_in.setValue(math.max(math.min(lat_in.getValue() - b_horizontal.getValue() * speed * math.sin(up_azimuth.getValue() * D2R), 89.999), -89.999));
        lon_in.setValue(math.fmod(lon_in.getValue() + b_horizontal.getValue() * speed * math.cos(up_azimuth.getValue() * D2R) / math.cos(lat_in.getValue() * D2R) + 540, 360) - 180);
      }
      #print (math.cos(up_azimuth.getValue()), ' ', math.sin(up_azimuth.getValue()));
    }
  }
}
var timer_Stick_pos = maketimer(0.01, Stick_pos);
timer_Stick_pos.simulatedTime = 1;
var Stick_orient = func() {
  if (serviceable.getValue() == 1) {
    if (global_mode.getValue() == 4) {
      if (b_rotation.getValue() == 1) { true_hdg.setValue(math.fmod(true_hdg.getValue() + 359, 360)); }
      if (b_rotation.getValue() == -1) { true_hdg.setValue(math.fmod(true_hdg.getValue() + 1, 360)); }
    } else if (IPO_mode.getValue() == 1) {
      if (b_rotation.getValue() == 1) { IPO_angle_in.setValue(math.fmod(IPO_angle_in.getValue() + 1, 360)); }
      if (b_rotation.getValue() == -1) { IPO_angle_in.setValue(math.fmod(IPO_angle_in.getValue() + 359, 360)); }
    }
  }
}
setlistener(b_rotation.getPath(), Stick_orient);

######################################################## POWER ##############################################################
var POWER = func() {
  if (getprop("fdm/jsbsim/systems/elec/DC27/U-V") >= 25) { serviceable.setValue(1); }
  else { serviceable.setValue(0); }
}
setlistener("/fdm/jsbsim/systems/elec/DC27/U-V", POWER);


######################################################## MapStructure workaround ##############################################################

var __loadMapSymbols_2020_3_12 = func() {
  # Find files and load them:
  var contents_dir = getprop("/sim/aircraft-dir")~"/Models/Tu-144D-cockpit/Instruments/INO/canvas/";
  var dep_names = [
    # With these extensions, in this order:
    "lcontroller",
    "symbol",
    "scontroller",
    "controller",
    "overlay"
  ];
  var deps = {};
  foreach (var d; dep_names) deps[d] = [];
  foreach (var f; directory(contents_dir)) {
    var ext = size(var s=split(".", f)) > 1 ? s[-1] : nil;
    foreach (var d; dep_names) {
      if (ext == d) {
        append(deps[d], f);
        break
      }
    }
  }
  foreach (var d; dep_names) {
    foreach (var f; deps[d]) {
      var name = split(".", f)[0];
      canvas.MapStructure.loadFile(contents_dir~f, name);
      print("INO-2M installing MapStructure symbol " ~ f);
    }
  }
}

# We have to do this regardless of FG version, now that we are using custom paths.
__loadMapSymbols_2020_3_12();



######################################################## Switch and knob position saving ####################################

# FGBUG This does not work, idk why! Using -set.xml for now.
#aircraftData = props.globals.getNode("/sim/aircraft-data");
#aircraftData.addChild("path").setValue("instrumentation/INO/global_mode");
#aircraftData.addChild("path").setValue("instrumentation/INO/screen/brightness");
#aircraftData.addChild("path").setValue("instrumentation/INO/backlight/brightness");
#aircraftData.addChild("path").setValue("instrumentation/INO/map-centering/ctr-mode");


######################################################## Start ##############################################################
INO_start();
Main_Lamp();
Pos_filter();
timer_Pos_filter.start();
Orient_filter();
timer_Orient_filter.start();
Frame_ind_upd();
Aerodrome_checker();
timer_Aerodrome_checker.start();
Route_len_saver();
timer_Route_len_saver.start();
Frame_route();
Frame_num();
timer_Frame_num.start();
Lights_Manager();
timer_individual_lights.start();
Stick_pos();
timer_Stick_pos.start();
POWER();
