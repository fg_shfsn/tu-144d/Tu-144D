All animations in this tree that use JSBSim variables must be driven
by properties under "instrumentation/jsbsim" instead of "fdm/jsbsim".
The property rules that create those properties are generated
automatically by running the "bin/update-animation-filters.sh" script.

The property rules improve the animations basing on the control
element type and help decouple the flight recorder from JSBSim.

However, the write bindings should write directly to JSBSim tree.
