#!/bin/sh --
#
# sovmetar
#
# Преобразовывает телеграммы METAR / TAF в вид, использовавшийся в
# СССР.
#
#
# ИСТОЧНИКИ
# Единственный похожий на правду материал -- кадр с экраном АСУ из х/ф
# "Размах крыльев" @00:27:24. В остальном основано на догадках.
#
#
# ПРОБЛЕМЫ
# 1. Большинство погодных явлений не преобразовывается, т.к. для этого
#    нужна какая-то общая схема.
# 2. Код пришёл в состояние "так сойдёт".


set -e



qnh_mbar () {
 echo ${1:?} |sed -e's:^Q::' |awk '{printf "%3.0f\n", $1 * 0.75006156}'
}

qnh_100inhg () {
 echo ${1:?} |sed -e's:^A::' |awk '{printf "%3.0f\n", $1 * 0.254}'
}

wind () {
 line=${1:?}
 if echo ${line} |grep KT >/dev/null 2>&1; then
  knots=yes
 else
  knots=
 fi
 set $(echo ${line} | sed -e's:^...:& :' -e's:G: :' -e's:KT::' -e's:MPS::' -e's:KMH:КМЧ:' -e's:^VRB:ПРМ:')
 dir=${1:?}
 speed=${2:?}
 gust=${3}
 if [ -n "${knots}" ]; then
  speed=$(echo $speed |awk '{printf "%i\n", $1 * 0.51444444}')
  if [ $speed -lt 10 ]; then
   speed=0$speed
  fi
 fi
 if [ -n "${gust}" ]; then
  gust=$(echo $gust |awk '{printf "%i\n", $1 * 0.51444444}')
  if [ "$gust" -lt 10 ]; then
   gust=0"$gust"
  fi
  echo ${dir}${speed}%${gust}
 else
  echo ${dir}${speed}
 fi
}

visibility_sm () {
 v=${1:?}
 v=$(echo ${v} |sed 's:[A-Za-z]::g' |awk '{printf "%i00\n", $1 * 16.09344}')
 if [ $v -gt 9999 ]; then
  v=9999
 fi
 echo ${v}
}

cloud () {
 inp=${1:?}
 set $(echo ${inp} | sed -e's:^[A-Za-z]*:& :' -e's:[A-Za-z]*$: &:')
 amount=${1:?}
 h=${2:?}
 type=${3}
 h=$(echo ${h} |awk '{printf "%i", $1 * 0.3048 - 0.5}')
 if [ ${h} -lt 10 ]; then
  h=0${h}
 fi
 if [ ${h} -lt 100 ]; then
  h=0${h}
 fi
 case ${type} in
  CU)
   type=КУ
  ;;
  CB)
   type=КД
  ;;
  TCU)
   type=МК
  ;;
  CC)
   type=ПК
  ;;
  AC)
   type=ВК
  ;;
  SC)
   type=СК
  ;;
  CI)
   type=ПЕ
  ;;
  CS)
   type=ПС
  ;;
  AS)
   type=ВС
  ;;
  NS)
   type=СД
  ;;
  ST)
   type=СЛ
  ;;
 esac
 # SIC Часть типов облаков выбраны случайно, т.к. из METAR их не восстановить.
 case ${amount} in
  FEW)
   echo 2СК${type}${h}
  ;;
  SCT)
   echo 4РС${type}${h}
  ;;
  BKN)
   echo 6СК${type}${h}
  ;;
  OVC)
   echo 8СК${type}${h}
  ;;
 esac
}

while read LINE; do

 ii=0
 for ITEM in ${LINE}; do
   case ${ITEM} in
    [0-9][0-9][0-9]V[0-9][0-9][0-9])
     echo ${ITEM} |sed -e's:V:%:g'
    ;;
    Q[0-9][0-9][0-9]*)
     qnh_mbar ${ITEM}
    ;;
    A[2-3][0-9][0-9][0-9])
     qnh_100inhg ${ITEM}
    ;;
    *[0-9]KT)
     wind ${ITEM}
    ;;
    *[0-9]MPS)
     wind ${ITEM}
    ;;
    *[0-9]KMH)
     wind ${ITEM}
    ;;
    *[0-9]SM)
     visibility_sm ${ITEM}
    ;;
    VV[0-9]*)
     echo ${ITEM} |sed -e's/^VV/ВВ/'
    ;;
    FEW[0-9]*)
     cloud ${ITEM}
    ;;
    SCT[0-9]*)
     cloud ${ITEM}
    ;;
    BKN[0-9]*)
     cloud ${ITEM}
    ;;
    OVC[0-9]*)
     cloud ${ITEM}
    ;;
    PROB[0-9]*)
     echo ${ITEM} |sed -e's/^PROB/ВЕР/'
    ;;
    FM[0-9]*)
     echo ${ITEM} |sed -e's/^FM/ОТ/'
    ;;
    AFT)
     echo ПОСЛЕ
    ;;
    TAF)
     echo ТАФ
    ;;
    AMD|AMDS|COR)
     echo ИСПР
    ;;
    CAVOK)
     echo ХОР
    ;;
    CAVU)
     echo ХОР
    ;;
    LAST)
     echo ПОСЛ
    ;;
    NEXT)
     echo СЛЕД
    ;;
    NO)
     echo БЕЗ
    ;;
    NOSIG)
     echo БИЗМ
    ;;
    BECMG)
     echo ИЗМН
    ;;
    TEMPO)
     echo ВРЕМ
    ;;
    NIL)
     echo НЕТ
    ;;
    RMK)
     echo ПМЧ
    ;;
    AO1)
     echo АВТ
    ;;
    AUTO|AO2)
     echo АВТО
    ;;
    CB|CBS)
     echo КДОБЛ
    ;;
    DSNT)
     echo ДАЛЬН
    ;;
    SKC)
     echo ЯСН
    ;;
    CLR)
     echo ЯС3
    ;;
    NSC)
     echo БСО
    ;;
    NSW)
     echo БСП
    ;;
    BR)
     echo ДК
    ;;
    HZ|FG|BCFG)
     echo ТМ
    ;;
    FU)
     echo ДМ
    ;;
    DU)
     echo ПЛ
    ;;
    LTD)
     echo ОГР
    ;;
    LTG)
     echo МОЛН
    ;;
    SQ)
     echo ШК
    ;;
    WND|WIND)
     echo ВЕТЕР
    ;;
    NXT)
     echo СЛЕД
    ;;
    FCST)
     echo ПРОГН
    ;;
    AND)
     echo И
    ;;
    BY)
     echo К
    ;;
#    N)
#     echo С
#    ;;
#    E)
#     echo В
#    ;;
#    S)
#     echo Ю
#    ;;
#    W)
#     echo З
#    ;;
    *DZ*|*SH*|*TS*|*RA*|*SN*|*GR*)
     echo ${ITEM} |sed \
      -e's:GR:ГД:' \
      -e's:RA:ДЖ:' \
      -e's:SN:СН:' \
      -e's:TS:ГР:' \
      -e's:BC:МЕ:' \
      -e's:DZ:МО:' \
      -e's:SH:ЛИ:' \
      -e's:VC:ОК:' \
     ;
    ;;
    *)
     echo ${ITEM}
    ;;
   esac

  ii=$((ii+1))

 done |
 while read var; do printf '%s ' $var; done |tr a-z A-Z | sed \
  -e's:/:%:g' \
  -e's:PK ВЕТЕР:ПР ВЕТЕР:g' \
  -e's:TO VIS:ВИД ПРИ ВЗЛ:g' \
 ;
#  -e's/[`@]/Ю/g'\
#  -e's/A/А/g'\
#  -e's/B/Б/g'\
#  -e's/C/Ц/g'\
#  -e's/D/Д/g'\
#  -e's/E/Е/g'\
#  -e's/F/Ф/g'\
#  -e's/G/Г/g'\
#  -e's/H/Х/g'\
#  -e's/I/И/g'\
#  -e's/J/Й/g'\
#  -e's/K/К/g'\
#  -e's/L/Л/g'\
#  -e's/M/М/g'\
#  -e's/N/Н/g'\
#  -e's/O/О/g'\
#  -e's/P/П/g'\
#  -e's/Q/Я/g'\
#  -e's/R/Р/g'\
#  -e's/S/С/g'\
#  -e's/T/Т/g'\
#  -e's/U/У/g'\
#  -e's/V/Ж/g'\
#  -e's/W/В/g'\
#  -e's/X/Ь/g'\
#  -e's/Y/Ы/g'\
#  -e's/Z/З/g'\
#  -e's/[\[{]/Ш/g'\
#  -e's/[\\|]/Э/g'\
#  -e's/[\]}]/Щ/g'\
#  -e's/[~\^]/Ч/g'\
#  -e's/[_]/Ъ/g'\
 echo

done
