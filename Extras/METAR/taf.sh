#!/bin/sh --
#
# taf <ICAO code>
#
# Скачивает и печатает прогноз TAF

AIRPORT=$(echo ${1:?} |tr '[A-Z]' '[a-z]')
curl -s "https://www.aviationweather.gov/taf/data?ids=${AIRPORT}&format=raw&metars=off&layout=off" |grep '<code>' |sed -e's/<[/]*code>//g' -e's/<br\/>\&nbsp;\&nbsp;//g' -e's/<br\/>//g'
