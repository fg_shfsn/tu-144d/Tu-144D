#!/bin/sh --
#
# metar <ICAO code>
#
# Скачивает и печатает погоду METAR

AIRPORT=$(echo ${1:?} |tr '[a-z]' '[A-Z]')
METAR=$(curl -s https://tgftp.nws.noaa.gov/data/observations/metar/stations/${AIRPORT}.TXT)
if echo ${METAR} | grep -q '<html>'; then
 echo ${AIRPORT} NIL
 exit 1
fi
echo ${METAR}
