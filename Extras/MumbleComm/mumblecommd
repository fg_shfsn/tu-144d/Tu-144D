#!/bin/sh --
set -e

# mumblecommd
# Mumble RPC daemon
#
# Usage:
# ./mumblecommd [channel-list] [fifo]


LIST=${1:-${HOME}/.fgfs/mumblecomm/Channels.dat}
FIFO=${2:-${HOME}/.fgfs/mumblecomm/fifo}


# Print the first Mumble URL matching the frequency.
findchannel () {
 FREQ=${1:?}
 sed -e's/#.*$//' < ${LIST} \
 | grep "^${FREQ}	" ${LIST} \
 | head -1 \
 | sed -e's/^.*	//g'
}


# Check if Mumble is running and run a Mumble command.
mumble_cmd () {
 if pgrep -u${USER} mumble >/dev/null 2>&1; then
  mumble ${@} || true
 else
  echo 'Mumble is not running.' >&2
 fi
}

# Join Mumble URL.
mumble_join () {
 join="${1}"
 if [ "${join}" != "${URL}" ]; then
  URL="${join}"
  echo "mumble_join ${URL}" >&2
  if [ -n "${join}" ]; then
   mumble_cmd "${URL}"
   if [ "${VOL}" != "0.00" ]; then
    mumble_cmd rpc undeaf
   fi
  else
   URL=
   mumble_cmd rpc mute
   mumble_cmd rpc deaf
  fi
 fi
}

# Mumble PTT.
# FIXME Mumble can only mute/unmute.
mumble_tx () {
 new=${1:?}
 if [ "${new}" != "${TX}" ]; then
  TX=${new}
  echo "mumble_tx ${TX}" >&2
  if [ "${TX}" == 1 ] && [ -n "${URL}" ]; then
   mumble_cmd rpc unmute
  else
   mumble_cmd rpc mute
  fi
 fi
}

# Set Mumble Volume.
# FIXME Mumble can only deaf/undeaf.
mumble_vol () {
 new=${1:?}
 if [ "${new}" != "${VOL}" ]; then
  VOL=${new}
  echo "mumble_vol ${VOL}" >&2
  if [ "${VOL}" != "0.00" ] && [ -n "${URL}" ]; then
   mumble_cmd rpc undeaf
  else
   mumble_cmd rpc deaf
  fi
 fi
}

# Prepare Mumble.
mumble_init() {
 mumble_join ""
 mumble_cmd rpc mute
 mumble_cmd rpc deaf
}


while :; do

 TX=0
 FREQ=0.0000
 URL=XYZZY
 VOL=0.00
 mumble_init

 while read line <${FIFO}; do
  set ${line}
  mumble_tx ${1:?}
  FREQ_NEW=${2:?}
  if [ "${FREQ_NEW}" != "${FREQ}" ]; then
   FREQ=${FREQ_NEW}
   join="$(findchannel ${FREQ})"
   mumble_join "${join}"
  fi
  mumble_vol ${3:?}
 done

 sleep 1
 echo "Received EOF. Restarting." >&2
done
