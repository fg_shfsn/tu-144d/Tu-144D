#!/usr/bin/gnuplot -c
#
#
# MSRPview.plt
#
# Interactively view MSRP-12 recordings
# Protocol Rev. 1.0.0
#
#
# Usage:
# ./MSRPview.plt header recording space-separated-list-of-channels
# gnuplot -c MSRPview.plt header recording space-separated-list-of-channels
#
# Analog channels are numbered 1 to 12.
# Per-channel groups of one-time commands are 13 to 24 for channels 1 to 12,
# respectively, but their display is too crude now.


# Use this if font rendering is strange.
set term X11 noenhanced

header=ARG1
input=ARG2
list=ARG3

set grid
set clip two

plot \
 for [channel in list] \
  "<cat ".header." ".input u 1:(column(channel+1)) w l t columnheader(channel+1)

pause -1
