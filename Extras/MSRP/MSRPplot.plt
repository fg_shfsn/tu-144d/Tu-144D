#!/usr/bin/gnuplot -c
#
#
# MSRPplot.plt
#
# Plot MSRP-12 recordings
# Protocol Rev. 1.0.0
#
#
# Usage:
# ./MSRPplot.plt <header> <recording> <pdf-file> <t_min> <t_max>
# gnuplot -c MSRPplot.plt <header> <recording> <pdf-file> <t_min> <t_max>


set terminal pdfcairo size 14.8cm,21.0cm
set output ARG3

header=ARG1
input=ARG2
t_min=ARG4
t_max=ARG5

set tics font "Helvetica, 4"
set key font "Helvetica, 10" sample -1

set grid

set multiplot layout 24,1 margins 0.04,0.96,0.025,0.99 spacing 0,0.004

# Plot all but the last channel without x numbers.
set xtics format ""
do for [col=2:24] {
 plot [t_min:t_max] "<cat ".header." ".input u 1:col w l t columnheader(col) lt rgb "black"
}
set xtics format "% g"
plot [t_min:t_max] "<cat ".header." ".input u 1:25 w l t columnheader(25) lt rgb "black"
