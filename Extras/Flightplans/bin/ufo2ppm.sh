#!/bin/sh --
set -e

# Convert UFO flightplan to PPM

grep -B4 -A3 '<crossat type' \
|grep -v \
 -e'<crossat type' \
 -e'<on-ground type' \
 -e'<ktas type'\
|sed \
 -e's/<wpt/<PPM/' \
 -e's/wpt>/PPM>\
/' \
 -e's/<name.*">/<!-- /' \
 -e's/<\/name>/ -->/' \
 -e's/<lon/<lam-deg/' \
 -e's/lon>/lam-deg>/' \
 -e's/<lat/<phi-deg/' \
 -e's/lat>/phi-deg>/' \
 -e's/type="double"/type="float"/' \
 -e's/  / /g'
