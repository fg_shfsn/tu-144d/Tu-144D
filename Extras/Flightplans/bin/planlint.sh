#!/bin/sh --
set -e

FILE=${1:?}

echo 'Counts and indices.'
grep -ecount -e'PPM n' -e'AP n' -e'RM n' -- "${FILE}"
echo

echo 'Route latitude.'
grep -A5 'PPM n' -- "${FILE}" |grep phi
echo

echo 'Route longitude.'
grep -A5 'PPM n' -- "${FILE}" |grep lam
echo
