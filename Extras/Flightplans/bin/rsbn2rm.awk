#!/usr/bin/awk -f

# Convert RSBN lines from nav.dat to RM.

{
 printf "\
  <RM n=\"%i\">\
   <!-- %s %s %s %s -->\
   <phi-deg type=\"float\">%s</phi-deg>\
   <lam-deg type=\"float\">%s</lam-deg>\
   <f type=\"float\">%2.1f</f>\
  </RM>\
", NR, $8, $9, $10, $11, $2, $3, ($5-95995)/5
}
