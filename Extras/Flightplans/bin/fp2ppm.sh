#!/bin/sh --
set -e

# Convert FlightGear flightplan to PPM

grep -B1 -A5 \
 -e'<type.*>basic<.*type' \
 -e'<type.*>navaid<.*type' \
|grep -v '<type type' \
|sed \
 -e's/<wp/<PPM/' \
 -e's/wp>/PPM>\
/' \
 -e's/<ident.*">/<!-- /' \
 -e's/<\/ident>/ -->/' \
 -e's/<lon/<lam-deg/' \
 -e's/lon>/lam-deg>/' \
 -e's/<lat/<phi-deg/' \
 -e's/lat>/phi-deg>/' \
 -e's/type="double"/type="float"/' \
 -e's/  / /g'
