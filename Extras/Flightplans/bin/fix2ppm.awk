#!/usr/bin/awk -f

# Convert lines from fix.dat to PPM.

{
 printf "\
  <PPM n=\"%i\">\
   <!-- %s -->\
   <phi-deg type=\"float\">%s</phi-deg>\
   <lam-deg type=\"float\">%s</lam-deg>\
  </PPM>\
", NR, $3, $1, $2
}
