#!/usr/bin/awk -f

# Convert lines from nav.dat to PPM.

{
 printf "\
  <PPM n=\"%i\">\
   <!-- %s %s %s %s -->\
   <phi-deg type=\"float\">%s</phi-deg>\
   <lam-deg type=\"float\">%s</lam-deg>\
  </PPM>\
", NR, $8, $9, $10, $11, $2, $3
}
