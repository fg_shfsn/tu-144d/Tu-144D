<?xml version="1.0" encoding="UTF-8"?>

<!DOCTYPE system [
 <!ENTITY NPK "fcs/NPK">
 <!ENTITY ABSU "&NPK;/ABSU">
 <!ENTITY U "&ABSU;/SAU/VKV">
]>

<system name="VKV-144 altitude correction calculator">
 <!-- Вычислитель корректора высоты ВКВ-144 -->
 <!-- [ABSU 2] -->


 <!-- [p. 34] -->
 <!-- ABSU-154
  Name      Value                 Unit                 Page
  K          0.1 +- 0.015         deg_elevator / m     571
  K_gnd      0.2 +- 0.03          deg_elevator / m     572
  Ki         0.002 +- 0.0006      deg_elevator / (m*s) 574
  Kd         0.4 +- 0.12          deg_elevator / (m/s) 575
  theta_MGV 10 +- 2.5             deg                  576
  F1        theta_MGV / K_theta (see SAU)              576
 -->
 <property value="0.1">&U;/H/tuning/K</property>
 <property value="0.2">&U;/H/tuning/K_gnd</property>
 <property value="0.4">&U;/H/tuning/Kd</property>
 <property value="0.002">&U;/H/tuning/Ki</property>
 <property value="-5.0">&U;/H/tuning/F1-min</property>
 <property value="5.0">&U;/H/tuning/F1-max</property>


 <property value="0.05">&U;/H_FL/tuning/K</property>
 <property value="0.4">&U;/H_FL/tuning/Kd</property>
 <property value="0.002">&U;/H_FL/tuning/Ki</property>
 <property value="-2.5">&U;/H_FL/tuning/F-min</property>
 <property value="2.5">&U;/H_FL/tuning/F-max</property>
 <!-- deltaH outside which the integrator is disabled. -->
<!-- Disabled: if FL is drifting away from set FL, this will further speed it up once past the limits
 <property value="-100.0">&U;/H_FL/tuning/deltaH_i-min</property>
 <property value="100.0">&U;/H_FL/tuning/deltaH_i-max</property>
-->




 <channel name="H">
  <!-- [p. 34] -->

  <switch name="&U;/H/K">
   <default value="&U;/H/tuning/K"/>
   <test logic="OR" value="&U;/H/tuning/K_gnd">
    &ABSU;/sensor/RV/ok NE 0
    gear/main-up EQ 0
   </test>
  </switch>

  <pure_gain name="&U;/H/deltaH-K">
   <input>&NPK;/SVS/H/deltaH-m</input>
   <gain>&U;/H/K</gain>
  </pure_gain>

  <lead_lag_filter name="&U;/H/deltaH-KdTd">
   <input>&NPK;/SVS/H/deltaH-m</input>
   <c1>&U;/H/tuning/Kd</c1>
   <c2>0.0</c2>
   <c3>1.0</c3>
   <c4>1.0</c4>
  </lead_lag_filter>

  <switch name="&U;/H/deltaH-KdTd-switched">
   <default value="&U;/H/deltaH-KdTd"/>
   <test value="0.0">
    &NPK;/panel/PSU/switch/turbulence NE 0
   </test>
  </switch>

  <switch name="&U;/H/func/Ki">
   <default value="&U;/H/tuning/Ki"/>
   <test value="10.0">
    &ABSU;/SAU/status/mode/pitch-H EQ 0
   </test>
  </switch>

  <switch name="&U;/H/deltaH-Ki_in">
   <default value="&NPK;/SVS/H/deltaH-m"/>
   <!-- Relax if disabled -->
   <test value="-&U;/H/deltaH-Ki-F1">
    &ABSU;/SAU/status/mode/pitch-H EQ 0
   </test>
   <!-- Windup protection -->
   <test logic="AND" value="0.0">
    &U;/H/deltaH-Ki-F1 LE &U;/H/tuning/F1-min
    &NPK;/SVS/H/deltaH-m LT 0.0
   </test>
   <test logic="AND" value="0.0">
    &U;/H/deltaH-Ki-F1 GE &U;/H/tuning/F1-max
    &NPK;/SVS/H/deltaH-m GT 0.0
   </test>
  </switch>

  <integrator name="&U;/H/deltaH-Ki-F1">
   <input>&U;/H/deltaH-Ki_in</input>
   <c1>&U;/H/func/Ki</c1>
  </integrator>

  <summer name="&U;/H/deltaH_star">
   <output>&ABSU;/SAU/pitch/deltaH_star</output>
   <input>&U;/H/deltaH-K</input>
   <input>&U;/H/deltaH-KdTd-switched</input>
   <input>&U;/H/deltaH-Ki-F1</input>
  </summer>

 </channel>


 <channel name="Flight level">

  <pure_gain name="&U;/H_FL/deltaH-K">
   <input>&U;/H_FL/deltaH-m</input>
   <gain>&U;/H_FL/tuning/K</gain>
  </pure_gain>

  <lead_lag_filter name="&U;/H_FL/deltaH-KdTd">
   <!-- SIC H, not deltaH, else will jump when setting alt. -->
   <input>&NPK;/SVS/H/Habs-m</input>
   <c1>&U;/H_FL/tuning/Kd</c1>
   <c2>0.0</c2>
   <c3>1.0</c3>
   <c4>1.0</c4>
  </lead_lag_filter>

  <switch name="&U;/H_FL/deltaH-KdTd-switched">
   <default value="&U;/H_FL/deltaH-KdTd"/>
   <test value="0.0">
    &NPK;/panel/PSU/switch/turbulence NE 0
   </test>
  </switch>

  <switch name="&U;/H_FL/func/Ki">
   <default value="&U;/H_FL/tuning/Ki"/>
   <test logic="OR" value="10.0">
    &ABSU;/STU/status/route-pitch EQ 0
    &ABSU;/SAU/status/mode/pitch-STU EQ 0
<!--
    &U;/H_FL/deltaH-m LT &U;/H_FL/tuning/deltaH_i-min
    &U;/H_FL/deltaH-m GT &U;/H_FL/tuning/deltaH_i-max
-->
   </test>
  </switch>

  <switch name="&U;/H_FL/deltaH-Ki_in">
   <default value="&U;/H_FL/deltaH-m"/>
   <!-- Relax if disabled or outside of range where integration is allowed -->
   <test logic="OR" value="-&U;/H_FL/deltaH-Ki-F">
    &ABSU;/STU/status/route-pitch EQ 0
    &ABSU;/SAU/status/mode/pitch-STU EQ 0
<!--
    &U;/H_FL/deltaH-m LT &U;/H_FL/tuning/deltaH_i-min
    &U;/H_FL/deltaH-m GT &U;/H_FL/tuning/deltaH_i-max
-->
   </test>
   <!-- Windup protection -->
   <test logic="AND" value="0.0">
    &U;/H_FL/deltaH-Ki-F LE &U;/H_FL/tuning/F-min
    &U;/H_FL/deltaH-m LT 0.0
   </test>
   <test logic="AND" value="0.0">
    &U;/H_FL/deltaH-Ki-F GE &U;/H_FL/tuning/F-max
    &U;/H_FL/deltaH-m GT 0.0
   </test>
  </switch>

  <integrator name="&U;/H_FL/deltaH-Ki-F">
   <input>&U;/H_FL/deltaH-Ki_in</input>
   <c1>&U;/H_FL/func/Ki</c1>
  </integrator>

<!--
  <switch name="&U;/H_FL/deltaH-Ki-F-switched">
   <default value="&U;/H_FL/deltaH-Ki-F"/>
   <test logic="OR" value="0.0">
    &U;/H_FL/deltaH-m LT &U;/H_FL/tuning/deltaH_i-min
    &U;/H_FL/deltaH-m GT &U;/H_FL/tuning/deltaH_i-max
   </test>
  </switch>
-->

  <summer name="&U;/H_FL/deltaH_star">
   <output>&ABSU;/STU/pitch/VE/deltaH_star</output>
   <input>&U;/H_FL/deltaH-K</input>
   <input>&U;/H_FL/deltaH-KdTd-switched</input>
<!--
   <input>&U;/H_FL/deltaH-Ki-F-switched</input>
-->
   <input>&U;/H_FL/deltaH-Ki-F</input>
  </summer>

 </channel>


</system>
